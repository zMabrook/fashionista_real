package com.zmabrook.fashionista.Config;

/**
 * CommonConstants
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Utils
 * @since 7/18/16
 **/
public class CommonConstants {
    public static final String PREFS_NAME = "FASHIONISTAaap";
    public static final String PREFS_USER = "user";
    public static final String PREFS_TOUR_FINISHED = "TourFinished";
    public static final String PREFS_LOGGED_IN = "isLoggedIn";
    public static final String APPLICATION_URL = "URL";




    public static final String BASIC_URL= "http://52.178.165.220/";

    public static final String CLIENT_ID="cF0gmmroamVDNTcM";
    public static final String CLIENT_SECRET="xSwJd0MCTxhdCNZa2HYasMt13KsXPom0cv9s";

    public static final String TWITTER_KEY = "jYTtYS3kSedM5uXnYBeeGEDJ9";
    public static final String TWITTER_SECRET = "UbzMmMx2Um2c7dDZH4sV39sY2Ms9udOCFizLrczTyyIjASbnKR";
    public static final String GOOGLE_CLIENT_ID = "648342988430-pbv8oi1n6ekvoqfqps77rvmihgck75vn.apps.googleusercontent.com";



    public static final String APP_TAG = "fashionistaApp";



    final static public  int SIGN_UP_ACTION  =101;
    final static public  int LOGIN_ACTION  =102;
    final static public  int TRENDING_ACTION  =103;
    final static public  int CATEGORIES_SEARCH_ACTION  =104;
    final static public  int BRANDS_SEARCH_ACTION  =105;
    final static public  int PEOPLE_SEARCH_ACTION  =106;
    final static public  int MYFASHION_ACTION  =107;
    final static public  int TRENDING_PEOPLE_ACTION  =108;
    final static public  int FEATURED_PEOPLE_ACTION  =109;
    final static public  int GET_USER_ACTION  =110;
    final static public  int GET_USER_LOOKS_ACTION  =111;
    final static public  int GET_MY_PROFILE_DATA_ACTION  =112;
    final static public  int TOGGLE_FOLLOW_NOTIFICATIONS  =113;
    final static public  int GET_MY_FOLLOWERS_ACTION  =114;
    final static public  int GET_MY_FOLLOWING_ACTION  =115;
    final static public  int GET_MY_BLOCKED_ACTION  =116;
    final static public  int SUB_CATEGORIES_SEARCH_ACTION  =117;
    final static public  int LOOK_TYPES_SEARCH_ACTION  =118;
    final static public  int CREATE_NEW_LOOK  =119;
    final static public  int FOLLOW_USER_ACTION  =120;
    final static public  int UNFOLLOW_USER_ACTION  =121;
    final static public  int LIKE_LOOK_ACTION  =122;
    final static public  int UNLIKE_LOOK_ACTION  =123;
    final static public  int BLOCK_USER_ACTION  =124;
    final static public  int UNBLOCK_USER_ACTION  =125;
    final static public  int PIN_LOOK_ACTION  =126;
    final static public  int UNPIN_LOOK_ACTION  =127;
    final static public  int UPDATE_USER_DATA_ACTION  =128;


}

