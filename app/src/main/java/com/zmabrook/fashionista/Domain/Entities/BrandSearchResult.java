package com.zmabrook.fashionista.Domain.Entities;

import java.util.ArrayList;

/**
 * BrandSearchResult
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Entities
 * @since 8/9/16
 **/
public class BrandSearchResult extends  Entity {
    double numOfBrands;
    double numOfPages;
    ArrayList<Brand> brands;


    public ArrayList<Brand> getBrands() {
        return brands;
    }

    public void setBrands(ArrayList<Brand> brands) {
        this.brands = brands;
    }

    public double getNumOfBrands() {
        return numOfBrands;
    }

    public void setNumOfBrands(double numOfBrands) {
        this.numOfBrands = numOfBrands;
    }

    public double getNumOfPages() {
        return numOfPages;
    }

    public void setNumOfPages(double numOfPages) {
        this.numOfPages = numOfPages;
    }
}
