package com.zmabrook.fashionista.Domain.Entities;

import java.io.Serializable;

/**
 * Category
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Entities
 * @since 8/8/16
 **/
public class Category  extends Entity implements Serializable {
    String id;
    String name;
    String iconUrl;
    boolean isSub =false;

    public boolean isSub() {
        return isSub;
    }

    public void setIsSub(boolean isSub) {
        this.isSub = isSub;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
