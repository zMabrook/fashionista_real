package com.zmabrook.fashionista.Domain.Entities;

import java.util.ArrayList;

/**
 * CategorySearchResult
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Entities
 * @since 8/28/16
 **/
public class CategorySearchResult extends  Entity {
    ArrayList<Category> categories ;

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }
}
