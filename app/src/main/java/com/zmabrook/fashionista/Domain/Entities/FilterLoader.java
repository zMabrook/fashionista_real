package com.zmabrook.fashionista.Domain.Entities;

import android.widget.ImageView;

/**
 * FilterLoader
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Entities
 * @since 8/26/16
 **/
public class FilterLoader extends  Entity {
   public ImageView imageView;
    public  int  filterID;
}
