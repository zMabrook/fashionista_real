package com.zmabrook.fashionista.Domain.Entities;

import java.io.Serializable;

/**
 * Item
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Entities
 * @since 8/4/16
 **/
public class Item extends Entity implements Serializable{
    String id;
    double x;
    double y;
    double price;
    String brand;
    Location location;
    Category category;
    Brand wholeBrand;


    public Brand getWholeBrand() {
        return wholeBrand;
    }

    public void setWholeBrand(Brand wholeBrand) {
        this.wholeBrand = wholeBrand;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
