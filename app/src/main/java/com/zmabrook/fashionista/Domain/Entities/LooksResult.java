package com.zmabrook.fashionista.Domain.Entities;

import java.util.ArrayList;

/**
 * LooksResult
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Entities
 * @since 8/4/16
 **/
public class LooksResult extends  Entity {
    public ArrayList<Look> looks;
    public int numberOfLooks;
    public int numberOfPages;

}
