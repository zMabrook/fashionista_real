package com.zmabrook.fashionista.Domain.Entities;

import java.util.ArrayList;

/**
 * PeopleSearchResult
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Entities
 * @since 8/10/16
 **/
public class PeopleSearchResult  extends  Entity {
    double numOfPeople;
    double numOfPages;
    ArrayList<User> PeopleList;


    public ArrayList<User> getPeopleList() {
        return PeopleList;
    }

    public void setPeopleList(ArrayList<User> peopleList) {
        this.PeopleList = peopleList;
    }

    public double getNumOfPeople() {
        return numOfPeople;
    }

    public void setNumOfPeople(double numOfPeople) {
        this.numOfPeople = numOfPeople;
    }

    public double getNumOfPages() {
        return numOfPages;
    }

    public void setNumOfPages(double numOfPages) {
        this.numOfPages = numOfPages;
    }
}
