package com.zmabrook.fashionista.Domain.Entities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * RequestBody
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Entities
 * @since 7/25/16
 **/
public class RequestBody extends Entity {
    JSONObject mJsonObjectRequestBody;
    JSONArray mJsonArrayRequestBody;
    HashMap<String, String> headers;
    HashMap<String, String> queryParams = new HashMap<>();
    HashMap<String, String> pathParams = new HashMap<>();

    public boolean cancelRequestsWithSameTag = false;

    public JSONArray getmJsonArrayRequestBody() {
        return mJsonArrayRequestBody;
    }


    public HashMap<String, String> getPathParams() {
        return pathParams;
    }

    public void setPathParams(HashMap<String, String> pathParams) {
        this.pathParams = pathParams;
    }

    public void setmJsonArrayRequestBody(JSONArray mJsonArrayRequestBody) {
        this.mJsonArrayRequestBody = mJsonArrayRequestBody;
    }

    public JSONObject getJsonObjectRequestBody() {
        return mJsonObjectRequestBody;
    }

    public void setJsonObjectRequestBody(JSONObject requestBody) {
        this.mJsonObjectRequestBody = requestBody;
    }

    public HashMap<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(HashMap<String, String> headers) {
        this.headers = headers;
    }

    public HashMap<String, String> getQueryParams() {
        if (queryParams == null) {
            queryParams = new HashMap<>();
        }
        return queryParams;
    }

    public void setQueryParams(HashMap<String, String> queryParams) {
        this.queryParams = queryParams;
    }
}


