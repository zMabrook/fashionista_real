package com.zmabrook.fashionista.Domain.Entities;

import java.io.Serializable;

/**
 * User
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Entities
 * @since 7/19/16
 **/
public class User extends Entity implements Serializable {
    public String uid;
    public String token;
    public String refreshToken;
    public String client;

    String mUserID;
    String mName;
    String mUsername;
    String mEmail;
    int mAge;
    String mGender;
    String imageUrl;
    String coverImageUrl;
    String mPassword;
    String bio;

    String followersCount;
    String followingCount;

    String looksCount;
    String viewsCount;
    String likesCount;
    String pinsCount;
    String blocksCount;

    boolean isfollowed;
    boolean isHeader = false;


    public String getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(String viewsCount) {
        this.viewsCount = viewsCount;
    }

    public String getBlocksCount() {
        return blocksCount;
    }

    public void setBlocksCount(String blocksCount) {
        this.blocksCount = blocksCount;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getLooksCount() {
        return looksCount;
    }

    public void setLooksCount(String looksCount) {
        this.looksCount = looksCount;
    }

    public String getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(String likesCount) {
        this.likesCount = likesCount;
    }

    public String getPinsCount() {
        return pinsCount;
    }

    public void setPinsCount(String pinsCount) {
        this.pinsCount = pinsCount;
    }

    public boolean isfollowed() {
        return isfollowed;
    }

    public void setIsfollowed(boolean isfollowed) {
        this.isfollowed = isfollowed;
    }


    public boolean isHeader() {
        return isHeader;
    }

    public void setIsHeader(boolean isHeader) {
        this.isHeader = isHeader;
    }

    public String getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(String followersCount) {
        this.followersCount = followersCount;
    }

    public String getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(String followingCount) {
        this.followingCount = followingCount;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUserID() {
        return mUserID;
    }

    public void setUserID(String mUserID) {
        this.mUserID = mUserID;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        this.mUsername = username;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public int getAge() {
        return mAge;
    }

    public void setAge(int age) {
        this.mAge = age;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        this.mGender = gender;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }


}
