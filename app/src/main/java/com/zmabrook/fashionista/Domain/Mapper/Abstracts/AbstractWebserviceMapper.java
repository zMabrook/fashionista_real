package com.zmabrook.fashionista.Domain.Mapper.Abstracts;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Mapper.VolleyQueue.QueueWrapper;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Fashionista.FashionistaError;
import com.zmabrook.fashionista.Interfaces.Parser;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.Utils.NetWorkConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * AbstractWebserviceMapper
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Mapper.Abstracts
 * @since 7/25/16
 **/
abstract public class AbstractWebserviceMapper {

    public enum JsonRequestType {JSON_ARRAY, JSON_OBJECT}

    protected Context mContext;
    protected QueueWrapper mQueueWrapper;
    public String requestTag;
    public int statusCode;
    public int action;
    protected Parser parser = null;
    protected String mFullUrl;
    protected HashMap<String, String> mDefaultQueryStringParams;
    protected HashMap<String, String> mQueryStringParams;
    protected Map<String, String> responseHeaders;
    protected RequestBody body;


    public AbstractWebserviceMapper(Context Context, int action) {
        this.mContext = Context;
        this.action = action;
        mQueueWrapper = new QueueWrapper(mContext);
        mDefaultQueryStringParams = new HashMap<>();
        responseHeaders = new HashMap<>();

        initParser();

    }

    /**
     * returns the url needed to do the http request to the service
     *
     * @return String
     */
    protected abstract String getUrl();

    /**
     * @return tag of the request
     */
    protected abstract String initRequestTag();

    protected abstract void initParser();

    protected abstract HashMap<String, String> getQueryStringParams();

    public String getRequestTag() {
        return requestTag;
    }

    protected String getFullUrl() {
        mQueryStringParams = getQueryStringParams();
        mQueryStringParams.putAll(mDefaultQueryStringParams);
        String params = convertMapToString(mQueryStringParams);


        return CommonConstants.BASIC_URL + getUrl() + "?" + params;
    }

    protected String convertMapToString(HashMap<String, String> map) {
        ArrayList<String> params = new ArrayList<>();

        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (entry.getKey().equals("path")) {
                params.add(entry.getValue());
            } else if (entry.getKey().equals("latlang")) {
                params.add("@" + entry.getValue());

            } else {
                params.add(entry.getKey() + "=" + entry.getValue());
            }
        }
        StringBuilder sb = new StringBuilder();
        for (String s : params) {
            sb.append(s);
            sb.append("&");
        }
        if (sb.length() != 0) {
            sb.deleteCharAt(sb.length() - 1);

        }

        return sb.toString().replace("\n", "");
    }


    /**
     * @return list of the requested data
     */
    public void executeRequest(JsonRequestType requestType, int method, FashionistaListener listener, RequestBody body) {
        boolean isConnectedToInternet = NetWorkConnection.isConnected(mContext);
        this.body = body;
        if (isConnectedToInternet) {


            switch (requestType) {
                case JSON_ARRAY:
                    executeJsonArrayRequest(method, listener, body);

                    break;
                case JSON_OBJECT:
                    executeJsonObjectRequest(method, listener, body);
                    break;


            }
        } else {
            FashionistaError error = new FashionistaError(false,mContext);
            listener.onError(error, null);
        }
    }


    private void executeJsonArrayRequest(int method, final FashionistaListener listener, final RequestBody body) {
        mFullUrl = getFullUrl();
        Log.d(CommonConstants.APP_TAG, "url: " + mFullUrl);
        requestTag = initRequestTag();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(method, mFullUrl, body.getmJsonArrayRequestBody(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d(CommonConstants.APP_TAG, "successssssss:" + response.toString());
                ArrayList<Entity> entities = new ArrayList<>();
                for(int i=0 ; i<response.length(); i++) {
                    try {
                        Entity entity = parser.parse(response.get(i));
                        if (entity==null){
                            continue;
                        }
                        entities.add(entity);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                listener.onSuccess(entities);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (statusCode>=200 && statusCode<=209){
                    listener.onSuccess(null);
                }
                Log.d(CommonConstants.APP_TAG, "errrrror:" + error.getMessage());
                String body;
                JSONObject jsonObject = null;

                if (error!=null && error.networkResponse!= null && error.networkResponse.data != null) {
                    try {
                        body = new String(error.networkResponse.data, "UTF-8");
                        jsonObject = new JSONObject(body);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                Entity entity = parser.parse(jsonObject);
                Log.d(CommonConstants.APP_TAG, "errrrror:" + jsonObject);

                listener.onError(error, entity);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                SharedPreferences settings = mContext.getApplicationContext().getSharedPreferences(CommonConstants.PREFS_NAME, Context.MODE_PRIVATE);

                Gson gson = new Gson();
                String json = settings.getString(CommonConstants.PREFS_USER, "");
                User user = gson.fromJson(json, User.class);
                HashMap<String, String> headers = new HashMap<String, String>();
                if (user != null) {
                    headers.put("access-token", user.token);
                    headers.put("uid", user.uid);
                    headers.put("client", user.client);
                }
                return headers;
            }

            @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                statusCode = response.statusCode;
                responseHeaders = response.headers;

                Log.d("MyApp", "the status code isssss: " + statusCode);
                return super.parseNetworkResponse(response);
                /*try {
                    String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));

                    JSONObject result = null;

                    if (jsonString != null && jsonString.length() > 0)
                        result = new JSONObject(jsonString);

                    return Response.success(result,
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException je) {
                    return Response.error(new ParseError(je));
                }        */
            }
        };

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                -1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        if (body.cancelRequestsWithSameTag) {
            mQueueWrapper.addToRequestQueue(jsonArrayRequest, getRequestTag());

        } else {
            mQueueWrapper.addToRequestQueueWithoutCanceling(jsonArrayRequest, getRequestTag());

        }
    }



    private void executeJsonObjectRequest(int method, final FashionistaListener listener, final RequestBody body) {
        mFullUrl = getFullUrl();
        Log.d(CommonConstants.APP_TAG, "url: " + mFullUrl);
        requestTag = initRequestTag();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(method, mFullUrl, body.getJsonObjectRequestBody(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(CommonConstants.APP_TAG, "successssssss:" + response.toString());
                Entity entity = parser.parse(response);
                if (entity == null) {
                    listener.onSuccess(entity);
                    return;
                }
                if (action == CommonConstants.LOGIN_ACTION || action == CommonConstants.SIGN_UP_ACTION) {
                    User user = (User) entity;
                    user.uid = responseHeaders.get("uid");
                    user.token = responseHeaders.get("access-token");
                    user.client = responseHeaders.get("client");
                    user.refreshToken = responseHeaders.get("refresh-token");
                    listener.onSuccess(user);
                    return;
                }

                listener.onSuccess(entity);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (statusCode>=200 && statusCode<=209){
                    listener.onSuccess(null);
                    return;
                }
                Log.d(CommonConstants.APP_TAG, "errrrror:" + error.getMessage());
                String body;
                JSONObject jsonObject = null;

                if (error!=null && error.networkResponse!=null && error.networkResponse.data != null) {
                    try {
                        body = new String(error.networkResponse.data, "UTF-8");
                        jsonObject = new JSONObject(body);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Entity entity=null;

                Log.d(CommonConstants.APP_TAG, "errrrror:" + jsonObject);
                if (action == CommonConstants.LOGIN_ACTION || action == CommonConstants.SIGN_UP_ACTION) {
                    entity = parser.parse(jsonObject);

                }

                    listener.onError(error, entity);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                SharedPreferences settings = mContext.getApplicationContext().getSharedPreferences(CommonConstants.PREFS_NAME, Context.MODE_PRIVATE);

                Gson gson = new Gson();
                String json = settings.getString(CommonConstants.PREFS_USER, "");
                User user = gson.fromJson(json, User.class);
                HashMap<String, String> headers = new HashMap<String, String>();
                if (user != null) {
                    headers.put("access-token", user.token);
                    headers.put("uid", user.uid);
                    headers.put("client", user.client);
                }
                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                statusCode = response.statusCode;
                responseHeaders = response.headers;

                Log.d("MyApp", "the status code isssss: " + statusCode);
                return super.parseNetworkResponse(response);
                /*try {
                    String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));

                    JSONObject result = null;

                    if (jsonString != null && jsonString.length() > 0)
                        result = new JSONObject(jsonString);

                    return Response.success(result,
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException je) {
                    return Response.error(new ParseError(je));
                }        */
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                -1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        if (body.cancelRequestsWithSameTag) {
            mQueueWrapper.addToRequestQueue(jsonObjectRequest, getRequestTag());

        } else {
            mQueueWrapper.addToRequestQueueWithoutCanceling(jsonObjectRequest, getRequestTag());

        }
    }


    private void executeJsonObjectGetRequest(int method, final FashionistaListener listener, final RequestBody requestData) {
        mFullUrl = getFullUrl();
        Log.d(CommonConstants.APP_TAG, "url: " + mFullUrl);
        requestTag = initRequestTag();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(method, mFullUrl, requestData.getJsonObjectRequestBody(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Entity entity = parser.parse(response);
                if (entity == null) {
                    listener.onSuccess(entity);
                    return;
                }

                listener.onSuccess(entity);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                listener.onError(error, null);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                SharedPreferences settings = mContext.getApplicationContext().getSharedPreferences(CommonConstants.PREFS_NAME, Context.MODE_PRIVATE);

                Gson gson = new Gson();
                String json = settings.getString(CommonConstants.PREFS_USER, "");
                User user = gson.fromJson(json, User.class);
                HashMap<String, String> headers = new HashMap<String, String>();

                if (user != null) {
                    headers.put("access-token", user.token);
                    headers.put("uid", user.uid);
                    headers.put("client", user.client);
                }
                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                statusCode = response.statusCode;
                responseHeaders = response.headers;
                Log.d("MyApp", "the status code isssss: " + statusCode);
                return super.parseNetworkResponse(response);
                /*try {
                    String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));

                    JSONObject result = null;

                    if (jsonString != null && jsonString.length() > 0)
                        result = new JSONObject(jsonString);

                    return Response.success(result,
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException je) {
                    return Response.error(new ParseError(je));
                }        */
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                -1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        if (body.cancelRequestsWithSameTag) {
            mQueueWrapper.addToRequestQueue(jsonObjectRequest, getRequestTag());

        } else {
            mQueueWrapper.addToRequestQueueWithoutCanceling(jsonObjectRequest, getRequestTag());

        }
    }


}
