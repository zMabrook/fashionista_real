package com.zmabrook.fashionista.Domain.Mapper;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.Item;
import com.zmabrook.fashionista.Domain.Entities.Location;
import com.zmabrook.fashionista.Domain.Entities.Look;
import com.zmabrook.fashionista.Domain.Entities.LooksResult;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Interfaces.Parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * LooksMapper
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Mapper
 * @since 8/4/16
 **/
public class LooksMapper extends AbstractWebserviceMapper {
    public static final String trendingTag = "trendingTag";
    public static final String myfashionTag = "myfashionTag";
    public static final String createNewLookTag = "createNewLookTag";
    public static final String likeTag = "likeTag";
    public static final String unlikeTag = "unlikeTag";
    public static final String pinTag = "pinTag";
    public static final String unpinTag = "unpinTag";

    public LooksMapper(Context Context, int action) {
        super(Context, action);
    }

    @Override
    protected String getUrl() {
        switch (action) {
            case CommonConstants.TRENDING_ACTION:
                return "api/v1/looks";

            case CommonConstants.MYFASHION_ACTION:
                return "api/v1/looks/me";
            case CommonConstants.GET_USER_LOOKS_ACTION:
                return "api/v1/profile/"+body.getPathParams().get("id")+"/looks";
            case CommonConstants.CREATE_NEW_LOOK:
                return "api/v1/looks";
            case CommonConstants.LIKE_LOOK_ACTION:
                return "api/v1/looks/"+body.getPathParams().get("id")+"/likes";
            case CommonConstants.UNLIKE_LOOK_ACTION:
                return "api/v1/looks/"+body.getPathParams().get("id")+"/likes";
            case CommonConstants.PIN_LOOK_ACTION:
                return "api/v1/looks/"+body.getPathParams().get("id")+"/pins";
            case CommonConstants.UNPIN_LOOK_ACTION:
                return "api/v1/looks/"+body.getPathParams().get("id")+"/pins";
        }
        return "api/v1/looks";
    }

    @Override
    protected String initRequestTag() {
        switch (action) {
            case CommonConstants.TRENDING_ACTION:
                return trendingTag;
            case CommonConstants.MYFASHION_ACTION:
                return myfashionTag;
            case CommonConstants.CREATE_NEW_LOOK:
                return createNewLookTag;
            case CommonConstants.LIKE_LOOK_ACTION:
                return likeTag;
            case CommonConstants.UNLIKE_LOOK_ACTION:
                return unlikeTag;
            case CommonConstants.PIN_LOOK_ACTION:
                return pinTag;
            case CommonConstants.UNPIN_LOOK_ACTION:
                return unpinTag;
        }
        return trendingTag;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject results) {
                LooksResult looksResult = new LooksResult();
                ArrayList<Look> looks = null;
                try {
                    if (results.getInt("count") == 0) {
                        return looksResult;
                    }
                    if (results.has("count") && results.getInt("count") != 0)
                        looksResult.numberOfLooks = results.getInt("count");

                    if (results.has("pages") && results.getInt("pages") != 0)
                        looksResult.numberOfPages = results.getInt("pages");


                    if (results.has("looks") && results.getJSONArray("looks").length() != 0) {
                        looks = new ArrayList<>();
                        JSONArray lArray = results.getJSONArray("looks");
                        try {


                            for (int i = 0; i < lArray.length(); i++) {
                                JSONObject lObject = lArray.getJSONObject(i);
                                Look newLook = new Look();

                                if (lObject.has("id") && lObject.getString("id") != null)
                                    newLook.setId(lObject.getString("id"));

                                if (lObject.has("is_pinned") && lObject.getString("is_pinned") != null)
                                    newLook.setIsPinned(lObject.getBoolean("is_pinned"));

                                if (lObject.has("is_liked") && lObject.getString("is_liked") != null)
                                    newLook.setIsLiked(lObject.getBoolean("is_liked"));

                                if (lObject.has("title") && lObject.getString("title") != null)
                                    newLook.setTitle(lObject.getString("title"));

                                if (lObject.has("created_at") && lObject.getString("created_at") != null)
                                    newLook.setCreatedAt(lObject.getString("created_at"));


                                if (lObject.has("media_type") && lObject.getString("media_type") != null) {
                                    if (lObject.getString("media_type").equals("image")) {
                                        newLook.setMediaType(Look.MediaType.Image);

                                    } else if (lObject.getString("media_type").equals("video")) {
                                        newLook.setMediaType(Look.MediaType.Video);
                                    }
                                }

                                if (lObject.has("comments_count"))
                                    newLook.setCommentsCount(lObject.getString("comments_count"));

                                if (lObject.has("likes_count") && lObject.getString("likes_count") != null)
                                    newLook.setLikesCount(lObject.getString("likes_count"));


                                if (lObject.has("views_count") && lObject.getString("views_count") != null)
                                    newLook.setViewsCount(lObject.getString("views_count"));


                                if (lObject.has("media_full") && lObject.getString("media_full") != null)
                                    newLook.setImageUrl(lObject.getString("media_full"));


                                if (lObject.has("user_location") && lObject.getJSONObject("user_location") != null) {
                                    JSONObject location = lObject.getJSONObject("user_location");
                                    Location loc = new Location();
                                    if (location.has("id") && location.getString("id") != null)
                                        loc.setId(location.getString("id"));

                                    if (location.has("lang") && location.has("lat")) {
                                        LatLng latlng = new LatLng(location.getDouble("lat"), location.getDouble("lang"));
                                        loc.setLatLng(latlng);
                                    }

                                    if (location.has("city") && location.getString("city") != null)
                                        loc.setCity(location.getString("city"));

                                    if (location.has("country") && location.getString("country") != null)
                                        loc.setCountry(location.getString("country"));


                                    if (location.has("district") && location.getString("district") != null)
                                        loc.setDistrict(location.getString("district"));

                                    newLook.setLocation(loc);
                                }

                                if (lObject.has("items") && lObject.getJSONArray("items") != null) {
                                    JSONArray items = lObject.getJSONArray("items");
                                    ArrayList<Item> itemsArrayList = new ArrayList<>();

                                    for (int j = 0; j < items.length(); j++) {
                                        Item item = new Item();
                                        JSONObject itemJsonObject = items.getJSONObject(j);

                                        if (itemJsonObject.has("id") && itemJsonObject.getString("id") != null)
                                            item.setId(itemJsonObject.getString("id"));

                                        if (itemJsonObject.has("x"))
                                            item.setX(itemJsonObject.getDouble("x"));


                                        if (itemJsonObject.has("y"))
                                            item.setY(itemJsonObject.getDouble("y"));


                                        if (itemJsonObject.has("price") && !itemJsonObject.getString("price").equals("null") && itemJsonObject.get("price")!=null){
                                            item.setPrice(itemJsonObject.getDouble("price"));

                                        }


                                        if (itemJsonObject.has("brand") && itemJsonObject.getString("brand") != null)
                                            item.setBrand(itemJsonObject.getString("brand"));


                                        if (itemJsonObject.has("location") && itemJsonObject.getJSONObject("location") != null) {
                                            JSONObject location = itemJsonObject.getJSONObject("location");
                                            Location loc = new Location();
                                            if (location.has("id") && location.getString("id") != null)
                                                loc.setId(location.getString("id"));

                                            if (location.has("lang") && location.has("lat")) {
                                                LatLng latlng = new LatLng(location.getDouble("lat"), location.getDouble("lang"));
                                                loc.setLatLng(latlng);
                                            }

                                            if (location.has("city") && location.getString("city") != null)
                                                loc.setCity(location.getString("city"));

                                            if (location.has("country") && location.getString("country") != null)
                                                loc.setCountry(location.getString("country"));


                                            if (location.has("district") && location.getString("district") != null)
                                                loc.setDistrict(location.getString("district"));

                                            item.setLocation(loc);

                                        }


                                        itemsArrayList.add(item);
                                    }

                                    newLook.setItems(itemsArrayList);

                                }

                                if (lObject.has("user") && lObject.getJSONObject("user") != null) {
                                    JSONObject userJsonObject = lObject.getJSONObject("user");
                                    User user = new User();

                                    if (userJsonObject.has("id") && userJsonObject.getString("id") != null)
                                        user.setUserID(userJsonObject.getString("id"));


                                    if (userJsonObject.has("username") && userJsonObject.getString("username") != null)
                                        user.setUsername(userJsonObject.getString("username"));

                                    if (userJsonObject.has("is_followed") && userJsonObject.getString("is_followed") != null)
                                        user.setIsfollowed(userJsonObject.getBoolean("is_followed"));


                                    if (userJsonObject.has("age"))
                                        user.setAge(userJsonObject.getInt("age"));

                                    if (userJsonObject.has("gender") && userJsonObject.getString("gender") != null)
                                        user.setGender(userJsonObject.getString("gender"));

                                    if (userJsonObject.has("email") && userJsonObject.getString("email") != null)
                                        user.setEmail(userJsonObject.getString("email"));

                                    if (userJsonObject.has("full_name") && userJsonObject.getString("full_name") != null)
                                        user.setName(userJsonObject.getString("full_name"));

                                    if (userJsonObject.has("photo") && userJsonObject.getString("photo") != null)
                                        user.setImageUrl(userJsonObject.getString("photo"));

                                    if (userJsonObject.has("followers_count") && userJsonObject.getString("followers_count") != null)
                                        user.setFollowersCount(userJsonObject.getString("followers_count"));

                                        newLook.setUser(user);
                                }


                                looks.add(newLook);
                            }
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }


                        looksResult.looks = looks;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return looksResult;
            }
        };

    }


    @Override
    protected HashMap<String, String> getQueryStringParams() {

        return body.getQueryParams();
    }
}
