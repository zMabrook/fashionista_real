package com.zmabrook.fashionista.Domain.Mapper;

import android.content.Context;

import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.PeopleSearchResult;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Interfaces.Parser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by matrix on 13/08/2016.
 */
public class PeopleMapper extends AbstractWebserviceMapper {
    public static final String trendingPeopleTag  = "trendingPeopleTag";
    public static final String featuredPeopleTag  = "featuredPeopleTag";
    public static final String getFollowersTag  = "getFollowersTag";
    public static final String getFollowingTag  = "getFollowingTag";
    public static final String getBlockedTag  = "getBlockedTag";


    public PeopleMapper(Context Context, int action) {
        super(Context, action);
    }

    @Override
    protected String getUrl() {
        switch (action) {
            case CommonConstants.TRENDING_PEOPLE_ACTION:
                return "api/v1/people/trended";
            case CommonConstants.FEATURED_PEOPLE_ACTION:
                return "api/v1/people/featured";
            case CommonConstants.GET_MY_FOLLOWERS_ACTION:
                return "api/v1/profile/followers";
            case CommonConstants.GET_MY_FOLLOWING_ACTION:
                return "api/v1/profile/following";
            case CommonConstants.GET_MY_BLOCKED_ACTION:
                return "api/v1/profile/blocked";
        }
        return "api/v1/people/trended";
    }

    @Override
    protected String initRequestTag() {
        switch (action) {
            case CommonConstants.TRENDING_PEOPLE_ACTION:
                return trendingPeopleTag;
            case CommonConstants.FEATURED_PEOPLE_ACTION:
                return featuredPeopleTag;

            case CommonConstants.GET_MY_FOLLOWERS_ACTION:
                return getFollowersTag;

            case CommonConstants.GET_MY_FOLLOWING_ACTION:
                return getFollowingTag;

            case CommonConstants.GET_MY_BLOCKED_ACTION:
                return getBlockedTag;
        }
    return trendingPeopleTag;
    }

    @Override
    protected void initParser() {
        parser = new Parser() {
            @Override
            public Entity parse(Object results) {
                return parseUser(results);
            }
        };

    }

    @Override
    protected HashMap<String, String> getQueryStringParams() {
        return body.getQueryParams();
    }

    private Entity parseUser(Object results){
        JSONObject object = (JSONObject) results;
        PeopleSearchResult peopleSearchResult = new PeopleSearchResult();
        ArrayList<User> usersList;


        try {
            if (object.has("count")&object.getDouble("count")!=0){
                peopleSearchResult.setNumOfPeople(object.getDouble("count"));
            }else {
                return peopleSearchResult;
            }

            if (object.has("pages")&object.getDouble("pages")!=0){
                peopleSearchResult.setNumOfPages(object.getDouble("pages"));

            }else {
                return peopleSearchResult;
            }


            if (object.has("users")&object.getJSONArray("users")!=null){
                usersList = new ArrayList<>();
                JSONArray brands = object.getJSONArray("users");
                User user;
                for (int i=0 ; i< brands.length() ; i++){
                    user= new User();

                    JSONObject brandObject = brands.getJSONObject(i);

                    if (brandObject.has("id")&& brandObject.getString("id")!=null){
                        user.setUserID(brandObject.getString("id"));

                    }else {continue;}

                    if (brandObject.has("full_name")&& brandObject.getString("full_name")!=null){
                        user.setName(brandObject.getString("full_name"));

                    }else {
                        continue;}


                    if (brandObject.has("photo")&& brandObject.getString("photo")!=null){
                        user.setImageUrl(brandObject.getString("photo"));

                    }

                    if (brandObject.has("is_followed")&& brandObject.getString("is_followed")!=null){
                        user.setIsfollowed(brandObject.getBoolean("is_followed"));

                    }

                    if (brandObject.has("bio")&& brandObject.getString("bio")!=null && brandObject.getString("bio")!="null" ){
                        user.setBio(brandObject.getString("bio"));
                    }

                    usersList.add(user);
                }
                peopleSearchResult.setPeopleList(usersList);
            }

        }catch (Exception e){
            e.printStackTrace();
        }





        return  peopleSearchResult;
    }

}
