package com.zmabrook.fashionista.Domain.Mapper.RequestStack;

import com.zmabrook.fashionista.Fashionista.FashionistaRequest;

import java.util.ArrayList;

/**
 * RequestStack
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Mapper
 * @since 7/25/16
 **/
public class RequestStack {
    static private ArrayList<FashionistaRequest> requestStack;



    public static ArrayList<FashionistaRequest> getRequestStack() {
        if (requestStack == null){
            requestStack = new ArrayList<>();
        }
        return requestStack;
    }

    public static void pushToRequestStack(FashionistaRequest request) {
        if (getRequestStack().size() >10){
            getRequestStack().remove(0);
        }
      requestStack.add(request);
    }

    public static FashionistaRequest  popFromRequestStack( ) {
        if (getRequestStack().size() !=0){
            FashionistaRequest request = getRequestStack().get(requestStack.size()-1);
            return request;
        }
        return null;
    }





}
