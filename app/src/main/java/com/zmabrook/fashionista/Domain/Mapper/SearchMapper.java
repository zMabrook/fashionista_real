package com.zmabrook.fashionista.Domain.Mapper;

import android.content.Context;

import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Brand;
import com.zmabrook.fashionista.Domain.Entities.BrandSearchResult;
import com.zmabrook.fashionista.Domain.Entities.Category;
import com.zmabrook.fashionista.Domain.Entities.CategorySearchResult;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.LookType;
import com.zmabrook.fashionista.Domain.Entities.PeopleSearchResult;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Interfaces.Parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * SearchMapper
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Mapper
 * @since 8/8/16
 **/
public class SearchMapper extends AbstractWebserviceMapper {
    public static final String searchTag  = "searchTag";

    public SearchMapper(Context Context, int action) {
        super(Context, action);
    }

    @Override
    protected String getUrl() {
        switch (action) {
            case CommonConstants.CATEGORIES_SEARCH_ACTION:
                return "api/v1/categories";
            case CommonConstants.BRANDS_SEARCH_ACTION:
                return "api/v1/brands";

            case CommonConstants.PEOPLE_SEARCH_ACTION:
                return "/api/v1/people";

            case CommonConstants.SUB_CATEGORIES_SEARCH_ACTION:
                return "/api/v1/categories/"+body.getPathParams().get("id");

            case CommonConstants.LOOK_TYPES_SEARCH_ACTION:
                return "/api/v1/look_types";
        }
        return "api/v1/categories";
    }

    @Override
    protected String initRequestTag() {
        return searchTag;
    }

    @Override
    protected void initParser() {
        parser =new Parser() {
            @Override
            public Entity parse(Object results) {
                switch (action) {
                    case CommonConstants.CATEGORIES_SEARCH_ACTION:
                        return parseCategory(results);
                    case CommonConstants.BRANDS_SEARCH_ACTION:
                        return parseBrand(results);
                    case CommonConstants.PEOPLE_SEARCH_ACTION:
                        return parseUser(results);
                    case CommonConstants.SUB_CATEGORIES_SEARCH_ACTION:
                        return parseSubCategory(results);
                    case CommonConstants.LOOK_TYPES_SEARCH_ACTION:
                        return parseLookType(results);

                }
                return null;
            }
        };

    }

    @Override
    protected HashMap<String, String> getQueryStringParams() {
        return body.getQueryParams();
    }


    private Entity parseCategory(Object results){
        Category category = new Category();
        JSONObject object = (JSONObject) results;

        try {

            if (object.has("id")&& object.getString("id")!=null){
                category.setId(object.getString("id"));

            }else {return null;}

            if (object.has("title")&& object.getString("title")!=null){
                category.setName(object.getString("title"));

            }else {return null;}


            if (object.has("icon")&& object.getString("icon")!=null){
                category.setIconUrl(object.getString("icon"));

            }else {return null;}


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return category;

    }


    private Entity parseBrand(Object results){
        JSONObject object = (JSONObject) results;
        BrandSearchResult brandSearchResult = new BrandSearchResult();
        ArrayList<Brand>  brandsList;
        try {
            if (object.has("count")&object.getDouble("count")!=0){
                brandSearchResult.setNumOfBrands(object.getDouble("count"));
            }else {
                return null;
            }

            if (object.has("pages")&object.getDouble("pages")!=0){
                brandSearchResult.setNumOfPages(object.getDouble("pages"));

            }else {
                return null;
            }

            if (object.has("brands")&object.getJSONArray("brands")!=null){
                brandsList = new ArrayList<>();
                JSONArray brands = object.getJSONArray("brands");
                Brand brand;
                for (int i=0 ; i< brands.length() ; i++){
                    brand= new Brand();

                    JSONObject brandObject = brands.getJSONObject(i);

                    if (brandObject.has("id")&& brandObject.getString("id")!=null){
                        brand.setId(brandObject.getString("id"));

                    }else {continue;}

                    if (brandObject.has("title")&& brandObject.getString("title")!=null){
                        brand.setName(brandObject.getString("title"));

                    }else {continue;}


                    if (brandObject.has("image")&& brandObject.getString("image")!=null){
                        brand.setIconUrl(brandObject.getString("image"));

                    }else {continue;}


                    brandsList.add(brand);
                }
                brandSearchResult.setBrands(brandsList);
            }






        } catch (JSONException e) {
            e.printStackTrace();
        }

        return brandSearchResult;
    }

    private Entity parseUser(Object results){
        JSONObject object = (JSONObject) results;
        PeopleSearchResult peopleSearchResult = new PeopleSearchResult();
        ArrayList<User>  usersList;


        try {
            if (object.has("count")&object.getDouble("count")!=0){
                peopleSearchResult.setNumOfPeople(object.getDouble("count"));
            }else {
                return null;
            }

            if (object.has("pages")&object.getDouble("pages")!=0){
                peopleSearchResult.setNumOfPages(object.getDouble("pages"));

            }else {
                return null;
            }


            if (object.has("users")&object.getJSONArray("users")!=null){
                usersList = new ArrayList<>();
                JSONArray brands = object.getJSONArray("users");
                User user;
                for (int i=0 ; i< brands.length() ; i++){
                    user= new User();

                    JSONObject brandObject = brands.getJSONObject(i);

                    if (brandObject.has("id")&& brandObject.getString("id")!=null){
                        user.setUserID(brandObject.getString("id"));

                    }else {continue;}

                    if (brandObject.has("full_name")&& brandObject.getString("full_name")!=null){
                        user.setName(brandObject.getString("full_name"));

                    }else {continue;}


                    if (brandObject.has("photo")&& brandObject.getString("photo")!=null){
                        user.setImageUrl(brandObject.getString("photo"));

                    }

                    if (brandObject.has("is_followed")&& brandObject.getString("is_followed")!=null){
                        user.setIsfollowed(brandObject.getBoolean("is_followed"));

                    }


                    usersList.add(user);
                }
                peopleSearchResult.setPeopleList(usersList);
            }








        }catch (Exception e){
            e.printStackTrace();
        }





        return  peopleSearchResult;
    }

    private Entity parseSubCategory(Object results){
        ArrayList<Category> categoryArrayLis = new ArrayList<>();
        Category category ;


        try {
            JSONArray objects = ((JSONObject) results).getJSONArray("sub_categories");

            for (int i = 0 ; i <objects.length() ;i++){
                JSONObject object =objects.getJSONObject(i);
                category = new Category();
                if (object.has("id")&& object.getString("id")!=null){
                    category.setId(object.getString("id"));

                }else {return null;}

                if (object.has("title")&& object.getString("title")!=null){
                    category.setName(object.getString("title"));

                }else {return null;}


                if (object.has("icon")&& object.getString("icon")!=null){
                    category.setIconUrl(object.getString("icon"));

                }else {return null;}
                category.setIsSub(true);

                categoryArrayLis.add(category);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


        CategorySearchResult categorySearchResult = new CategorySearchResult();
        categorySearchResult.setCategories(categoryArrayLis);
        return categorySearchResult;

    }

    private Entity parseLookType(Object results){
        LookType lookType =new LookType();
        JSONObject object  = (JSONObject)results;

        try {

            if (object.has("id")&& object.getString("id")!=null){
                lookType.setId(object.getString("id"));

            }else {return null;}

            if (object.has("name")&& object.getString("name")!=null){
                lookType.setName(object.getString("name"));

            }else {return null;}


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return lookType;

    }

}
