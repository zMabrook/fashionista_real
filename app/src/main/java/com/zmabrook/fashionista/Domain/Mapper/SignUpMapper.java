package com.zmabrook.fashionista.Domain.Mapper;

import android.content.Context;

import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.SignUpResult;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Interfaces.Parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * SignUpMapper
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Mapper
 * @since 7/25/16
 **/
public class SignUpMapper extends AbstractWebserviceMapper {
    public static final String signUpTag = "SignUpTag";
    public static final String loginTag = "loginTag";

    public SignUpMapper(Context Context, int action) {
        super(Context, action);
    }

    @Override
    protected String getUrl() {
        switch (action) {
            case CommonConstants.SIGN_UP_ACTION:
                return "api/v1/auth";


            case CommonConstants.LOGIN_ACTION:
                return "api/v1/auth/sign_in";

        }
        return "api/v1/auth";

    }

    @Override
    protected String initRequestTag() {
        switch (action) {
            case CommonConstants.SIGN_UP_ACTION:
                return signUpTag;


            case CommonConstants.LOGIN_ACTION:
                return loginTag;

        }

        return signUpTag;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject results) {

                try {
                    if (results == null){
                        return null;
                    }
                    if (results.has("errors")) {
                        SignUpResult signUpResult = new SignUpResult();
                        StringBuilder message = new StringBuilder();
                        JSONArray errors;
                        switch (action) {
                            case CommonConstants.SIGN_UP_ACTION:
                                errors = results.getJSONObject("errors").getJSONArray("full_messages");

                                for (int i = 0; i < errors.length(); i++) {
                                    message.append((String) errors.get(i));
                                    if (errors.length() - 1 != i) {
                                        message.append("\n");
                                    }
                                    signUpResult.Errormessages = message.toString();


                                }
                                return signUpResult;

                            case CommonConstants.LOGIN_ACTION:
                                errors = results.getJSONArray("errors");

                                for (int i = 0; i < errors.length(); i++) {
                                    message.append((String) errors.get(i));
                                    if (errors.length() - 1 != i) {
                                        message.append("\n");
                                    }
                                    signUpResult.Errormessages = message.toString();

                                }
                                return signUpResult;

                        }


                    } else {
                        User user = null;
                        if (results.has("data")) {
                            user = new User();

                            //TODO: add conditins to handle null and existance
                            user.setUserID(results.getJSONObject("data").getString("id"));
                            user.setUsername(results.getJSONObject("data").getString("username"));
                            user.setAge(results.getJSONObject("data").getInt("age"));
                            user.setGender(results.getJSONObject("data").getString("gender"));
                            user.setEmail(results.getJSONObject("data").getString("email"));
                            user.setName(results.getJSONObject("data").getString("full_name"));

                        }
                        return user;

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }
        };

    }

    @Override
    protected HashMap<String, String> getQueryStringParams() {
        HashMap<String, String> map = new HashMap<>();

        return map;
    }
}
