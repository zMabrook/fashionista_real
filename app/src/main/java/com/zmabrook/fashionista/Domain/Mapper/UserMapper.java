package com.zmabrook.fashionista.Domain.Mapper;

import android.content.Context;

import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Interfaces.Parser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * UserMapper
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Mapper
 * @since 8/15/16
 **/
public class UserMapper extends AbstractWebserviceMapper {

    public static final String getUserDataTag = "getUserDataTag";
    public static final String getMyProfileDataTag = "getMyProfileDataTag";
    public static final String toggleNotificationsTag = "ToggleNotifications";
    public static final String followTag = "followTag";
    public static final String unfollowTag = "unfollowTag";
    public static final String updateUserData = "updateUserData";
    public static final String blockuserTag = "blockuserTag";
    public static final String unblockuserTag = "unblockuserTag";

    public UserMapper(Context Context, int action) {
        super(Context, action);
    }

    @Override
    protected String getUrl() {
        switch (action) {
            case CommonConstants.GET_USER_ACTION:
                return "api/v1/users/"+body.getPathParams().get("id");

            case CommonConstants.GET_MY_PROFILE_DATA_ACTION:
                return "api/v1/profile";

            case CommonConstants.TOGGLE_FOLLOW_NOTIFICATIONS:
                return "api/v1/users/notification_toggle";

            case CommonConstants.FOLLOW_USER_ACTION:
                return "api/v1/users/"+body.getPathParams().get("id")+"/follow";
            case CommonConstants.UNFOLLOW_USER_ACTION:
                return "api/v1/users/"+body.getPathParams().get("id")+"/unfollow";
            case CommonConstants.UPDATE_USER_DATA_ACTION:
                return "api/v1/profile";
            case CommonConstants.BLOCK_USER_ACTION:
                return "api/v1/users/"+body.getPathParams().get("id")+"/block";
            case CommonConstants.UNBLOCK_USER_ACTION:
                return "api/v1/users/"+body.getPathParams().get("id")+"/unblock";
        }

        return "api/v1/users/";
    }

    @Override
    protected String initRequestTag() {
        switch (action) {
            case CommonConstants.GET_USER_ACTION:
                return getUserDataTag;
            case CommonConstants.GET_MY_PROFILE_DATA_ACTION:
                return getMyProfileDataTag;
            case CommonConstants.TOGGLE_FOLLOW_NOTIFICATIONS:
                return toggleNotificationsTag;
            case CommonConstants.FOLLOW_USER_ACTION:
                return followTag;
            case CommonConstants.UNFOLLOW_USER_ACTION:
                return unfollowTag;
            case CommonConstants.UPDATE_USER_DATA_ACTION:
                return updateUserData;
            case CommonConstants.BLOCK_USER_ACTION:
                return blockuserTag;
            case CommonConstants.UNBLOCK_USER_ACTION:
                return unblockuserTag;
        }

        return getUserDataTag;
    }


    @Override
    protected void initParser() {
        parser = new Parser() {
            @Override
            public Entity parse(Object results) {
                User user = new User();

                JSONObject userJsonObject = (JSONObject) results;

                try {
                    if (userJsonObject.has("id") && userJsonObject.getString("id") != null) {
                        user.setUserID(userJsonObject.getString("id"));

                    } else {
                        return null;
                    }


                    if (userJsonObject.has("full_name") && userJsonObject.getString("full_name") != null) {
                        user.setName(userJsonObject.getString("full_name"));

                    } else {
                        return null;
                    }


                    if (userJsonObject.has("photo") && userJsonObject.getString("photo") != null) {
                        user.setImageUrl(userJsonObject.getString("photo"));

                    }

                    if (userJsonObject.has("is_followed") && userJsonObject.getString("is_followed") != null) {
                        user.setIsfollowed(userJsonObject.getBoolean("is_followed"));

                    }


                    if (userJsonObject.has("username") && userJsonObject.getString("username") != null) {
                        user.setUsername(userJsonObject.getString("username"));

                    }

                    if (userJsonObject.has("cover_photo") && userJsonObject.getString("cover_photo") != null) {
                        user.setCoverImageUrl(userJsonObject.getString("cover_photo"));

                    }

                    if (userJsonObject.has("bio") && userJsonObject.getString("bio") != null && userJsonObject.getString("bio") != "null") {
                        user.setBio(userJsonObject.getString("bio"));
                    }

                    if (userJsonObject.has("followers_count") && userJsonObject.getString("followers_count") != null && userJsonObject.getString("followers_count") != "null") {
                        user.setFollowersCount(userJsonObject.getString("followers_count"));
                    }

                    if (userJsonObject.has("following_count") && userJsonObject.getString("following_count") != null && userJsonObject.getString("following_count") != "null") {
                        user.setFollowingCount(userJsonObject.getString("following_count"));
                    }

                    if (userJsonObject.has("looks_count") && userJsonObject.getString("looks_count") != null && userJsonObject.getString("looks_count") != "null") {
                        user.setLooksCount(userJsonObject.getString("looks_count"));
                    }

                    if (userJsonObject.has("likes_count") && userJsonObject.getString("likes_count") != null && userJsonObject.getString("likes_count") != "null") {
                        user.setLikesCount(userJsonObject.getString("likes_count"));
                    }

                    if (userJsonObject.has("views_count") && userJsonObject.getString("views_count") != null && userJsonObject.getString("views_count") != "null") {
                        user.setViewsCount(userJsonObject.getString("views_count"));
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                return user;
            }
        };

    }

    @Override
    protected HashMap<String, String> getQueryStringParams() {
        return body.getQueryParams();
    }
}
