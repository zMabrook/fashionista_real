package com.zmabrook.fashionista.Domain.Mapper.VolleyQueue;

import android.content.Context;

import com.android.volley.*;
import com.zmabrook.fashionista.Domain.Mapper.RequestStack.RequestStack;
import com.zmabrook.fashionista.Fashionista.FashionistaRequest;

/**
 * QueueWrapper
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Mapper.VolleyQueue
 * @since 7/23/16
 **/
public class QueueWrapper {


    com.android.volley.RequestQueue requestQueue;

    public QueueWrapper(Context context) {
        RequestQueue.context = context;
        requestQueue = RequestQueue.getRequestQueue();

    }


    /**
     *
     * @param req the request to be added to the queue
     * @param tag the tag of the request
     */
    public void addToRequestQueue(Request req, String tag) {
        // set the default tag if tag is empty
        RequestStack.pushToRequestStack(new FashionistaRequest(req,tag));
        requestQueue.cancelAll(tag);
        req.setTag(tag);
        requestQueue.add(req);
    }



    /**
     *
     * @param req the request to be added to the queue
     * @param tag the tag of the request
     */
    public void addToRequestQueueWithoutCanceling(Request req, String tag) {
        // set the default tag if tag is empty

        req.setTag(tag);
        requestQueue.add(req);
    }
    /**
     *
     * @param tag the tag of request that will be canceled
     */

    public void cancelPendingRequests(String tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }

    /**
     * cancel all request in the queue
     */
    public void cancelAllRequests() {

        if (requestQueue != null) {
            requestQueue.cancelAll(new com.android.volley.RequestQueue.RequestFilter() {
                @Override
                public boolean apply(Request<?> request) {
                    return true;
                }
            });
        }
    }
}
