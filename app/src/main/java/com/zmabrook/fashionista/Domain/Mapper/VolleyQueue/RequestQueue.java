package com.zmabrook.fashionista.Domain.Mapper.VolleyQueue;

import android.content.Context;

import com.android.volley.toolbox.Volley;

/**
 * RequestQueue
 * <br/> this class is the singleton class for the requests queue
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Domain.Mapper.VolleyQueue
 * @since 7/23/16
 **/
public class RequestQueue {

    private static com.android.volley.RequestQueue mRequestsQueue = null;
    public static Context context;

    /**
     *
     * @return the singleton object of the queue
     */
    public static com.android.volley.RequestQueue getRequestQueue() {
        if (mRequestsQueue == null) {
            mRequestsQueue = Volley.newRequestQueue(context);
        }
        return mRequestsQueue;
    }
}
