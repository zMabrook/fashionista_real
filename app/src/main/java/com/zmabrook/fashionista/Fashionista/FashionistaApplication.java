package com.zmabrook.fashionista.Fashionista;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;

import com.zmabrook.fashionista.R;

/**
 * FashionistaApplication
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views
 * @since 7/19/16
 **/
public class FashionistaApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }

}
