package com.zmabrook.fashionista.Fashionista;

import android.content.Context;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

/**
 * FashionistaError
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista
 * @since 7/25/16
 **/
public class FashionistaError extends VolleyError {
    boolean isConnectedToInternet;

    public FashionistaError(boolean isConnectedToInternet ,Context context){
        super();
        this.isConnectedToInternet = isConnectedToInternet;
        if (isConnectedToInternet==false){
            Toast toast = Toast.makeText(context, "No network connection. Please check your network", Toast.LENGTH_SHORT);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            if( v != null) v.setGravity(Gravity.CENTER);
            toast.show();
        }
    }

}
