package com.zmabrook.fashionista.Fashionista;

import com.android.volley.Request;

/**
 * FashionistaRequest
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista
 * @since 7/25/16
 **/
public class FashionistaRequest {
    public Request request;
    public String tag;

    public FashionistaRequest(Request request, String tag) {
        this.request = request;
        this.tag = tag;
    }
}
