package com.zmabrook.fashionista.Interfaces;

import com.zmabrook.fashionista.Domain.Entities.Entity;

/**
 * Parser
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Interfaces
 * @since 7/25/16
 **/
public interface Parser<T>{

    Entity parse (T results);
}
