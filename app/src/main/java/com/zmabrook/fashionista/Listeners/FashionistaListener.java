package com.zmabrook.fashionista.Listeners;

import com.android.volley.VolleyError;

/**
 * FashionistaListener
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Listeners
 * @since 7/23/16
 **/
public interface FashionistaListener<T> {

    void onSuccess (T result);

    void onError (Exception e , T result);
}