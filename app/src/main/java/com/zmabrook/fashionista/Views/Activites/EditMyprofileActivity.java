package com.zmabrook.fashionista.Views.Activites;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.UserMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.MyProfile.MyProfileActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class EditMyprofileActivity extends AppCompatActivity {

    @BindView(R.id.toolbarEditImageview)
    ImageView editCoverPhotoImageView;

    @BindView(R.id.coverImageView)
    ImageView coverImageView;

    @BindView(R.id.toolbarDonemageView)
    ImageView doneImageView;

    @BindView(R.id.avatarRecView)
    CardView avatarContainer;

    @BindView(R.id.avatarImageView)
    CircleImageView avatarImageView;

    @BindView(R.id.fullNameEditText)
    EditText fullNameEditText;

    @BindView(R.id.usernameEditText)
    TextView usernameTextView;

    @BindView(R.id.bioEditText)
    EditText bioEditText;
    User user;
    final int OPEN_GALLERY_FOR_AVATAR = 101;
    final int OPEN_GALLERY_FOR_COVER = 102;


    Target avatarTarget = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            avatarImageView.setImageBitmap(bitmap);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if (bitmap.getByteCount()<2000){
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object

            }else{
                bitmap.compress(Bitmap.CompressFormat.JPEG, 60, baos);
            }
            byte[] byteArrayImage = baos.toByteArray();


            String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
            encodedImage = "data:image/jpeg;base64," + encodedImage;
            user.setImageUrl(encodedImage);

        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };
    Target coverTarget = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            coverImageView.setImageBitmap(bitmap);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if (bitmap.getByteCount()<2000){
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object

            }else{
                bitmap.compress(Bitmap.CompressFormat.JPEG, 60, baos);
            }
            byte[] byteArrayImage = baos.toByteArray();


            String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
            encodedImage = "data:image/jpeg;base64," + encodedImage;
            user.setCoverImageUrl(encodedImage);

        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_myprofile);
        ButterKnife.bind(this);
        fullNameEditText.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        bioEditText.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        usernameTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));


        if (getIntent().getExtras() != null)
            user = (User) getIntent().getExtras().getSerializable(CommonConstants.PREFS_USER);
        if (user != null) {
            loadUser(user);
        } else {
            //make request
            final RequestBody requestBody = new RequestBody();
            final UserMapper userMapper = new UserMapper(this, CommonConstants.GET_MY_PROFILE_DATA_ACTION);
            userMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener() {
                @Override
                public void onSuccess(Object result) {
                    user = (User) result;
                    loadUser(user);
                }

                @Override
                public void onError(Exception e, Object result) {
                    Toast.makeText(EditMyprofileActivity.this, "Failed to load. Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }, requestBody);
        }


        doneImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //make data update request
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(EditMyprofileActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                boolean isValid = validateData();
                if (isValid) {
                    user.setName(fullNameEditText.getText().toString());

                    if (!bioEditText.getText().toString().equals(""))
                        user.setBio(bioEditText.getText().toString());

                    //make request here
                    RequestBody requestBody = new RequestBody();
                    JSONObject userObject = new JSONObject();
                    JSONObject object = new JSONObject();
                    try {
                        if (user.getBio() != null)
                            object.put("bio", user.getBio());
                        if (user.getName() !=null)
                            object.put("full_name", user.getName());
                        if (user.getImageUrl() !=null && user.getImageUrl().contains("base64"))
                            object.put("avatar", user.getImageUrl());
                        if (user.getCoverImageUrl() !=null && user.getCoverImageUrl().contains("base64"))
                            object.put("cover_photo", user.getCoverImageUrl());

                        userObject.put("user",object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    requestBody.setJsonObjectRequestBody(userObject);
                    UserMapper userMapper = new UserMapper(EditMyprofileActivity.this, CommonConstants.UPDATE_USER_DATA_ACTION);
                    userMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.PUT, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            Intent i = new Intent(EditMyprofileActivity.this, MyProfileActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
                            startActivity(i);

                            finish();
                            Toast.makeText(EditMyprofileActivity.this, "DataUpdated", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            Toast.makeText(EditMyprofileActivity.this,"Something went wrong please try again later",Toast.LENGTH_LONG).show();

                        }
                    }, requestBody);


                }
            }
        });

        avatarContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, OPEN_GALLERY_FOR_AVATAR);//one can be replaced with any action code
            }
        });

        editCoverPhotoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, OPEN_GALLERY_FOR_COVER);//one can be replaced with any action code
            }
        });

    }


    private void loadUser(final User user) {

        if (user.getName() != null) {
            fullNameEditText.setText(user.getName(), TextView.BufferType.EDITABLE);
        }

        if (user.getUsername() != null) {
            usernameTextView.setText(user.getUsername());
        }

        if (user.getBio() != null) {
            bioEditText.setText(user.getBio(), TextView.BufferType.EDITABLE);
        }

        if (user.getImageUrl() != null) {
            Picasso.with(EditMyprofileActivity.this)
                    .load(user.getImageUrl())
                    .into(avatarImageView);
        }


        if (user.getCoverImageUrl() != null) {
            Picasso.with(EditMyprofileActivity.this)
                    .load(user.getCoverImageUrl())
                    .into(coverImageView);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case OPEN_GALLERY_FOR_AVATAR:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();

                    Picasso.with(this)
                            .load(selectedImage)
                            .error(R.mipmap.avatar)
                            .into(avatarTarget);
                }
                break;
            case OPEN_GALLERY_FOR_COVER:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    user.setCoverImageUrl(selectedImage.toString());


                    Picasso.with(this)
                            .load(selectedImage)
                            .into(coverTarget);
                }
                break;
        }
    }

    private boolean validateData() {
        boolean isValid = true;
        if (fullNameEditText.getText().toString().equals("") || usernameTextView.toString().equals(""))
            isValid = false;

        if (fullNameEditText.getText().toString().length() > 100) {
            Toast.makeText(EditMyprofileActivity.this, getString(R.string.fullNameLimit), Toast.LENGTH_LONG).show();
            isValid = false;
        }

        return isValid;
    }
}
