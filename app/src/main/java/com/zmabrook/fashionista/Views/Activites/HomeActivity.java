package com.zmabrook.fashionista.Views.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.Abstract.FashionistaActivity;
import com.zmabrook.fashionista.Views.Activites.MyProfile.MyProfileActivity;
import com.zmabrook.fashionista.Views.Activites.NewLook.NewLookActivity;
import com.zmabrook.fashionista.Views.Adapters.HomePagerViewAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends FashionistaActivity {

    @BindView(R.id.homePager)
    ViewPager viewPager;

    @BindView(R.id.toolbarSearchImageview)
    ImageView searchbutton;

    @BindView(R.id.toolbarSearchImageview2)
    ImageView searchbutton2;

    @BindView(R.id.toolbarMapImageView)
    ImageView mapImageview;

    @BindView(R.id.toolbarListImageview)
    ImageView listImageView;

    @BindView(R.id.trendingButton)
    ImageView trendingImageView;

    @BindView(R.id.peopleButton)
    ImageView peopleImageView;

    @BindView(R.id.myFashionButton)
    ImageView myFashionImageView;


    @BindView(R.id.homeToolbar)
    Toolbar homeToolbar;

    @BindView(R.id.peopletoolbar)
    Toolbar peopleToolbar;

    @BindView(R.id.myFashionToolbar)
    Toolbar myFashionToolbar;

    @BindView(R.id.toolbarprofileImageView)
    ImageView toolbarprofileImageView;


    @BindView(R.id.toolbarprofileImageView2)
    ImageView myfashionToolbarprofileImageView;

    @BindView(R.id.fabbutton)
    FloatingActionButton fabbutton;


    User user;
    HomePagerViewAdapter mAdapter;

    public HomeActivity() {


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        peopleToolbar.setVisibility(View.GONE);
        myFashionToolbar.setVisibility(View.GONE);

        SharedPreferences settings = getApplicationContext().getSharedPreferences(CommonConstants.PREFS_NAME, Context.MODE_PRIVATE);

        Gson gson = new Gson();
        String json = settings.getString(CommonConstants.PREFS_USER, "");
        user = gson.fromJson(json, User.class);


        mAdapter = new HomePagerViewAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setCurrentItem(2, true);


        toolbarprofileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MyProfileActivity.class);
                startActivity(intent);
            }
        });

        myfashionToolbarprofileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this,MyProfileActivity.class);
                startActivity(intent);
            }
        });

        listImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(2, true);

                listImageView.setImageResource(R.mipmap.icontrendingliston);
                mapImageview.setImageResource(R.mipmap.icontrendingmapoff);

            }
        });

        mapImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(3, true);

                listImageView.setImageResource(R.mipmap.icontrendinglistoff);
                mapImageview.setImageResource(R.mipmap.icontrendingmapon);
            }
        });


        myFashionImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(0, true);

            }
        });


        peopleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(1, true);

            }
        });

        trendingImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(2, true);

            }
        });


        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:

                        trendingImageView.setImageResource(R.mipmap.icontrendingoff);
                        peopleImageView.setImageResource(R.mipmap.iconpeopleoff);
                        myFashionImageView.setImageResource(R.mipmap.iconmyfashionon);

                        myFashionToolbar.setVisibility(View.VISIBLE);
                        peopleToolbar.setVisibility(View.GONE);
                        homeToolbar.setVisibility(View.GONE);
                        break;
                    case 1:
                        trendingImageView.setImageResource(R.mipmap.icontrendingoff);
                        peopleImageView.setImageResource(R.mipmap.iconpeopleon);
                        myFashionImageView.setImageResource(R.mipmap.iconmyfashionoff);

                        peopleToolbar.setVisibility(View.VISIBLE);
                        homeToolbar.setVisibility(View.GONE);
                        myFashionToolbar.setVisibility(View.GONE);

                        break;
                    case 2:
                        listImageView.setImageResource(R.mipmap.icontrendingliston);
                        mapImageview.setImageResource(R.mipmap.icontrendingmapoff);


                        trendingImageView.setImageResource(R.mipmap.icontrendingon);
                        peopleImageView.setImageResource(R.mipmap.iconpeopleoff);
                        myFashionImageView.setImageResource(R.mipmap.iconmyfashionoff);

                        homeToolbar.setVisibility(View.VISIBLE);
                        peopleToolbar.setVisibility(View.GONE);
                        myFashionToolbar.setVisibility(View.GONE);

                        break;
                    case 3:
                        listImageView.setImageResource(R.mipmap.icontrendinglistoff);
                        mapImageview.setImageResource(R.mipmap.icontrendingmapon);


                        trendingImageView.setImageResource(R.mipmap.icontrendingon);
                        peopleImageView.setImageResource(R.mipmap.iconpeopleoff);
                        myFashionImageView.setImageResource(R.mipmap.iconmyfashionoff);

                        homeToolbar.setVisibility(View.VISIBLE);
                        peopleToolbar.setVisibility(View.GONE);
                        myFashionToolbar.setVisibility(View.GONE);

                        break;

                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        searchbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, SearchActivity.class);
                startActivity(i);
            }
        });
        searchbutton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, SearchActivity.class);
                startActivity(i);
            }
        });

        fabbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this,NewLookActivity.class);
                startActivity(i);
            }
        });

    }
}
