package com.zmabrook.fashionista.Views.Activites;

import android.app.DownloadManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Item;
import com.zmabrook.fashionista.Domain.Entities.Look;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.LooksMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.NewLook.TagItemActivity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class LookViewActivity extends AppCompatActivity {

    @BindView(R.id.mainLayoutContainer)
    RelativeLayout mainLayoutContainer;

    @BindView(R.id.toBeHiddenPart)
    RelativeLayout toBeHiddenPart;

    @BindView(R.id.HomeItemImage)
    ImageView lookImage;

    @BindView(R.id.backImageview)
    ImageView backImageview;

    @BindView(R.id.settingsImageView)
    ImageView settingsImageView;

    @BindView(R.id.shareImageView)
    ImageView shareImageView;

    @BindView(R.id.likeImageView)
    ImageView likeImageView;

    @BindView(R.id.pinImageView)
    ImageView pinImageView;

    @BindView(R.id.smallAvatar)
    CircleImageView smallAvatar;

    @BindView(R.id.captionTextView)
    TextView captionTextView;

    @BindView(R.id.numOfFollowersTextView)
    TextView numOfFollowersTextView;

    @BindView(R.id.numOfLikesTextView)
    TextView numOfLikesTextView;

    @BindView(R.id.numOfCommentsTextView)
    TextView numOfCommentsTextView;

    @BindView(R.id.numOfViewsTextView)
    TextView numOfViewsTextView;

    ImageView tagImageView;

    boolean imageLoaded = false;

    Target lookImageTarget = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            lookImage.setImageBitmap(bitmap);
            imageLoaded = true;
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };
    public static Look look;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_look_view);
        ButterKnife.bind(this);

        backImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        numOfCommentsTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        numOfFollowersTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        numOfLikesTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        numOfViewsTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        captionTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));

        if (look == null) {
            finish();
            return;
        }


        if (look.getViewsCount() != null) {
            numOfViewsTextView.setText(look.getViewsCount());
        }
        if (look.getLikesCount() != null) {
            numOfLikesTextView.setText(look.getLikesCount());
        }


        if (look.isLiked()) {
            likeImageView.setImageResource(R.mipmap.iconlikeposteron);
        } else {
            likeImageView.setImageResource(R.mipmap.iconlikeposteroff);

        }

        if (look.isPinned()) {
            pinImageView.setImageResource(R.mipmap.iconpinposton);

        } else {
            pinImageView.setImageResource(R.mipmap.iconpinpostoff);

        }

        if (look.getCommentsCount() != null) {
            numOfCommentsTextView.setText(look.getCommentsCount());
        }

        if (look.getTitle() != null) {
            captionTextView.setText(look.getTitle());
        }


        if (look.getUser() != null && look.getUser().getFollowersCount() != null) {
            numOfFollowersTextView.setText(look.getUser().getFollowersCount());

        }

        settingsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downLoadFile();
            }

        });

        Picasso.with(this)
                .load(look.getUser().getImageUrl())
                .into(smallAvatar, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        Picasso.with(LookViewActivity.this)
                                .load(look.getUser().getImageUrl())
                                .into(smallAvatar);
                    }
                });


        pinImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (look.isPinned()) {
                    // unpin request

                    RequestBody requestBody = new RequestBody();
                    HashMap<String, String> pathParams = new HashMap<String, String>();
                    pathParams.put("id", look.getId());
                    requestBody.setPathParams(pathParams);

                    LooksMapper looksMapper = new LooksMapper(LookViewActivity.this, CommonConstants.UNPIN_LOOK_ACTION);
                    looksMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.DELETE, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            pinImageView.setImageResource(R.mipmap.iconpinpostoff);
                            look.setIsPinned(false);
                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            pinImageView.setImageResource(R.mipmap.iconpinposton);
                            look.setIsPinned(true);
                        }
                    }, requestBody);

                    pinImageView.setImageResource(R.mipmap.iconpinpostoff);
                } else {
                    // pin request

                    RequestBody requestBody = new RequestBody();
                    HashMap<String, String> pathParams = new HashMap<String, String>();
                    pathParams.put("id", look.getId());
                    requestBody.setPathParams(pathParams);

                    LooksMapper looksMapper = new LooksMapper(LookViewActivity.this, CommonConstants.PIN_LOOK_ACTION);
                    looksMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            pinImageView.setImageResource(R.mipmap.iconpinposton);
                            look.setIsPinned(true);
                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            pinImageView.setImageResource(R.mipmap.iconpinpostoff);
                            look.setIsPinned(false);
                        }
                    }, requestBody);

                    pinImageView.setImageResource(R.mipmap.iconpinposton);
                }
            }
        });
        likeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (look.isLiked()) {
                    //unlike request
                    RequestBody requestBody = new RequestBody();
                    HashMap<String, String> pathParams = new HashMap<String, String>();
                    pathParams.put("id", look.getId());
                    requestBody.setPathParams(pathParams);

                    LooksMapper looksMapper = new LooksMapper(LookViewActivity.this, CommonConstants.UNLIKE_LOOK_ACTION);
                    looksMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.DELETE, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            look.setIsLiked(false);
                            likeImageView.setImageResource(R.mipmap.iconlikeposteroff);

                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            look.setIsLiked(true);
                            likeImageView.setImageResource(R.mipmap.iconlikeposteron);
                        }
                    }, requestBody);
                    likeImageView.setImageResource(R.mipmap.iconlikeposteroff);

                } else {
                    //like request
                    RequestBody requestBody = new RequestBody();
                    HashMap<String, String> pathParams = new HashMap<String, String>();
                    pathParams.put("id", look.getId());
                    requestBody.setPathParams(pathParams);

                    LooksMapper looksMapper = new LooksMapper(LookViewActivity.this, CommonConstants.LIKE_LOOK_ACTION);
                    looksMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            look.setIsLiked(true);
                            likeImageView.setImageResource(R.mipmap.iconlikeposteron);

                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            look.setIsLiked(false);
                            likeImageView.setImageResource(R.mipmap.iconlikeposteroff);
                        }
                    }, requestBody);

                    likeImageView.setImageResource(R.mipmap.iconlikeposteron);

                }

            }
        });



        ArrayList<Item> items = look.getItems();
        if (items.size() != 0) {
            for (int i = 0; i < items.size(); i++) {
                Item item = items.get(i);

                tagImageView = new ImageView(LookViewActivity.this);

                tagImageView.setImageResource(R.mipmap.xytag);
                int tagDimension = (int) getResources().getDimension(R.dimen.tag);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) getResources().getDimension(R.dimen.tag), (int) getResources().getDimension(R.dimen.tag));
                params.leftMargin = (int) item.getX();
                params.topMargin = (int) item.getY();

                toBeHiddenPart.addView(tagImageView, params);

            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (look.getImageUrl() != null) {
            Picasso.with(this)
                    .load(look.getImageUrl())
                    .into(lookImage, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(LookViewActivity.this)
                                    .load(look.getImageUrl())
                                    .into(lookImageTarget);
                        }
                    });


        }

    }

    private void downLoadFile(){

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(look.getImageUrl()));
        final String filename = "fashionista"+ new Date().getTime();

        request.setDescription(filename);
        request.setTitle(filename);
        request.setVisibleInDownloadsUi(true);

        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename + ".jpeg");

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    //Show a notification
                    NotificationCompat.Builder mBuilder =   new NotificationCompat.Builder(LookViewActivity.this)
                            .setSmallIcon(R.mipmap.ic_launcher) // notification icon
                            .setContentTitle("Download Complete") // title for notification
                            .setContentText(filename+" downloaded successfully.") // message for notification
                            .setAutoCancel(true); // clear notification after click
                    Intent intent1 = new Intent(LookViewActivity.this, LookViewActivity.class);
                    PendingIntent pi = PendingIntent.getActivity(LookViewActivity.this, 0, intent1, 0);
                    mBuilder.setContentIntent(pi);
                    NotificationManager mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.notify(0, mBuilder.build());
                }
            }
        };
        registerReceiver(receiver, filter);
        Toast.makeText(LookViewActivity.this,"Download Started", Toast.LENGTH_LONG).show();

    }
}
