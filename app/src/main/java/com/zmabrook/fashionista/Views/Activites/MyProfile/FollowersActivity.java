package com.zmabrook.fashionista.Views.Activites.MyProfile;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.PeopleSearchResult;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.PeopleMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Adapters.PeopleRecyclerViewAdapter;
import com.zmabrook.fashionista.Views.Adapters.PeopleSearchRecyclerViewAdapter;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FollowersActivity extends AppCompatActivity {

    LinearLayoutManager linearLayoutManager;
    int mFirstVisibleItem, mVisibleItemCount, mTotalItemCount, mLastFirstVisibleItem;
    boolean isLoading = false;
    boolean gotToEnd = false;
    PeopleSearchResult result;
    public static int pageNumber = 1;
    PeopleSearchResult peopleSearchResult;
    PeopleSearchRecyclerViewAdapter adapter;


    @BindView(R.id.followersRecyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.trloadingBar)
    TextView loadingBar;

    @BindView(R.id.followToolbartitle)
    TextView toolbarTitle;

    @BindView(R.id.backImageview)
    ImageView backImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followers);
        ButterKnife.bind(this);
        showLoadingBar();

        toolbarTitle.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        toolbarTitle.setText("Followers");
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        peopleSearchResult = new PeopleSearchResult();


        HashMap<String, String> queryparams = new HashMap<String, String>();
        queryparams.put("page", String.valueOf(pageNumber));
        final RequestBody requestBody = new RequestBody();
        requestBody.setQueryParams(queryparams);


        PeopleMapper peopleMapper = new PeopleMapper(FollowersActivity.this, CommonConstants.GET_MY_FOLLOWERS_ACTION);
        peopleMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
            @Override
            public void onSuccess(Entity result) {
                if (result != null) {
                    PeopleSearchResult followersPeopleSearchResult = (PeopleSearchResult) result;
                    if (followersPeopleSearchResult.getNumOfPeople() == 0)

                        return;

                    peopleSearchResult.setPeopleList(followersPeopleSearchResult.getPeopleList());
                    peopleSearchResult.setNumOfPages(followersPeopleSearchResult.getNumOfPages());
                    peopleSearchResult.setNumOfPeople(followersPeopleSearchResult.getNumOfPeople());

                    adapter = new PeopleSearchRecyclerViewAdapter(FollowersActivity.this, peopleSearchResult.getPeopleList());
                    recyclerView.setAdapter(adapter);
                    recyclerView.setHasFixedSize(true);
                    hideLoadingBar();


                    recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

                        @Override
                        public void onScrolled(final RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            mVisibleItemCount = recyclerView.getChildCount();
                            mTotalItemCount = linearLayoutManager.getItemCount();
                            mFirstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                            //checking if no more results to get
                            if (mTotalItemCount >= peopleSearchResult.getNumOfPeople()) {
                                gotToEnd = true;

                                return;
                            }

                            //checking if scrolling down
                            if (mLastFirstVisibleItem < mFirstVisibleItem) {
                                int lastInScreen = mFirstVisibleItem + mVisibleItemCount;

                                //check if got to last item and not loading and the item count is bigger than default:20
                                if ((lastInScreen == mTotalItemCount) && !(isLoading) && !gotToEnd) {
                                    showLoadingBar();
                                    isLoading = true;


                                    RequestBody requestBody = new RequestBody();
                                    HashMap<String, String> queryparams = new HashMap<String, String>();
                                    queryparams.put("page", String.valueOf(++pageNumber));


                                    PeopleMapper mapper = new PeopleMapper(FollowersActivity.this, CommonConstants.GET_MY_FOLLOWERS_ACTION);
                                    mapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
                                        @Override
                                        public void onSuccess(Entity result) {
                                            PeopleSearchResult trendingListResult = (PeopleSearchResult) result;
                                            result = trendingListResult;
                                            adapter.add(trendingListResult.getPeopleList());
                                            hideLoadingBar();
                                            isLoading = false;
                                        }

                                        @Override
                                        public void onError(Exception e, Entity result) {

                                        }
                                    }, requestBody);


                                }
                                mLastFirstVisibleItem = mFirstVisibleItem;

                            }

                        }
                    });


                }
            }

            @Override
            public void onError(Exception e, Entity result) {

            }
        }, requestBody);




        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private void showLoadingBar() {
        loadingBar.setText("Loading...");
        loadingBar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        loadingBar.setVisibility(View.VISIBLE);
        loadingBar
                .animate()
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator(1));
    }

    private void hideLoadingBar() {
        loadingBar.setText("Loading...");
        loadingBar
                .animate()
                .translationY(loadingBar.getHeight())
                .setInterpolator(new AccelerateInterpolator(1));
        loadingBar.setVisibility(View.GONE);

    }
}
