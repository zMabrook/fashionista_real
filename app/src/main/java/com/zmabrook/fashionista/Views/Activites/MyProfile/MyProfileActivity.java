package com.zmabrook.fashionista.Views.Activites.MyProfile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.UserMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.Abstract.FashionistaActivity;
import com.zmabrook.fashionista.Views.Activites.EditMyprofileActivity;
import com.zmabrook.fashionista.Views.Activites.Registeration.RegiserationActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MyProfileActivity extends FashionistaActivity {
    @BindView(R.id.toolbarBackImageview)
    ImageView backImageView;

    @BindView(R.id.toolbarEditImageView)
    ImageView editImageView;


    @BindView(R.id.coverImageView)
    ImageView coverImageView;

    @BindView(R.id.avatarImageView)
    CircleImageView avatarImageView;

    @BindView(R.id.nameTextView)
    TextView nameTextView;

    @BindView(R.id.usernameTextView)
    TextView usernameTextView;


    @BindView(R.id.bioTextView)
    TextView bioTextView;


    @BindView(R.id.followingNumberTextView)
    TextView followingNumberTextView;

    @BindView(R.id.followersNumberTextView)
    TextView followersNumberTextView;

    @BindView(R.id.blockedNumberTextView)
    TextView blockedNumberTextView;

    @BindView(R.id.postsNumberTextView)
    TextView postsNumberTextView;

    @BindView(R.id.followingTextView)
    TextView followingTextView;

    @BindView(R.id.followersTextView)
    TextView followersTextView;

    @BindView(R.id.blockedTextView)
    TextView blockedTextView;

    @BindView(R.id.postsTextView)
    TextView postsTextView;


    @BindView(R.id.followSwitch)
    SwitchCompat switchCompat;

    @BindView(R.id.userFollowsYou)
    TextView userFollowsYou;

    @BindView(R.id.shareAppTextView)
    TextView shareAppTextView;

    @BindView(R.id.helpCenterTextView)
    TextView helpCenterTextView;

    @BindView(R.id.sendFeedbackTextView)
    TextView sendFeedbackTextView;

    @BindView(R.id.logoutTextView)
    TextView logoutTextView;


    @BindView(R.id.legalTextView)
    TextView legalTextView;

    User user1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        ButterKnife.bind(this);
        nameTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaMedium.ttf"));
        userFollowsYou.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        usernameTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        bioTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        blockedNumberTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        followersNumberTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        postsNumberTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        followingNumberTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        postsTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        followersTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        followingTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        blockedTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        postsTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        shareAppTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        helpCenterTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        sendFeedbackTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        logoutTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        legalTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));


        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        editImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyProfileActivity.this, EditMyprofileActivity.class);

                if (user1 != null) {
                    i.putExtra(CommonConstants.PREFS_USER, user1);

                }

                startActivity(i);
            }
        });




        logoutTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences settings = getApplicationContext().getSharedPreferences(CommonConstants.PREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.clear();
                editor.putBoolean(CommonConstants.PREFS_TOUR_FINISHED, true);
                editor.commit();
                Intent i = new Intent(MyProfileActivity.this, RegiserationActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }
        });

        shareAppTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Try this cool application Fashionista \n" + CommonConstants.APPLICATION_URL);
                sendIntent.setType("text/plain");
                MyProfileActivity.this.startActivity(Intent.createChooser(sendIntent, "Share To"));
            }
        });

        switchCompat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestBody requestBody1 = new RequestBody();
                UserMapper userMapper1 = new UserMapper(MyProfileActivity.this, CommonConstants.TOGGLE_FOLLOW_NOTIFICATIONS);
                userMapper1.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.PUT, new FashionistaListener() {
                    @Override
                    public void onSuccess(Object result) {

                    }

                    @Override
                    public void onError(Exception e, Object result) {

                    }
                }, requestBody1);


            }
        });


        followersNumberTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user1.getFollowersCount() == "0"){
                    Toast.makeText(MyProfileActivity.this, "You have no followers yet", Toast.LENGTH_LONG).show();

                    return;

                }

                Intent i = new Intent(MyProfileActivity.this, FollowersActivity.class);
                startActivity(i);

            }
        });
        followersTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                followersNumberTextView.performClick();
            }
        });



        followingNumberTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user1.getFollowingCount() == "0"){
                    Toast.makeText(MyProfileActivity.this, "You aren't following anyone yet.", Toast.LENGTH_LONG).show();

                    return;

                }

                Intent i = new Intent(MyProfileActivity.this, FollowingActivity.class);
                startActivity(i);
            }
        });

        followingTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                followingNumberTextView.performClick();
            }
        });


    }

    private void loadUser(final User user) {
        if (user != null) {

            if (user.getName() != null) {
                nameTextView.setText(user.getName());
            }

            if (user.getUsername() != null) {
                usernameTextView.setText("@" + user.getUsername());
            }

            if (user.getBio() != null) {
                bioTextView.setText(user.getBio());
            }

            if (user.getFollowingCount() != null) {
                followingNumberTextView.setText(user.getFollowingCount());
            }

            if (user.getFollowersCount() != null) {
                followersNumberTextView.setText(user.getFollowersCount());
            }

            if (user.getBlocksCount() != null) {
                blockedNumberTextView.setText(user.getBlocksCount());
            }

            if (user.getLooksCount() != null) {
                postsNumberTextView.setText(user.getLooksCount());
            }

            if (user.getImageUrl() != null) {
                Picasso.with(MyProfileActivity.this)
                        .load(user.getImageUrl())
                        .into(avatarImageView, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Picasso.with(MyProfileActivity.this)
                                        .load(user.getImageUrl())
                                        .into(avatarImageView);
                            }
                        });
            }


            if (user.getCoverImageUrl() != null) {
                Picasso.with(MyProfileActivity.this)
                        .load(user.getCoverImageUrl())
                        .into(coverImageView, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Picasso.with(MyProfileActivity.this)
                                        .load(user.getCoverImageUrl())
                                        .into(coverImageView);
                            }
                        });
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

            final RequestBody requestBody = new RequestBody();
            final UserMapper userMapper = new UserMapper(this, CommonConstants.GET_MY_PROFILE_DATA_ACTION);
            userMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener() {
                @Override
                public void onSuccess(Object result) {
                    final User user = (User) result;
                    user1 = user;
                    loadUser(user);

                }

                @Override
                public void onError(Exception e, Object result) {
                    Toast.makeText(MyProfileActivity.this, "Failed to load. Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }, requestBody);
        }

}
