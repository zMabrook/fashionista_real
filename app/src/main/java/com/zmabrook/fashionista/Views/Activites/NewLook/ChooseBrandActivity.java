package com.zmabrook.fashionista.Views.Activites.NewLook;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.BrandSearchResult;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.SearchMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Adapters.ItemBrandsRecyclerViewAdapter;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChooseBrandActivity extends AppCompatActivity {

    @BindView(R.id.toolbarbackImageView)
    ImageView toolbarbackImageView;

    @BindView(R.id.toolbarNextImageView)
    ImageView toolbarNextImageView;

    @BindView(R.id.titleTextView)
    TextView titleTextView;

    @BindView(R.id.justUseTextView)
    TextView justUseTextView;

    @BindView(R.id.lookImage)
    ImageView lookImage;

    @BindView(R.id.searchEditText)
    EditText searchEditText;

    @BindView(R.id.recylerView)
    RecyclerView recyclerView;

    @BindView(R.id.mainView)
    RelativeLayout mainLayout;

    @BindView(R.id.searchCardView)
    CardView searchCardView;

    @BindView(R.id.indicator)
    ProgressBar indicator;
    String searchQuery ="";

    @BindView(R.id.choosedBrandCardView)
    CardView choosedBrandCardView;

    @BindView(R.id.choosenCategoryIcon)
    ImageView choosenCategoryIcon;

    @BindView(R.id.choosenBrandName)
    TextView choosenBrandName;

    @BindView(R.id.choosenBrandIcon)
    ImageView choosenBrandIcon;


    LinearLayoutManager linearLayoutManager;
    ItemBrandsRecyclerViewAdapter adapter;
    BrandSearchResult brandSearchResult;
    public static int pageNumber = 1;
    int mFirstVisibleItem, mVisibleItemCount, mTotalItemCount, mLastFirstVisibleItem;
    boolean isLoading=false;
    boolean gotToEnd=false;

    ImageView tagImageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_brand);
        ButterKnife.bind(this);

        titleTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        searchEditText.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        choosenBrandName.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));

        titleTextView.setText("Pick the Item's brand");
        lookImage.setImageBitmap(TagItemActivity.filteredBitmap);
        indicator.setVisibility(View.GONE);

        toolbarbackImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbarNextImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TagItemActivity.item.getBrand()!=null || TagItemActivity.item.getWholeBrand() !=null){
                    Intent i = new Intent(ChooseBrandActivity.this,ChoosePriceActivity.class);
                    startActivity(i);
                }else {
                    Toast.makeText(ChooseBrandActivity.this,"please Choose Your item brand first",Toast.LENGTH_LONG).show();

                }

            }
        });


        tagImageView = new ImageView(ChooseBrandActivity.this);

        tagImageView.setImageResource(R.mipmap.xytag);
        int tagDimension = (int) getResources().getDimension(R.dimen.tag);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) getResources().getDimension(R.dimen.tag), (int) getResources().getDimension(R.dimen.tag));
        params.leftMargin =(int)TagItemActivity.item.getX();
        params.topMargin = (int)TagItemActivity.item.getY() ;


        mainLayout.addView(tagImageView, params);



        linearLayoutManager = new LinearLayoutManager(ChooseBrandActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);



        RequestBody requestBody = new RequestBody();
        HashMap<String, String> queryparams = new HashMap<String, String>();
        queryparams.put("page", String.valueOf(pageNumber));
        if (!searchEditText.getText().toString().equals("")) {
            queryparams.put("query", searchEditText.getText().toString());
            searchQuery = searchEditText.getText().toString();
        }
        SearchMapper mapper = new SearchMapper(ChooseBrandActivity.this, CommonConstants.BRANDS_SEARCH_ACTION);
        mapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
            @Override
            public void onSuccess(Entity result) {
                //hideLoadingBar();
                brandSearchResult = (BrandSearchResult) result;
                if (brandSearchResult ==null){
                    brandSearchResult = new BrandSearchResult();
                    justUsethisBrandName();
                }else {
                    justUseTextView.setVisibility(View.GONE);

                }
                adapter = new ItemBrandsRecyclerViewAdapter(ChooseBrandActivity.this, brandSearchResult.getBrands());
                recyclerView.setAdapter(adapter);
                recyclerView.setHasFixedSize(true);


                recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

                    @Override
                    public void onScrolled(final RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        mVisibleItemCount = recyclerView.getChildCount();
                        mTotalItemCount = linearLayoutManager.getItemCount();
                        mFirstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                        //checking if no more results to get
                        if (mTotalItemCount >= brandSearchResult.getNumOfBrands()) {
                            gotToEnd = true;

                            return;
                        }

                        //checking if scrolling down
                        if (mLastFirstVisibleItem < mFirstVisibleItem) {
                            int lastInScreen = mFirstVisibleItem + mVisibleItemCount;

                            //check if got to last item and not loading and the item count is bigger than default:20
                            if ((lastInScreen == mTotalItemCount) && !(isLoading) && !gotToEnd) {
                                //   showLoadingBar();
                                isLoading = true;


                                RequestBody requestBody = new RequestBody();
                                HashMap<String, String> queryparams = new HashMap<String, String>();
                                queryparams.put("page", String.valueOf(pageNumber));
                                if (!searchQuery.equals("")){
                                    queryparams.put("query", searchQuery);

                                }

                                requestBody.setQueryParams(queryparams);

                                SearchMapper mapper = new SearchMapper(ChooseBrandActivity.this, CommonConstants.BRANDS_SEARCH_ACTION);
                                mapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
                                    @Override
                                    public void onSuccess(Entity result) {
                                        BrandSearchResult trendingListResult = (BrandSearchResult) result;
                                        if (trendingListResult == null) {
                                            trendingListResult = new BrandSearchResult();

                                            justUsethisBrandName();

                                        }else {
                                            justUseTextView.setVisibility(View.GONE);

                                        }

                                        adapter.add(trendingListResult.getBrands());
                                        //      hideLoadingBar();
                                        isLoading = false;
                                    }

                                    @Override
                                    public void onError(Exception e, Entity result) {

                                    }
                                }, requestBody);


                            }
                            mLastFirstVisibleItem = mFirstVisibleItem;

                        }

                    }
                });


            }

            @Override
            public void onError(Exception e, Entity result) {

            }
        }, requestBody);



        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length()!=0 && s.toString().length() < 4)
                    return;

                RequestBody requestBody = new RequestBody();
                HashMap<String, String> queryparams = new HashMap<String, String>();
                pageNumber = 1;
                queryparams.put("page", String.valueOf(pageNumber));
                if (!searchEditText.getText().toString().equals("")) {
                    queryparams.put("query", searchEditText.getText().toString());
                    searchQuery = searchEditText.getText().toString();
                }
                requestBody.setQueryParams(queryparams);

                SearchMapper mapper = new SearchMapper(ChooseBrandActivity.this, CommonConstants.BRANDS_SEARCH_ACTION);
                mapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
                    @Override
                    public void onSuccess(Entity result) {
                        //hideLoadingBar();
                        brandSearchResult = (BrandSearchResult) result;
                        if (brandSearchResult == null) {
                            brandSearchResult = new BrandSearchResult();
                            justUsethisBrandName();
                        }else {
                            justUseTextView.setVisibility(View.GONE);

                        }
                        adapter = new ItemBrandsRecyclerViewAdapter(ChooseBrandActivity.this, brandSearchResult.getBrands());
                        recyclerView.setAdapter(adapter);
                        recyclerView.setHasFixedSize(true);
                        if (brandSearchResult.getBrands()!=null &&brandSearchResult.getBrands().size() == 0) {
                            //noResultTextView.setVisibility(View.VISIBLE);

                        } else {
                            //noResultTextView.setVisibility(View.GONE);

                        }

                        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

                            @Override
                            public void onScrolled(final RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                mVisibleItemCount = recyclerView.getChildCount();
                                mTotalItemCount = linearLayoutManager.getItemCount();
                                mFirstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                                //checking if no more results to get
                                if (mTotalItemCount >= brandSearchResult.getNumOfBrands()) {
                                    gotToEnd = true;

                                    return;
                                }

                                //checking if scrolling down
                                if (mLastFirstVisibleItem < mFirstVisibleItem) {
                                    int lastInScreen = mFirstVisibleItem + mVisibleItemCount;

                                    //check if got to last item and not loading and the item count is bigger than default:20
                                    if ((lastInScreen == mTotalItemCount) && !(isLoading) && !gotToEnd) {
                                        //showLoadingBar();
                                        isLoading = true;


                                        RequestBody requestBody = new RequestBody();
                                        HashMap<String, String> queryparams = new HashMap<String, String>();
                                        queryparams.put("page", String.valueOf(++pageNumber));
                                        if (!searchQuery.equals("")) {
                                            queryparams.put("query", searchEditText.getText().toString());

                                        }

                                        SearchMapper mapper = new SearchMapper(ChooseBrandActivity.this, CommonConstants.BRANDS_SEARCH_ACTION);
                                        mapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
                                            @Override
                                            public void onSuccess(Entity result) {
                                                BrandSearchResult trendingListResult = (BrandSearchResult) result;
                                                if (trendingListResult == null) {
                                                    trendingListResult = new BrandSearchResult();
                                                    justUsethisBrandName();
                                                }else {
                                                    justUseTextView.setVisibility(View.GONE);

                                                }
                                                adapter.add(trendingListResult.getBrands());
                                                //hideLoadingBar();
                                                isLoading = false;
                                            }

                                            @Override
                                            public void onError(Exception e, Entity result) {

                                            }
                                        }, requestBody);


                                    }
                                    mLastFirstVisibleItem = mFirstVisibleItem;

                                }

                            }
                        });


                    }

                    @Override
                    public void onError(Exception e, Entity result) {

                    }
                }, requestBody);


                adapter.notifyDataSetChanged();

            }
        });
    }

    public void showChoosenBrand(){
        if (TagItemActivity.item.getWholeBrand()!=null){
            searchCardView.setVisibility(View.GONE);
            Picasso.with(ChooseBrandActivity.this).load(TagItemActivity.item.getCategory().getIconUrl()).into(choosenCategoryIcon, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(ChooseBrandActivity.this).load(TagItemActivity.item.getCategory().getIconUrl()).into(choosenCategoryIcon);
                }
            });

            Picasso.with(ChooseBrandActivity.this).load(TagItemActivity.item.getWholeBrand().getIconUrl()).into(choosenBrandIcon, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(ChooseBrandActivity.this).load(TagItemActivity.item.getWholeBrand().getIconUrl()).into(choosenBrandIcon);
                }
            });

            choosenBrandName.setText(TagItemActivity.item.getWholeBrand().getName());


                    choosedBrandCardView.setVisibility(View.VISIBLE);

        }else if(TagItemActivity.item.getBrand()!=null){
            searchCardView.setVisibility(View.GONE);

            Picasso.with(ChooseBrandActivity.this).load(TagItemActivity.item.getCategory().getIconUrl()).into(choosenCategoryIcon, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(ChooseBrandActivity.this).load(TagItemActivity.item.getCategory().getIconUrl()).into(choosenCategoryIcon);
                }
            });

            choosenBrandIcon.setVisibility(View.GONE);
            choosenBrandName.setText(TagItemActivity.item.getBrand());
            choosedBrandCardView.setVisibility(View.VISIBLE);


        }else {
            Toast.makeText(ChooseBrandActivity.this,"please Choose Your item brand first",Toast.LENGTH_LONG).show();

        }

    }

    private void justUsethisBrandName(){
        justUseTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        justUseTextView.setVisibility(View.VISIBLE);
        justUseTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchEditText.getText().toString().equals("")){
                    return;
                }
                TagItemActivity.item.setBrand(searchEditText.getText().toString());
                justUseTextView.setVisibility(View.GONE);
                showChoosenBrand();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
