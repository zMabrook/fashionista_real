package com.zmabrook.fashionista.Views.Activites.NewLook;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Location;
import com.zmabrook.fashionista.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChooseLocationActivity extends AppCompatActivity {
    @BindView(R.id.toolbarbackImageView)
    ImageView toolbarbackImageView;

    @BindView(R.id.toolbarNextImageView)
    ImageView toolbarNextImageView;

    @BindView(R.id.titleTextView)
    TextView titleTextView;

    @BindView(R.id.selectPlaceOnmap)
    TextView selectPlaceOnmap;

    @BindView(R.id.whereDidyouBuyItTextView)
    TextView whereDidyouBuyItTextView;

    @BindView(R.id.lookImage)
    ImageView lookImage;

    Marker marker;
    SupportMapFragment mapView;
    Location itemLocation;
    GoogleMap map;
    public LocationManager locationManager;
    LatLng CENTER = null;
    String CityName;
    LatLng glatLng;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_location);
        ButterKnife.bind(this);
        titleTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        selectPlaceOnmap.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        whereDidyouBuyItTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));

        titleTextView.setText("Mention The Location");


        toolbarbackImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbarNextImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (itemLocation!=null){
                TagItemActivity.item.setLocation(itemLocation);
                Intent i = new Intent(ChooseLocationActivity.this,TagAnotherItemActivity.class);
                startActivity(i);

            }else {
                Toast.makeText(ChooseLocationActivity.this,"Please select location first",Toast.LENGTH_LONG).show();
            }

            }
        });

        lookImage.setImageBitmap(TagItemActivity.filteredBitmap);


        setMapView();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void setMapView() {
        mapView = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.mapview);

        try {
            MapsInitializer.initialize(ChooseLocationActivity.this);

            switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(ChooseLocationActivity.this)) {
                case ConnectionResult.SUCCESS:
                    // Toast.makeText(getActivity(), "SUCCESS", Toast.LENGTH_SHORT)
                    // .show();

                    // Gets to GoogleMap from the MapView and does initialization
                    // stuff
                    if (mapView != null) {

                        locationManager = ((LocationManager) ChooseLocationActivity.this
                                .getSystemService(Context.LOCATION_SERVICE));

                        Boolean localBoolean = Boolean.valueOf(locationManager
                                .isProviderEnabled("network"));

                        if (localBoolean.booleanValue()) {

                            CENTER = new LatLng(30.0553861, 31.3834799);

                        } else {

                        }
                        map =mapView.getMap();
                        if (map == null) {

                            Log.d(CommonConstants.APP_TAG, "Map SearchBrandsFragment Not Found or no Map in it!!");

                        }



                        map.setIndoorEnabled(true);
                        map.setMyLocationEnabled(true);
                        map.moveCamera(CameraUpdateFactory.zoomTo(5));
                        if (CENTER != null) {
                            map.animateCamera(
                                    CameraUpdateFactory.newLatLng(CENTER), 1750,
                                    null);
                        }
                        // add circle
                        CircleOptions circle = new CircleOptions();
                        circle.center(CENTER).fillColor(Color.BLUE).radius(10);
                        map.addCircle(circle);
                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);


                        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(LatLng latLng) {
                                itemLocation = null;
                                map.clear();
                                 marker  = map.addMarker(new MarkerOptions()
                                        .position(latLng)
                                        .title("I bought it from here")
                                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
                                Geocoder geocoder;
                                geocoder = new Geocoder(ChooseLocationActivity.this, Locale.getDefault());
                                glatLng = latLng;
                                new RetrieveAddress().execute();




                            }
                        });

                    }
                    break;
                case ConnectionResult.SERVICE_MISSING:
                    Log.d(CommonConstants.APP_TAG, "Google Play service missing");

                    break;
                case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                    Log.d(CommonConstants.APP_TAG, "Google Play service require update");

                    break;
                default:

            }
        } catch (Exception e) {

        }

    }


    class RetrieveAddress extends AsyncTask<String, Void, Location> {

        private Exception exception;

        protected Location doInBackground(String... urls) {
            Location location=null;
            HttpGet httpGet = new HttpGet("http://maps.google.com/maps/api/geocode/json?latlng="+glatLng.latitude+","+glatLng.longitude+"&sensor=true");
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            StringBuilder stringBuilder = new StringBuilder();

            try {
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                InputStream stream = entity.getContent();
                int b;
                while ((b = stream.read()) != -1) {
                    stringBuilder.append((char) b);
                }
            } catch (ClientProtocolException e) {
            } catch (IOException e) {
            }


            try {
                JSONObject jsonObject = new JSONObject(stringBuilder.toString());
                JSONArray addressComponents =jsonObject.getJSONArray("results").getJSONObject(0).getJSONArray("address_components");
                location = new Location();
                for (int i=0 ; i < addressComponents.length();i++){
                    String type =  addressComponents.getJSONObject(i).getJSONArray("types").getString(0);

                    switch (type){
                        case "administrative_area_level_2":
                            location.setDistrict(addressComponents.getJSONObject(i).getString("long_name"));
                            break;
                        case "administrative_area_level_1":
                            location.setCity(addressComponents.getJSONObject(i).getString("long_name"));

                            break;
                        case "country":
                            location.setCountry(addressComponents.getJSONObject(i).getString("long_name"));
                            break;
                        default:
                            break;
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return location;


        }

        protected void onPostExecute(Location feed) {
            if (feed!=null){
                itemLocation = feed;
                itemLocation.setLatLng(glatLng);
                marker.setSnippet(itemLocation.getCountry()+","+itemLocation.getCity());

            }

            // TODO: check this.exception
            // TODO: do something with the feed
        }
    }


}
