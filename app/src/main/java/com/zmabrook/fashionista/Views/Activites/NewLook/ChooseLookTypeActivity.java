package com.zmabrook.fashionista.Views.Activites.NewLook;

import android.content.Context;
import android.content.Intent;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.gms.maps.model.LatLng;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.Item;
import com.zmabrook.fashionista.Domain.Entities.Location;
import com.zmabrook.fashionista.Domain.Entities.LookType;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.SearchMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.Abstract.FashionistaActivity;
import com.zmabrook.fashionista.Views.Adapters.ItemBrandsRecyclerViewAdapter;
import com.zmabrook.fashionista.Views.Adapters.LookTypesRecyclerViewAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChooseLookTypeActivity extends FashionistaActivity implements LocationListener {
    @BindView(R.id.toolbarbackImageView)
    ImageView toolbarbackImageView;

    @BindView(R.id.toolbarNextImageView)
    ImageView toolbarNextImageView;

    @BindView(R.id.lookImage)
    ImageView lookImage;

    @BindView(R.id.recylerView)
    RecyclerView recyclerView;

    @BindView(R.id.mainLayoutContainer)
    RelativeLayout mainLayout;

    LinearLayoutManager linearLayoutManager;
    LookTypesRecyclerViewAdapter adapter;
    ArrayList<LookType> lookTypes;
    LocationManager locationManager;
    ImageView tagImageView;
    LatLng glatLng;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_look_type);
        ButterKnife.bind(this);

        locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);


        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 5000, 10, this);
        toolbarbackImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbarNextImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ChooseLookTypeActivity.this,"Please select look type",Toast.LENGTH_LONG).show();
            }
        });

        lookImage.setImageBitmap(TagItemActivity.filteredBitmap);
         ArrayList<Item> items = TagItemActivity.newLook.getItems();
        if (items.size()!=0){
            for (int i=0 ; i <items.size();i++){
                Item item = items.get(i);

                tagImageView = new ImageView(ChooseLookTypeActivity.this);

                tagImageView.setImageResource(R.mipmap.xytag);
                int tagDimension = (int) getResources().getDimension(R.dimen.tag);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) getResources().getDimension(R.dimen.tag), (int) getResources().getDimension(R.dimen.tag));
                params.leftMargin =(int)item.getX();
                params.topMargin = (int)item.getY();

                mainLayout.addView(tagImageView, params);

            }
        }


        linearLayoutManager = new LinearLayoutManager(ChooseLookTypeActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        RequestBody requestBody = new RequestBody();


        SearchMapper searchMapper = new SearchMapper(ChooseLookTypeActivity.this, CommonConstants.LOOK_TYPES_SEARCH_ACTION);
        searchMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_ARRAY, Request.Method.GET, new FashionistaListener() {
            @Override
            public void onSuccess( Object result) {
                lookTypes = (ArrayList<LookType>)result;
                adapter = new LookTypesRecyclerViewAdapter(ChooseLookTypeActivity.this, lookTypes);
                recyclerView.setAdapter(adapter);
                recyclerView.setHasFixedSize(true);
            }

            @Override
            public void onError(Exception e, Object result) {

            }
        },requestBody);
    }

    @Override
    public void onLocationChanged(android.location.Location location) {
        LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());
        glatLng = latLng;
        new RetrieveAddress().execute();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    class RetrieveAddress extends AsyncTask<String, Void, Location> {

        private Exception exception;

        protected Location doInBackground(String... urls) {
            Location location=null;
            HttpGet httpGet = new HttpGet("http://maps.google.com/maps/api/geocode/json?latlng="+glatLng.latitude+","+glatLng.longitude+"&sensor=true");
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            StringBuilder stringBuilder = new StringBuilder();

            try {
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                InputStream stream = entity.getContent();
                int b;
                while ((b = stream.read()) != -1) {
                    stringBuilder.append((char) b);
                }
            } catch (ClientProtocolException e) {
            } catch (IOException e) {
            }


            try {
                JSONObject jsonObject = new JSONObject(stringBuilder.toString());
                JSONArray addressComponents =jsonObject.getJSONArray("results").getJSONObject(0).getJSONArray("address_components");
                location = new Location();
                for (int i=0 ; i < addressComponents.length();i++){
                    String type =  addressComponents.getJSONObject(i).getJSONArray("types").getString(0);

                    switch (type){
                        case "administrative_area_level_2":
                            location.setDistrict(addressComponents.getJSONObject(i).getString("long_name"));
                            break;
                        case "administrative_area_level_1":
                            location.setCity(addressComponents.getJSONObject(i).getString("long_name"));

                            break;
                        case "country":
                            location.setCountry(addressComponents.getJSONObject(i).getString("long_name"));
                            break;
                        default:
                            break;
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return location;


        }

        protected void onPostExecute(Location feed) {
            if (feed!=null){
                TagItemActivity.newLook.setLocation(feed);
                TagItemActivity.newLook.getLocation().setLatLng(glatLng);

                Toast.makeText(ChooseLookTypeActivity.this,"Location taken",Toast.LENGTH_LONG).show();
            }

            // TODO: check this.exception
            // TODO: do something with the feed
        }
    }

}
