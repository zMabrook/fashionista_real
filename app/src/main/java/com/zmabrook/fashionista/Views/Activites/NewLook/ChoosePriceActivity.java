package com.zmabrook.fashionista.Views.Activites.NewLook;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zmabrook.fashionista.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChoosePriceActivity extends AppCompatActivity {
    @BindView(R.id.toolbarbackImageView)
    ImageView toolbarbackImageView;

    @BindView(R.id.toolbarNextImageView)
    ImageView toolbarNextImageView;

    @BindView(R.id.titleTextView)
    TextView titleTextView;

    @BindView(R.id.lookImage)
    ImageView lookImage;


    @BindView(R.id.choosedBrandCardView)
    CardView choosedBrandCardView;

    @BindView(R.id.choosenCategoryIcon)
    ImageView choosenCategoryIcon;

    @BindView(R.id.choosenBrandName)
    TextView choosenBrandName;

    @BindView(R.id.choosenBrandIcon)
    ImageView choosenBrandIcon;

    @BindView(R.id.priceEditText)
    EditText priceEditText;

    @BindView(R.id.howMuchTextView)
    TextView howMuchTextView;

    @BindView(R.id.skipTextView)
    TextView skipTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_price);
        ButterKnife.bind(this);


        titleTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        choosenBrandName.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        howMuchTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaMedium.ttf"));
        priceEditText.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaMedium.ttf"));
        skipTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));

        titleTextView.setText("Mention The Price");


        toolbarbackImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbarNextImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!priceEditText.getText().toString().equals("")&&Double.valueOf(priceEditText.getText().toString()) != 0) {

                    TagItemActivity.item.setPrice(Double.valueOf(priceEditText.getText().toString()));
                    Intent i = new Intent(ChoosePriceActivity.this, ChooseLocationActivity.class);
                    startActivity(i);
                } else {
                   skipTextView.performClick();
                }

            }
        });

        lookImage.setImageBitmap(TagItemActivity.filteredBitmap);


        showChoosenBrand();


        skipTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TagItemActivity.item.setPrice(0);
                Intent i = new Intent(ChoosePriceActivity.this, ChooseLocationActivity.class);
                startActivity(i);
            }
        });

    }

    public void showChoosenBrand() {
        if (TagItemActivity.item.getWholeBrand() != null) {
            Picasso.with(ChoosePriceActivity.this).load(TagItemActivity.item.getCategory().getIconUrl()).into(choosenCategoryIcon, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(ChoosePriceActivity.this).load(TagItemActivity.item.getCategory().getIconUrl()).into(choosenCategoryIcon);
                }
            });

            Picasso.with(ChoosePriceActivity.this).load(TagItemActivity.item.getWholeBrand().getIconUrl()).into(choosenBrandIcon, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(ChoosePriceActivity.this).load(TagItemActivity.item.getWholeBrand().getIconUrl()).into(choosenBrandIcon);
                }
            });

            choosenBrandName.setText(TagItemActivity.item.getWholeBrand().getName());


        } else if (TagItemActivity.item.getBrand() != null) {

            Picasso.with(ChoosePriceActivity.this).load(TagItemActivity.item.getCategory().getIconUrl()).into(choosenCategoryIcon, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(ChoosePriceActivity.this).load(TagItemActivity.item.getCategory().getIconUrl()).into(choosenCategoryIcon);
                }
            });

            choosenBrandIcon.setVisibility(View.GONE);
            choosenBrandName.setText(TagItemActivity.item.getBrand());


        } else {
            Toast.makeText(ChoosePriceActivity.this, "please Choose Your item brand first", Toast.LENGTH_LONG).show();

        }

    }



    @Override
    protected void onStart() {
        super.onStart();
        TagItemActivity.item.setPrice(0);

    }
}
