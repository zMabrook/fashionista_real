package com.zmabrook.fashionista.Views.Activites.NewLook;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.Abstract.FashionistaActivity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.Ragnarok.BitmapFilter;

public class FiltersActivity extends FashionistaActivity implements View.OnClickListener {
    @BindView(R.id.lookImage)
    ImageView lookImage;

    @BindView(R.id.toolbarbackImageView)
    ImageView toolbarbackImageView;

    @BindView(R.id.toolbarNextImageView)
    ImageView toolbarNextImageView;

    @BindView(R.id.titleTextView)
    TextView titleTextView;

    @BindView(R.id.filterNormalTextView)
    TextView filterNormalTextView;
    @BindView(R.id.filterNormalImageView)
    ImageView filterNormalImageView;

    @BindView(R.id.filterBlackandwhiteTextView)
    TextView filterBlackandwhiteTextView;
    @BindView(R.id.filterBlackandwhiteImageView)
    ImageView filterBlackandwhiteImageView;

    @BindView(R.id.filterTVTextView)
    TextView filterTVTextView;
    @BindView(R.id.filterTVImageView)
    ImageView filterTVImageView;


    @BindView(R.id.filterNameOldPhotoTextView)
    TextView filterNameOldPhotoTextView;
    @BindView(R.id.filterOldPhotoImageView)
    ImageView filterOldPhotoImageView;


    @BindView(R.id.filterHDRTextView)
    TextView filterHDRTextView;
    @BindView(R.id.filterHDRImageView)
    ImageView filterHDRImageView;


    @BindView(R.id.filterLomoTextView)
    TextView filterLomoTextView;
    @BindView(R.id.filterLomoImageView)
    ImageView filterLomoImageView;


    @BindView(R.id.filterLightTextView)
    TextView filterLightTextView;
    @BindView(R.id.filterLightImageView)
    ImageView filterLightImageView;

    @BindView(R.id.filterAVGBlurTextView)
    TextView filterAVGBlurTextView;
    @BindView(R.id.filterAVGBlurImageView)
    ImageView filterAVGBlurImageView;


    @BindView(R.id.filterGothamTextView)
    TextView filterGothamTextView;
    @BindView(R.id.filterGothamImageView)
    ImageView filterGothamImageView;

    @BindView(R.id.filterInvertTextView)
    TextView filterInvertTextView;
    @BindView(R.id.filterInvertImageView)
    ImageView filterInvertImageView;


    @BindView(R.id.filterSharpenTextView)
    TextView filterSharpenTextView;
    @BindView(R.id.filterSharpenImageView)
    ImageView filterSharpenImageView;


    @BindView(R.id.filterGblurTextView)
    TextView filterGblurTextView;
    @BindView(R.id.filterGblurImageView)
    ImageView filterGblurImageView;

    @BindView(R.id.filterMotionBlurTextView)
    TextView filterMotionBlurTextView;
    @BindView(R.id.filterMotionBlurImageView)
    ImageView filterMotionBlurImageView;

    @BindView(R.id.filterSoftGlowTextView)
    TextView filterSoftGlowTextView;
    @BindView(R.id.filterSoftGlowImageView)
    ImageView filterSoftGlowImageView;


    @BindView(R.id.indicator)
    ProgressBar indicator;

    String MediaUrl;
    int choosenFilter;
    Bitmap originalBitmap;
    Bitmap smallBitmap;
    BitmapWorkerTask2 oldtask2;

    SimpleTarget simpleTarget2 = new SimpleTarget<Bitmap>() {
        @Override
        public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
            smallBitmap = resource;


            filterNormalImageView.setImageBitmap(smallBitmap);

            loadBitmap(BitmapFilter.GRAY_STYLE,filterBlackandwhiteImageView);
            loadBitmap(BitmapFilter.OLD_STYLE,filterOldPhotoImageView);
            loadBitmap(BitmapFilter.HDR_STYLE,filterHDRImageView);

            loadBitmap(BitmapFilter.LOMO_STYLE,filterLomoImageView);
            loadBitmap(BitmapFilter.LIGHT_STYLE,filterLightImageView);
            loadBitmap(BitmapFilter.AVERAGE_BLUR_STYLE,filterAVGBlurImageView);
            loadBitmap(BitmapFilter.GOTHAM_STYLE,filterGothamImageView);


            loadBitmap(BitmapFilter.INVERT_STYLE,filterInvertImageView);
            loadBitmap(BitmapFilter.SHARPEN_STYLE,filterSharpenImageView);
            loadBitmap(BitmapFilter.GAUSSIAN_BLUR_STYLE,filterGblurImageView);
            loadBitmap(BitmapFilter.MOTION_BLUR_STYLE,filterMotionBlurImageView);
            loadBitmap(BitmapFilter.SOFT_GLOW_STYLE,filterSoftGlowImageView);
            loadBitmap(BitmapFilter.TV_STYLE,filterTVImageView);
        }
    };
    SimpleTarget simpleTarget = new SimpleTarget<Bitmap>() {
        @Override
        public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
            originalBitmap= resource;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            originalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            originalBitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
            lookImage.setImageBitmap(originalBitmap);







/*
        filterBlackandwhiteImageView.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.GRAY_STYLE));
        filterTVImageView.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.TV_STYLE));
        filterOldPhotoImageView.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.OLD_STYLE));
        filterHDRImageView.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.HDR_STYLE));
        filterLomoImageView.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.LOMO_STYLE));
        filterLightImageView.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.LIGHT_STYLE));
        filterAVGBlurImageView.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.AVERAGE_BLUR_STYLE));
        filterGothamImageView.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.GOTHAM_STYLE));
        filterInvertImageView.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.INVERT_STYLE));
        filterSharpenImageView.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.SHARPEN_STYLE));
        filterGblurImageView.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.GAUSSIAN_BLUR_STYLE));
        filterMotionBlurImageView.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.MOTION_BLUR_STYLE,3,2));
        filterSoftGlowImageView.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.SOFT_GLOW_STYLE));*/


        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);
        ButterKnife.bind(this);

        titleTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        titleTextView.setText("Choose Filter");


        filterNormalTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        filterBlackandwhiteTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        filterTVTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        filterNameOldPhotoTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        filterHDRTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        filterLomoTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        filterLightTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        filterAVGBlurTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        filterGothamTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        filterInvertTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        filterSharpenTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        filterGblurTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        filterMotionBlurTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        filterSoftGlowTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));


        toolbarNextImageView.setOnClickListener(this);
        toolbarbackImageView.setOnClickListener(this);

        filterNormalImageView.setOnClickListener(this);
        filterBlackandwhiteImageView.setOnClickListener(this);
        filterTVImageView.setOnClickListener(this);
        filterOldPhotoImageView.setOnClickListener(this);
        filterHDRImageView.setOnClickListener(this);
        filterLomoImageView.setOnClickListener(this);
        filterLightImageView.setOnClickListener(this);
        filterAVGBlurImageView.setOnClickListener(this);
        filterGothamImageView.setOnClickListener(this);
        filterInvertImageView.setOnClickListener(this);
        filterSharpenImageView.setOnClickListener(this);
        filterGblurImageView.setOnClickListener(this);
        filterMotionBlurImageView.setOnClickListener(this);
        filterSoftGlowImageView.setOnClickListener(this);

        MediaUrl = getIntent().getExtras().getString("MediaUrl");
        Glide.with(this)
                .load(MediaUrl)
                .asBitmap()
                .override(180, 180) // resizes the image to these dimensions (in pixel)
                .centerCrop()
                .into(simpleTarget2);

        Glide.with(this)
                .load(MediaUrl)
                .asBitmap()
                .fitCenter()
                .into(simpleTarget);





    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.toolbarbackImageView:
                finish();
                break;
            case R.id.toolbarNextImageView:
                    Intent i = new Intent(FiltersActivity.this, TagItemActivity.class);
                    i.putExtra("FilteredBitmap",MediaUrl);
                    i.putExtra("FilterID",choosenFilter);
                    startActivity(i);

                break;
            case R.id.filterNormalImageView:
                lookImage.setImageBitmap(originalBitmap);
                choosenFilter = 0;
                break;

            case R.id.filterBlackandwhiteImageView:
                //lookImage.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.GRAY_STYLE));
                loadLookBitmap(BitmapFilter.GRAY_STYLE, lookImage);

                choosenFilter = BitmapFilter.GRAY_STYLE;
                break;

            case R.id.filterOldPhotoImageView:
                //lookImage.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.OLD_STYLE));
                loadLookBitmap(BitmapFilter.OLD_STYLE, lookImage);

                choosenFilter = BitmapFilter.OLD_STYLE;

                break;

            case R.id.filterHDRImageView:
                //lookImage.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.HDR_STYLE));
                loadLookBitmap(BitmapFilter.HDR_STYLE, lookImage);

                choosenFilter = BitmapFilter.HDR_STYLE;

                break;

            case R.id.filterLomoImageView:
                //lookImage.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.LOMO_STYLE));
                loadLookBitmap(BitmapFilter.LOMO_STYLE, lookImage);

                choosenFilter = BitmapFilter.LOMO_STYLE;

                break;

            case R.id.filterLightImageView:
                //lookImage.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.LIGHT_STYLE));
                loadLookBitmap(BitmapFilter.LIGHT_STYLE, lookImage);

                choosenFilter = BitmapFilter.LIGHT_STYLE;

                break;

            case R.id.filterAVGBlurImageView:
                //lookImage.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.AVERAGE_BLUR_STYLE));
                loadLookBitmap(BitmapFilter.AVERAGE_BLUR_STYLE, lookImage);

                choosenFilter = BitmapFilter.AVERAGE_BLUR_STYLE;

                break;

            case R.id.filterGothamImageView:
                //lookImage.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.GOTHAM_STYLE));
                loadLookBitmap(BitmapFilter.GOTHAM_STYLE, lookImage);

                choosenFilter = BitmapFilter.GOTHAM_STYLE;

                break;


            case R.id.filterInvertImageView:
                //lookImage.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.INVERT_STYLE));
                loadLookBitmap(BitmapFilter.INVERT_STYLE, lookImage);

                choosenFilter = BitmapFilter.INVERT_STYLE;

                break;


            case R.id.filterSharpenImageView:
                //lookImage.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.SHARPEN_STYLE));
                loadLookBitmap(BitmapFilter.SHARPEN_STYLE, lookImage);

                choosenFilter = BitmapFilter.SHARPEN_STYLE;

                break;


            case R.id.filterGblurImageView:
                //lookImage.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.GAUSSIAN_BLUR_STYLE));
                loadLookBitmap(BitmapFilter.GAUSSIAN_BLUR_STYLE, lookImage);

                choosenFilter = BitmapFilter.GAUSSIAN_BLUR_STYLE;

                break;


            case R.id.filterMotionBlurImageView:
                //lookImage.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.MOTION_BLUR_STYLE));
                loadLookBitmap(BitmapFilter.MOTION_BLUR_STYLE, lookImage);

                choosenFilter = BitmapFilter.MOTION_BLUR_STYLE;

                break;

            case R.id.filterSoftGlowImageView:
                //lookImage.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.SOFT_GLOW_STYLE));
                loadLookBitmap(BitmapFilter.SOFT_GLOW_STYLE, lookImage);

                choosenFilter = BitmapFilter.SOFT_GLOW_STYLE;

                break;


            case R.id.filterTVImageView:
                //lookImage.setImageBitmap(BitmapFilter.changeStyle(originalBitmap, BitmapFilter.TV_STYLE));
                loadLookBitmap(BitmapFilter.TV_STYLE, lookImage);

                choosenFilter = BitmapFilter.TV_STYLE;

                break;
        }
    }


    class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap> {
        private final ImageView imageViewx;
        private int filterID = 0;

        public BitmapWorkerTask(ImageView imageView) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            imageViewx =  imageView;
        }

        // Decode image in background.
        @Override
        protected Bitmap doInBackground(Integer... params) {
            filterID = params[0];
            return BitmapFilter.changeStyle(smallBitmap, filterID);
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(Bitmap bitmap) {
                    imageViewx.setImageBitmap(bitmap);
                }


    }

    class BitmapWorkerTask2 extends AsyncTask<Integer, Void, Bitmap> {
        private final ImageView imageViewx;
        private int filterID = 0;

        public BitmapWorkerTask2(ImageView imageView) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            imageViewx =  imageView;
        }

        // Decode image in background.
        @Override
        protected Bitmap doInBackground(Integer... params) {
            filterID = params[0];
            return BitmapFilter.changeStyle(originalBitmap, filterID);
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            imageViewx.setImageBitmap(bitmap);
            indicator.setVisibility(View.GONE);

        }


    }

    public void loadBitmap(int resId, ImageView imageView ) {

        BitmapWorkerTask task = new BitmapWorkerTask(imageView);
        task.execute(resId);
    }

    public void loadLookBitmap(int resId, ImageView imageView) {
        indicator.setVisibility(View.VISIBLE);

        if (oldtask2 !=null)
            oldtask2.cancel(true);
        BitmapWorkerTask2 task = new BitmapWorkerTask2(imageView);
        task.execute(resId);
        oldtask2 = task;

    }



}
