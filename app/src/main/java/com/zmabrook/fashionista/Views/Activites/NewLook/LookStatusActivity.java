package com.zmabrook.fashionista.Views.Activites.NewLook;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Item;
import com.zmabrook.fashionista.Domain.Entities.Look;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.LooksMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.HomeActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LookStatusActivity extends AppCompatActivity {
    @BindView(R.id.toolbarbackImageView)
    ImageView toolbarbackImageView;


    @BindView(R.id.doneTextView)
    TextView publishTextView;

    @BindView(R.id.titleTextView)
    TextView titleTextView;

    @BindView(R.id.lookImage)
    ImageView lookImage;

    @BindView(R.id.lookTypeTextView)
    TextView lookTypeTextView;


    @BindView(R.id.mainLayoutContainer)
    RelativeLayout mainLayout;

    @BindView(R.id.lookStatusEditText)
    EditText lookStatusEditText;

    @BindView(R.id.indicator)
    ProgressBar indicator;


    ImageView tagImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_look_status);
        ButterKnife.bind(this);
        publishTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        lookTypeTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        lookStatusEditText.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));


        titleTextView.setText("");
        publishTextView.setText("Publish");
        lookImage.setImageBitmap(TagItemActivity.filteredBitmap);

        lookTypeTextView.setText(TagItemActivity.newLook.getLookType().getName());
        toolbarbackImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        publishTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!lookStatusEditText.getText().toString().equals("")) {
                    TagItemActivity.newLook.setTitle(lookStatusEditText.getText().toString());

                } else {
                    TagItemActivity.newLook.setTitle("-");
                }

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(LookStatusActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                indicator.setVisibility(View.VISIBLE);
                submitLook();

            }
        });

        ArrayList<Item> items = TagItemActivity.newLook.getItems();
        if (items.size() != 0) {
            for (int i = 0; i < items.size(); i++) {
                Item item = items.get(i);

                tagImageView = new ImageView(LookStatusActivity.this);

                tagImageView.setImageResource(R.mipmap.xytag);
                int tagDimension = (int) getResources().getDimension(R.dimen.tag);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) getResources().getDimension(R.dimen.tag), (int) getResources().getDimension(R.dimen.tag));
                params.leftMargin = (int) item.getX();
                params.topMargin = (int) item.getY();

                mainLayout.addView(tagImageView, params);

            }
        }
    }

    private void submitLook() {
        JSONObject look = new JSONObject();
        JSONObject mainobject = new JSONObject();
        Look newLook = TagItemActivity.newLook;
        try {
            look.put("title", newLook.getTitle());
            look.put("media", newLook.getImageUrl());
            look.put("media_type", "image");
            look.put("look_type_id", newLook.getLookType().getId());


            JSONObject userLocation = new JSONObject();
            userLocation.put("lang", "32.2343");
            userLocation.put("lat", "31.4245345");
            userLocation.put("city", "Cairo");
            userLocation.put("country", "Egypt");
            userLocation.put("district", "Cairo");
            look.put("user_location_attributes", userLocation);

            JSONObject items = new JSONObject();
            for (int i = 0; i < newLook.getItems().size(); i++) {
                Item item = newLook.getItems().get(i);
                JSONObject object = new JSONObject();
                object.put("x", item.getX());
                if (item.getPrice() != 0)
                    object.put("price", item.getPrice());
                object.put("category_id", item.getCategory().getId());
                object.put("y", item.getY());
                object.put("y", item.getY());

                JSONObject itembrand = new JSONObject();
                if (item.getWholeBrand() != null) {
                    itembrand.put("title", item.getWholeBrand().getName());
                } else {
                    itembrand.put("title", item.getBrand());
                }
                object.put("brand_attributes", itembrand);


                JSONObject itemLocation = new JSONObject();
                itemLocation.put("lang", item.getLocation().getLatLng().longitude);
                itemLocation.put("lat", item.getLocation().getLatLng().latitude);
                itemLocation.put("city", item.getLocation().getCity());
                itemLocation.put("country", item.getLocation().getCountry());
                itemLocation.put("district", item.getLocation().getDistrict());
                object.put("location_attributes", itemLocation);


                items.put(String.valueOf(i), object);

            }
            look.put("items_attributes", items);

            mainobject = new JSONObject();
            mainobject.put("look", look);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody requestBody = new RequestBody();
        requestBody.setJsonObjectRequestBody(mainobject);
        Log.d(CommonConstants.APP_TAG, look.toString());
        LooksMapper looksMapper = new LooksMapper(LookStatusActivity.this, CommonConstants.CREATE_NEW_LOOK);
        looksMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
            @Override
            public void onSuccess(Object result) {
                Toast.makeText(LookStatusActivity.this, "Look Created Successfully", Toast.LENGTH_LONG);
                Intent i = new Intent(LookStatusActivity.this, HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);
            }

            @Override
            public void onError(Exception e, Object result) {

            }
        }, requestBody);
    }
}
