package com.zmabrook.fashionista.Views.Activites.NewLook;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Adapters.HomePagerViewAdapter;
import com.zmabrook.fashionista.Views.Adapters.NewLookPagerViewAdapter;
import com.zmabrook.fashionista.Views.Fragments.NewLook.GalleryFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewLookActivity extends AppCompatActivity {
    @BindView(R.id.toolbarBackTextView)
    TextView cancelTextView;

    @BindView(R.id.toolbarNextImageView)
    ImageView toolbarNextImageView;

    @BindView(R.id.galleryIamgeView)
    ImageView galleryIamgeView;

    @BindView(R.id.photoIamgeView)
    ImageView photoIamgeView;

    @BindView(R.id.videoIamgeView)
    ImageView videoIamgeView;

    @BindView(R.id.titleTextView)
    TextView titleTextView;

    @BindView(R.id.pager)
    ViewPager viewPager;

    NewLookPagerViewAdapter mAdapter;
    public String selectedMediaUrl = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_look);
        ButterKnife.bind(this);


        cancelTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        titleTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        mAdapter = new NewLookPagerViewAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(4);

        galleryIamgeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(0, true);


            }
        });

        photoIamgeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(1,true);

            }
        });

        videoIamgeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(2, true);

            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        galleryIamgeView.setImageResource(R.mipmap.uploadgalleryon);
                        photoIamgeView.setImageResource(R.mipmap.uploadcameraoff);
                        videoIamgeView.setImageResource(R.mipmap.uploadvideooff);
                        titleTextView.setText("Gallery");
                        break;

                    case 1:
                        photoIamgeView.setImageResource(R.mipmap.uploadcameraon);
                        galleryIamgeView.setImageResource(R.mipmap.uploadgalleryoff);
                        videoIamgeView.setImageResource(R.mipmap.uploadvideooff);
                        titleTextView.setText("Photo");

                        break;
                    case 2:
                        videoIamgeView.setImageResource(R.mipmap.uploadvideoon);
                        galleryIamgeView.setImageResource(R.mipmap.uploadgalleryoff);
                        photoIamgeView.setImageResource(R.mipmap.uploadcameraoff);
                        titleTextView.setText("Video");

                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        toolbarNextImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedMediaUrl !=null){
                    Intent i = new Intent(NewLookActivity.this,FiltersActivity.class);
                    i.putExtra("MediaUrl",selectedMediaUrl);
                    startActivity(i);

                }else {
                    Toast.makeText(NewLookActivity.this,"Please select a photo first",Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment =mAdapter.getCurrentFragment();
        if (viewPager.getCurrentItem()==0 )
        {
            GalleryFragment fragment =((GalleryFragment)currentFragment);
            if(fragment.choosenImageView.getVisibility() == View.VISIBLE){
                fragment.choosenImageView.setVisibility(View.GONE);
                fragment.closeImageView.setVisibility(View.GONE);
            }else {
                super.onBackPressed();
            }
        }else {
            super.onBackPressed();
        }

    }
}
