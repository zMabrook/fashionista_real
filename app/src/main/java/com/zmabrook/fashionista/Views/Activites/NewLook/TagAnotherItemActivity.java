package com.zmabrook.fashionista.Views.Activites.NewLook;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zmabrook.fashionista.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TagAnotherItemActivity extends AppCompatActivity {
    @BindView(R.id.toolbarbackImageView)
    ImageView toolbarbackImageView;


    @BindView(R.id.doneTextView)
    TextView doneTextView;

    @BindView(R.id.titleTextView)
    TextView titleTextView;

    @BindView(R.id.choosenBrandName)
    TextView categoryTextView;

    @BindView(R.id.brandTextView)
    TextView brandTextView;

    @BindView(R.id.placeTextView)
    TextView placeTextView;

    @BindView(R.id.priceTextView)
    TextView priceTextView;

    @BindView(R.id.TagAnotherItemButton)
    Button TagAnotherItemButton;

    @BindView(R.id.choosenCategoryIcon)
    ImageView choosenCategoryIcon;

    @BindView(R.id.choosenBrandIcon)
    ImageView choosenBrandIcon;

    @BindView(R.id.lookImage)
    ImageView lookImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_another_item);
        ButterKnife.bind(this);
        titleTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        categoryTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        brandTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        placeTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        priceTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        doneTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));

        toolbarbackImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        doneTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TagItemActivity.newLook.getItems().add(TagItemActivity.item);
                Intent i = new Intent(TagAnotherItemActivity.this,ChooseLookTypeActivity.class);
                startActivity(i);

            }
        });

        lookImage.setImageBitmap(TagItemActivity.filteredBitmap);

        Picasso.with(TagAnotherItemActivity.this).load(TagItemActivity.item.getCategory().getIconUrl())
                .into(choosenCategoryIcon, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        Picasso.with(TagAnotherItemActivity.this).load(TagItemActivity.item.getCategory().getIconUrl())
                                .into(choosenCategoryIcon);
                    }
                });

        categoryTextView.setText(TagItemActivity.item.getCategory().getName());

        if (TagItemActivity.item.getWholeBrand() != null) {
            Picasso.with(TagAnotherItemActivity.this).load(TagItemActivity.item.getWholeBrand().getIconUrl())
                    .into(choosenBrandIcon, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(TagAnotherItemActivity.this).load(TagItemActivity.item.getWholeBrand().getIconUrl())
                                    .into(choosenBrandIcon);
                        }
                    });

            brandTextView.setText(TagItemActivity.item.getWholeBrand().getName());


        }else {
            brandTextView.setText(TagItemActivity.item.getBrand());
            choosenBrandIcon.setImageBitmap(null);
        }

        if (TagItemActivity.item.getLocation().getCountry()!=null &&
                TagItemActivity.item.getLocation().getCity()!=null &&
                TagItemActivity.item.getLocation().getDistrict()!=null){
            placeTextView.setText(TagItemActivity.item.getLocation().getDistrict()+"\n"
            +TagItemActivity.item.getLocation().getCity()+"/"+TagItemActivity.item.getLocation().getCountry());

        }else if (TagItemActivity.item.getLocation().getCountry()!=null &&
                TagItemActivity.item.getLocation().getCity()!=null){
            placeTextView.setText(TagItemActivity.item.getLocation().getCity()+"/"+TagItemActivity.item.getLocation().getCountry());
        }

        priceTextView.setText("Item Price:   $"+TagItemActivity.item.getPrice());

        TagAnotherItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TagItemActivity.newLook.getItems().add(TagItemActivity.item);
                Intent intent = new Intent(TagAnotherItemActivity.this,TagItemActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });
    }
}
