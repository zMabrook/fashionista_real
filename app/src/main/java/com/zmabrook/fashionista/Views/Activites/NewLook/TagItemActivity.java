package com.zmabrook.fashionista.Views.Activites.NewLook;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Category;
import com.zmabrook.fashionista.Domain.Entities.Item;
import com.zmabrook.fashionista.Domain.Entities.Look;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.SearchMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.Abstract.FashionistaActivity;
import com.zmabrook.fashionista.Views.Adapters.CategoriesRecyclerViewAdapter;
import com.zmabrook.fashionista.Views.Adapters.NewLookCategoriesRecyclerViewAdapter;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.Ragnarok.BitmapFilter;

public class TagItemActivity extends FashionistaActivity {
    @BindView(R.id.lookImage)
    ImageView lookImage;


    @BindView(R.id.indicator)
    ProgressBar indicator;


    @BindView(R.id.toolbarbackImageView)
    ImageView toolbarbackImageView;

    @BindView(R.id.toolbarNextImageView)
    ImageView toolbarNextImageView;

    @BindView(R.id.titleTextView)
    TextView titleTextView;
    PopupWindow popup;
    ImageView imageView;
    public static Look newLook;
    public static Item item;

    int screenHeight;
    int screenWidth;
    LinearLayoutManager linearLayoutManager;
    public NewLookCategoriesRecyclerViewAdapter adapter;
    public RecyclerView recyclerView;
    public ArrayList<Category> categories;

    @BindView(R.id.mainView)
    RelativeLayout mainLayout;

    Bitmap originalBitmap;
    public static Bitmap filteredBitmap;
    boolean imageLoaded = false;
    ImageView lastTagImageView;
    View lastPopuView;
    public int filterID;
    private GestureDetector gestureDetector;

    SimpleTarget simpleTarget = new SimpleTarget<Bitmap>() {
        @Override
        public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
            originalBitmap = resource;
            loadLookBitmap(filterID, lookImage);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_item);
        ButterKnife.bind(this);
        newLook = new Look();
        newLook.setItems(new ArrayList<Item>());
        titleTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        titleTextView.setText("Tag Item");


        toolbarbackImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        toolbarNextImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item != null && item.getCategory() != null) {
                    Intent i = new Intent(TagItemActivity.this, ChooseBrandActivity.class);
                    startActivity(i);

                } else {
                    Intent i = new Intent(TagItemActivity.this, ChooseLookTypeActivity.class);
                    startActivity(i);
                }

            }
        });

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        screenHeight = displaymetrics.heightPixels;
        screenWidth = displaymetrics.widthPixels;

        String bitmapUrl = (String) getIntent().getExtras().get("FilteredBitmap");
        filterID = (int) getIntent().getExtras().get("FilterID");
        gestureDetector = new GestureDetector(this, new SingleTapConfirm());


        Glide.with(this)
                .load(bitmapUrl)
                .asBitmap()
                .into(simpleTarget);


        lookImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();
        item = null;
        if (popup != null)
            popup.dismiss();
        if (imageView != null)
            imageView.setVisibility(imageView.GONE);

        lookImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (imageLoaded) {
                    if (gestureDetector.onTouchEvent(event)) {
                        if (lastTagImageView != null) {
                            mainLayout.removeView(lastTagImageView);
                            mainLayout.removeView(lastPopuView);
                        }

                        int touchX = (int) event.getX();
                        int touchY = (int) event.getY();

                        int[] viewCoords = new int[2];


                        lookImage.getLocationOnScreen(viewCoords);


                        int imageX = touchX - viewCoords[0]; // viewCoords[0] is the X coordinate
                        int imageY = touchY + viewCoords[1]; // viewCoords[1] is the y coordinate

                        imageView = new ImageView(TagItemActivity.this);

                        imageView.setImageResource(R.mipmap.xytag);
                        int tagDimension = (int) getResources().getDimension(R.dimen.tag);
                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) getResources().getDimension(R.dimen.tag), (int) getResources().getDimension(R.dimen.tag));
                        params.leftMargin = imageX - ((int) getResources().getDimension(R.dimen.tag) / 2);
                        params.topMargin = imageY - (((int) getResources().getDimension(R.dimen.tag)));

                        Point tagPoint = getTagPoint(params.leftMargin, params.topMargin);
                        params.leftMargin = tagPoint.x;
                        params.topMargin = tagPoint.y;
                        item = new Item();
                        item.setX(params.leftMargin);
                        item.setY(params.topMargin);
                        mainLayout.addView(imageView, params);
                        lastTagImageView = imageView;

                        Point popupPoint = getPopUpPoint(params.leftMargin + (tagDimension / 2), params.topMargin + (tagDimension / 2));

                        /*View view = new View(TagItemActivity.this);
                        view.setBackgroundColor(getResources().getColor(R.color.peopleNameGray));
*/
                        showPopup(TagItemActivity.this, popupPoint);


                        /*RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams((int) getResources().getDimension(R.dimen.popupWidth), (int) getResources().getDimension(R.dimen.popupHeight));
                        params1.leftMargin = popupPoint.x;
                        params1.topMargin = popupPoint.y;
                        mainLayout.addView(view, params1);
                        lastPopuView = view;*/


                        return true;

                    }
                }

                return false;
            }
        });

    }

    class BitmapWorkerTask2 extends AsyncTask<Integer, Void, Bitmap> {
        private final ImageView imageViewx;
        private int filterID = 0;

        public BitmapWorkerTask2(ImageView imageView) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            imageViewx = imageView;
        }

        // Decode image in background.
        @Override
        protected Bitmap doInBackground(Integer... params) {
            filterID = params[0];
            if (filterID == 0)
                return originalBitmap;

            return BitmapFilter.changeStyle(originalBitmap, filterID);
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            imageViewx.setImageBitmap(bitmap);
            filteredBitmap = bitmap;
            indicator.setVisibility(View.GONE);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            filteredBitmap.compress(Bitmap.CompressFormat.JPEG, 60, baos); //bm is the bitmap object
            byte[] byteArrayImage = baos.toByteArray();


            String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
            encodedImage = "data:image/jpeg;base64," + encodedImage;
            newLook.setImageUrl(encodedImage);
            newLook.setMediaType(Look.MediaType.Image);

        }


    }

    public void loadLookBitmap(int filterId, ImageView imageView) {
        indicator.setVisibility(View.VISIBLE);
        BitmapWorkerTask2 task = new BitmapWorkerTask2(imageView);
        task.execute(filterId);
        imageLoaded = true;

    }

    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            return true;
        }
    }

    private Point getPopUpPoint(int leftMargin, int topMargin) {
        Point p = new Point();
        if (leftMargin + (int) getResources().getDimension(R.dimen.popupWidth) > screenWidth) {
            if (topMargin + (int) getResources().getDimension(R.dimen.popupHeight) > screenHeight) {
                //not enought width or height
                p.x = leftMargin - (int) getResources().getDimension(R.dimen.popupWidth);
                p.y = topMargin - (int) getResources().getDimension(R.dimen.popupHeight);
                return p;
            } else {
                //not enought width
                p.x = leftMargin - (int) getResources().getDimension(R.dimen.popupWidth);
                p.y = topMargin;
                return p;

            }

        } else {
            if (topMargin + (int) getResources().getDimension(R.dimen.popupHeight) > screenHeight) {
                //not enought height
                p.x = leftMargin;
                p.y = topMargin - (int) getResources().getDimension(R.dimen.popupHeight);

                return p;

            } else {
                //engouh widht and height :: ordinary
                p.x = leftMargin;
                p.y = topMargin;
                return p;

            }
        }
    }


    private Point getTagPoint(int leftMargin, int topMargin) {
        Point p = new Point();
        if (leftMargin + (int) getResources().getDimension(R.dimen.tag) > screenWidth) {
            if (topMargin + (int) getResources().getDimension(R.dimen.tag) > screenHeight) {
                //not enought width or height
                p.x = leftMargin - (int) getResources().getDimension(R.dimen.tag);
                p.y = topMargin - (int) getResources().getDimension(R.dimen.tag);
                return p;
            } else {
                //not enought width
                p.x = leftMargin - (int) getResources().getDimension(R.dimen.tag);
                p.y = topMargin;
                return p;

            }

        } else {
            if (topMargin + (int) getResources().getDimension(R.dimen.tag) > screenHeight) {
                //not enought height
                p.x = leftMargin;
                p.y = topMargin - (int) getResources().getDimension(R.dimen.tag);

                return p;

            } else {
                //engouh widht and height :: ordinary
                p.x = leftMargin;
                p.y = topMargin;
                return p;

            }
        }
    }

    private void showPopup(final Activity context, Point p) {
        int popupWidth = (int) getResources().getDimension(R.dimen.popupWidth);
        int popupHeight = (int) getResources().getDimension(R.dimen.popupHeight);
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);


        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.item_tag_popup, viewGroup);

        popup = new PopupWindow(context);
        popup.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);

        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x, p.y);

        recyclerView = (RecyclerView) layout.findViewById(R.id.itemRecyclerView);
        linearLayoutManager = new LinearLayoutManager(TagItemActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);


        RequestBody requestBody = new RequestBody();
        HashMap<String, String> queryparams = new HashMap<String, String>();


        SearchMapper mapper = new SearchMapper(TagItemActivity.this, CommonConstants.CATEGORIES_SEARCH_ACTION);
        mapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_ARRAY, Request.Method.GET, new FashionistaListener() {
            @Override
            public void onSuccess(Object result) {
                categories = (ArrayList<Category>) result;


                adapter = new NewLookCategoriesRecyclerViewAdapter(TagItemActivity.this, categories);
                recyclerView.setAdapter(adapter);
                recyclerView.setHasFixedSize(true);

            }

            @Override
            public void onError(Exception e, Object result) {

            }
        }, requestBody);


    }

}
