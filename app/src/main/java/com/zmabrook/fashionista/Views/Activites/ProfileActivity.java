package com.zmabrook.fashionista.Views.Activites;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.LooksMapper;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Entities.LooksResult;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Domain.Mapper.UserMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.MyProfile.MyProfileActivity;
import com.zmabrook.fashionista.Views.Adapters.ProfileTimeLineRecyclerViewAdapter;
import com.zmabrook.fashionista.Views.Custom.FashionistaPopupMenu;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.toolbarmenuImageview)
    ImageView backImageView;

    @BindView(R.id.toolbarprofileImageView)
    ImageView profileButton;

    @BindView(R.id.coverImageView)
    ImageView coverImageView;

    @BindView(R.id.settingsImageView)
    ImageView settingsImageView;

    @BindView(R.id.bioTextView)
    TextView bioTextView;

    @BindView(R.id.profileTimelineRecylerView)
    RecyclerView recyclerView;

    @BindView(R.id.timelineTextView)
    TextView timelineTextView;

    @BindView(R.id.followImageView)
    ImageView followImageView;

    @BindView(R.id.avatarImageView)
    CircleImageView avatarImageView;

    @BindView(R.id.nameTextView)
    TextView nameTextView;

    @BindView(R.id.numOfLikesTextView)
    TextView likesTextView;

    @BindView(R.id.numOfFollowersTextView)
    TextView followersTextView;

    @BindView(R.id.numOfpinsTextView)
    TextView pinsTextView;

    @BindView(R.id.numOfViewsTextView)
    TextView viewsTextView;

    @BindView(R.id.trloadingBar)
    TextView loadingBar;

    User user;

    boolean followed = false;
    LinearLayoutManager linearLayoutManager;
    int mFirstVisibleItem, mVisibleItemCount, mTotalItemCount, mLastFirstVisibleItem;
    boolean isLoading = false;
    boolean gotToEnd = false;
    LooksResult result;
    public static int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = (User) getIntent().getExtras().getSerializable(CommonConstants.PREFS_USER);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);


        backImageView.setImageResource(R.mipmap.iconback);
        bioTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        timelineTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        nameTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        likesTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        viewsTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        followersTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        pinsTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));


        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);


        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this,MyProfileActivity.class);
                startActivity(intent);
            }
        });

        RequestBody requestBody = new RequestBody();


        HashMap<String, String> pathParams = new HashMap();
        pathParams.put("id", user.getUserID());
        requestBody.setPathParams(pathParams);

        final UserMapper userMapper = new UserMapper(this, CommonConstants.GET_USER_ACTION);
        userMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
            @Override
            public void onSuccess(Entity result) {
                final User user = (User) result;
                if ((user != null)) {
                    ProfileActivity.this.user = user;
                    nameTextView.setText(user.getName());



                    if (!user.isfollowed()){
                        followImageView.setImageResource(R.mipmap.iconequestpurple);
                    }else {
                        followImageView.setImageResource(R.mipmap.iconfollowedpurple);
                    }


                    if (user.getBio() != null)
                        bioTextView.setText(user.getBio());


                    if (user.getFollowersCount() != null)
                        followersTextView.setText(user.getFollowersCount());

                    if (user.getLikesCount() != null)
                        likesTextView.setText(user.getLikesCount());

                    if (user.getLooksCount() != null)
                        viewsTextView.setText(user.getLooksCount());

                    if (user.getPinsCount() != null)
                        viewsTextView.setText(user.getPinsCount());


                    if (user.getImageUrl() != null) {
                        Picasso.with(ProfileActivity.this)
                                .load(user.getImageUrl())
                                .into(avatarImageView, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError() {
                                        Picasso.with(ProfileActivity.this)
                                                .load(user.getImageUrl())
                                                .into(avatarImageView);
                                    }
                                });
                    }

                }


            }

            @Override
            public void onError(Exception e, Entity result) {


            }
        }, requestBody);


        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        followImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (user.isfollowed()){
                    //make unfollow request;

                    RequestBody requestBody = new RequestBody();
                    HashMap<String, String> pathParams =new HashMap<String, String>();
                    pathParams.put("id",user.getUserID());

                    requestBody.setPathParams(pathParams);
                    UserMapper userMapper = new UserMapper(ProfileActivity.this, CommonConstants.UNFOLLOW_USER_ACTION);
                    userMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            if (followImageView!=null){
                                followImageView.setImageResource(R.mipmap.iconequestpurple);
                                user.setIsfollowed(false);
                            }
                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            if (followImageView!=null){
                                followImageView.setImageResource(R.mipmap.iconfollowedpurple);
                                user.setIsfollowed(true);

                            }
                        }
                    },requestBody);

                    followImageView.setImageResource(R.mipmap.iconequestpurple);
                    user.setIsfollowed(false);

                }else {
                    //make follow request;


                    RequestBody requestBody = new RequestBody();
                    HashMap<String, String> pathParams =new HashMap<String, String>();
                    pathParams.put("id", user.getUserID());

                    requestBody.setPathParams(pathParams);
                    UserMapper userMapper = new UserMapper(ProfileActivity.this, CommonConstants.FOLLOW_USER_ACTION);
                    userMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            if (followImageView!=null){
                                followImageView.setImageResource(R.mipmap.iconfollowedpurple);
                                user.setIsfollowed(true);

                            }
                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            if (followImageView!=null){
                                followImageView.setImageResource(R.mipmap.iconequestpurple);
                                user.setIsfollowed(false);
                            }
                        }
                    },requestBody);

                    followImageView.setImageResource(R.mipmap.iconfollowedpurple);
                    user.setIsfollowed(true);
                }

            }
        });


        settingsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                FashionistaPopupMenu popup = new FashionistaPopupMenu(ProfileActivity.this, settingsImageView);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.profile_settings_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getOrder();
                        switch (id) {
                            case 0:


                                return true;
                            case 1:
                                new AlertDialog.Builder(ProfileActivity.this)
                                        .setTitle("Block")
                                        .setMessage("Are you sure you want to delete " + user.getName() + " ?")
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // continue with delete
                                                RequestBody requestBody1 = new RequestBody();
                                                HashMap<String, String> pathParams = new HashMap<String, String>();
                                                pathParams.put("id", user.getUserID());
                                                requestBody1.setPathParams(pathParams);

                                                UserMapper userMapper1 = new UserMapper(ProfileActivity.this, CommonConstants.BLOCK_USER_ACTION);
                                                userMapper1.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                                                    @Override
                                                    public void onSuccess(Object result) {
                                                        Toast.makeText(ProfileActivity.this, user.getName() + " blocked", Toast.LENGTH_LONG);
                                                        finish();
                                                    }

                                                    @Override
                                                    public void onError(Exception e, Object result) {
                                                        Toast.makeText(ProfileActivity.this, "error blocking " + user.getName() + ". Please try again later", Toast.LENGTH_LONG);

                                                    }
                                                }, requestBody1);
                                            }
                                        })
                                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // do nothing
                                            }
                                        })
                                        .show();
                                return false;
                        }

                        return false;
                    }
                });
                popup.show();//showing popup menu

            }
        });



        requestBody = new RequestBody();

        HashMap<String, String> queryparams = new HashMap<String, String>();
        queryparams.put("page", String.valueOf(page));



        requestBody.setQueryParams(queryparams);
        requestBody.setPathParams(pathParams);
        LooksMapper looksMapper = new LooksMapper(this, CommonConstants.GET_USER_LOOKS_ACTION);
        looksMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
            @Override
            public void onSuccess(Entity result) {
                final LooksResult looksResult = (LooksResult) result;
                if (result != null) {

                    final ProfileTimeLineRecyclerViewAdapter adapter = new ProfileTimeLineRecyclerViewAdapter(ProfileActivity.this, looksResult.looks);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setHasFixedSize(true);

                    if (this != null)
                        hideLoadingBar();

                    recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

                        @Override
                        public void onScrolled(final RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            mVisibleItemCount = recyclerView.getChildCount();
                            mTotalItemCount = linearLayoutManager.getItemCount();
                            mFirstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                            //checking if no more results to get
                            if (mTotalItemCount >= looksResult.numberOfLooks) {
                                gotToEnd = true;

                                return;
                            }

                            //checking if scrolling down
                            if (mLastFirstVisibleItem < mFirstVisibleItem) {
                                int lastInScreen = mFirstVisibleItem + mVisibleItemCount;

                                //check if got to last item and not loading and the item count is bigger than default:20
                                if ((lastInScreen == mTotalItemCount) && !(isLoading) && !gotToEnd) {
                                    showLoadingBar();
                                    isLoading = true;


                                    RequestBody requestBody = new RequestBody();


                                    HashMap<String, String> queryparams = new HashMap<String, String>();
                                    queryparams.put("page", String.valueOf(++page));

                                    requestBody.setQueryParams(queryparams);

                                    LooksMapper looksMapper = new LooksMapper(ProfileActivity.this, CommonConstants.TRENDING_ACTION);
                                    looksMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener() {
                                        @Override
                                        public void onSuccess(Object result) {
                                            LooksResult looksResult = (LooksResult) result;
                                            result = looksResult;
                                            if (looksResult.numberOfLooks != 0) {
                                                adapter.add(looksResult.looks);

                                            }
                                            hideLoadingBar();
                                            isLoading = false;


                                        }

                                        @Override
                                        public void onError(Exception e, Object result) {
                                            isLoading = false;
                                            hideLoadingBar();
                                        }
                                    }, requestBody);

                                }
                                mLastFirstVisibleItem = mFirstVisibleItem;

                            }

                        }
                    });

                }
            }

            @Override
            public void onError(Exception e, Entity result) {
                Toast.makeText(ProfileActivity.this, "Server error", Toast.LENGTH_LONG);
                if (ProfileActivity.this != null)
                    hideLoadingBar();

            }
        }, requestBody);

    }


    private void showLoadingBar() {
        loadingBar.setText("Loading...");
        loadingBar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        loadingBar.setVisibility(View.VISIBLE);
        loadingBar
                .animate()
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator(1));
    }

    private void hideLoadingBar() {
        loadingBar.setText("Loading...");
        loadingBar
                .animate()
                .translationY(loadingBar.getHeight())
                .setInterpolator(new AccelerateInterpolator(1));
        loadingBar.setVisibility(View.GONE);

    }
}
