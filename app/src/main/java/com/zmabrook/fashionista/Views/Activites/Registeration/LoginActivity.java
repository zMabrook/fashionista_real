package com.zmabrook.fashionista.Views.Activites.Registeration;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.SignUpMapper;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Entities.SignUpResult;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.HomeActivity;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {
    User user;

    @BindView(R.id.signupTitleTextView)
    TextView title;

    @BindView(R.id.usernameEditText)
    EditText usernameEditText;

    @BindView(R.id.passwordEditText)
    EditText passwordEditText;

    @BindView(R.id.forgotYourPassword)
    TextView forgotYourpassowrdTextView;

    @BindView(R.id.loginButton)
    Button loginButton;

    @BindView(R.id.signupNow)
    TextView signUpNowTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        title.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        usernameEditText.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        passwordEditText.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        forgotYourpassowrdTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        loginButton.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        signUpNowTextView.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));


        signUpNowTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(i);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!usernameEditText.getText().toString().equals("") &&
                        !passwordEditText.getText().toString().equals("")) {

                    JSONObject object = new JSONObject();
                    try {
                        object.put("email", usernameEditText.getText().toString());
                        object.put("password", passwordEditText.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    RequestBody requestBody = new RequestBody();
                    requestBody.setJsonObjectRequestBody(object);
                    requestBody.cancelRequestsWithSameTag =true;

                    SignUpMapper mapper = new SignUpMapper(LoginActivity.this, CommonConstants.LOGIN_ACTION);

                    mapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            if (result !=null){
                                Toast.makeText(LoginActivity.this, "Login Succesful", Toast.LENGTH_LONG).show();
                                User user = (User) result;


                                SharedPreferences settings = getApplicationContext().getSharedPreferences(CommonConstants.PREFS_NAME, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = settings.edit();
                                Gson gson = new Gson();
                                String json = gson.toJson(user);
                                editor.putString(CommonConstants.PREFS_USER, json);


                                //set isLoggedin boolean in shared prefs with true
                                editor.putBoolean(CommonConstants.PREFS_LOGGED_IN, true);
                                editor.commit();


                                Intent i = new Intent(LoginActivity.this, HomeActivity.class);


                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);




                            }else {
                                Toast.makeText(LoginActivity.this, "Server error", Toast.LENGTH_LONG).show();
                                return;
                            }

                        }

                        @Override
                        public void onError(Exception e, Object result1) {
                            if (result1 !=null){
                                SignUpResult result = (SignUpResult) result1;
                            Toast.makeText(LoginActivity.this, result.Errormessages, Toast.LENGTH_LONG).show();
                            }else {
                                Toast.makeText(LoginActivity.this, "Server error", Toast.LENGTH_LONG).show();

                            }

                        }
                    }, requestBody);


                } else {
                    Toast.makeText(LoginActivity.this, getString(R.string.fillData), Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });


    }
}
