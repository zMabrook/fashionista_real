package com.zmabrook.fashionista.Views.Activites.Registeration;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.Abstract.FashionistaActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginChoicesActivity extends FashionistaActivity {
    @BindView(R.id.loginTitle)
    TextView title;

    @BindView(R.id.loginFbButton)
    Button fbButton;

    @BindView(R.id.loginTwitterButton)
    Button twitterButton;

    @BindView(R.id.loginGplusButton)
    Button gplusButton;



    @BindView(R.id.loginEmailButton)
    Button emailButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choices_login);
        ButterKnife.bind(this);

        title.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        fbButton.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        twitterButton.setTypeface(Typeface.createFromAsset(getAssets(),"helveticaLight.ttf"));
        gplusButton.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        emailButton.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));


    }
}
