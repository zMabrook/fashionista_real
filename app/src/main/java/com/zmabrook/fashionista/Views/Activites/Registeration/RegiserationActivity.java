package com.zmabrook.fashionista.Views.Activites.Registeration;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.Abstract.FashionistaActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegiserationActivity extends FashionistaActivity {
    @BindView(R.id.registerationLoginButton)
    Button loginButton;

    @BindView(R.id.registerationSignupButton)
    Button signupButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regiseration);
        ButterKnife.bind(this);

        loginButton.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        signupButton.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RegiserationActivity.this,LoginActivity.class);
                startActivity(i);
            }
        });

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RegiserationActivity.this,SignupActivity.class);
                startActivity(i);

            }
        });
    }
}
