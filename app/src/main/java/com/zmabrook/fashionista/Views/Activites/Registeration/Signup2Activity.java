package com.zmabrook.fashionista.Views.Activites.Registeration;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.Abstract.FashionistaActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Signup2Activity extends FashionistaActivity {
    User enteredUser;
    final int OPEN_GALLERY=1;
    @BindView(R.id.avatarImageView)
    ImageView avatar;

    @BindView(R.id.fullNameEditText)
    EditText fullName;

    @BindView(R.id.usernameEditText)
    EditText userName;

    @BindView(R.id.emailEditText)
    EditText email;

    @BindView(R.id.passwordEditText)
    EditText password;

    @BindView(R.id.nextButton)
    Button next;

    @BindView(R.id.alreadyhaveAnAccount)
    TextView alreadyHaveAnAccount;



    public Signup2Activity() {
        enteredUser = new User();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup2);
        ButterKnife.bind(this);

        fullName.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        userName.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        email.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        password.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        next.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        alreadyHaveAnAccount.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        final User user= (User) getIntent().getExtras().getSerializable("user");

        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                avatar.setImageBitmap(bitmap);

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        if(user !=null){
            if (user.getUsername() !=null){
                userName.setText(user.getUsername());
            }
            if (user.getEmail() !=null){
                email.setText(user.getEmail());
            }
            if (user.getName() !=null){
                fullName.setText(user.getName());
            }
            if (user.getImageUrl() !=null){
                Picasso.with(this)
                        .load(user.getImageUrl())
                        .error(R.mipmap.avatar)
                        .resize(80,80)
                        .into(target);
                enteredUser.setImageUrl(user.getImageUrl());

            }
        }




        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , OPEN_GALLERY);//one can be replaced with any action code
            }
        });





        alreadyHaveAnAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Signup2Activity.this,LoginActivity.class);
                startActivity(i);
            }
        });


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = validateData();
                if (isValid){
                    //go to next page with entered data
                    enteredUser.setName(fullName.getText().toString());
                    enteredUser.setUsername(userName.getText().toString());
                    enteredUser.setEmail(email.getText().toString());
                    enteredUser.setPassword(password.getText().toString());
                    Intent i = new Intent(Signup2Activity.this, Signup3Activity.class);
                    i.putExtra("user",enteredUser);
                    startActivity(i);
                }else {
                    return;
                }
            }
        });
    }
    boolean validateData(){
            boolean isValid = true;
        if (fullName.getText().toString().equals("")
                ||email.getText().toString().equals("")
                ||userName.getText().toString().equals("")
                ||password.getText().toString().equals("")){

            Toast.makeText(Signup2Activity.this,getString(R.string.fillData),Toast.LENGTH_LONG).show();
            isValid = false;

        }else {
            if (fullName.getText().toString().length()>100){
                Toast.makeText(Signup2Activity.this,getString(R.string.fullNameLimit),Toast.LENGTH_LONG).show();
                isValid = false;

            } else if (!userName.getText().toString().matches("^[a-zA-Z0-9]+$")){
                Toast.makeText(Signup2Activity.this,getString(R.string.usernameValidation),Toast.LENGTH_LONG).show();
                isValid = false;

            } else if(userName.getText().toString().length()>14){
                Toast.makeText(Signup2Activity.this,getString(R.string.usernameValidationLong),Toast.LENGTH_LONG).show();
                isValid = false;

            }else if(!email.getText().toString().matches("^[a-zA-Z_A-Z0-9._%+-]+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$")){
                Toast.makeText(Signup2Activity.this,getString(R.string.emailValidation),Toast.LENGTH_LONG).show();
                isValid = false;

            }else if (!password.getText().toString().matches("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$")){
                Toast.makeText(Signup2Activity.this,getString(R.string.passwordValidation),Toast.LENGTH_LONG).show();
                isValid = false;

            }

        }
        return isValid;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case OPEN_GALLERY:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = data.getData();
                        enteredUser.setImageUrl(selectedImage.toString());


                    Picasso.with(this)
                            .load(selectedImage)
                            .error(R.mipmap.avatar)
                            .resize(80,80)
                            .into(avatar);
                }
                break;
        }
    }
}
