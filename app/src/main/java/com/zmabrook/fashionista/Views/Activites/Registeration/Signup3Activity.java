package com.zmabrook.fashionista.Views.Activites.Registeration;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.SignUpMapper;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Entities.SignUpResult;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.Abstract.FashionistaActivity;
import com.zmabrook.fashionista.Views.Activites.HomeActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Signup3Activity extends FashionistaActivity {
    User user;
    final int OPEN_GALLERY = 1;
    Bitmap imageToSend;

    @BindView(R.id.avatarImageView)
    ImageView avatar;

    @BindView(R.id.genderTitleTextView)
    TextView genderTitle;

    @BindView(R.id.maleGenderButton)
    Button maleButton;

    @BindView(R.id.femaleGenderButton)
    Button femaleButton;

    @BindView(R.id.signupButton)
    Button signUpButton;

    @BindView(R.id.ageEditText)
    EditText ageEditText;
    Target target;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup3);
        ButterKnife.bind(this);

        target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                avatar.setImageBitmap(bitmap);
                imageToSend = bitmap;

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        genderTitle.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaMedium.ttf"));
        maleButton.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        femaleButton.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        ageEditText.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));

        user = (User) getIntent().getExtras().getSerializable("user");
        if (user.getImageUrl() != null) {
            Picasso.with(this)
                    .load(user.getImageUrl())
                    .error(R.mipmap.avatar)
                    .resize(80, 80)
                    .into(target);
        }

        maleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.setGender("m");
                maleClicked();
            }
        });

        femaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.setGender("f");
                femaleClicked();
            }
        });

        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, OPEN_GALLERY);//one can be replaced with any action code
            }
        });


        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!ageEditText.getText().toString().equals("") && user.getGender() != null) {
                    if (Double.valueOf(ageEditText.getText().toString()) > 125 && Double.valueOf(ageEditText.getText().toString()) <= 15) {
                        Toast.makeText(Signup3Activity.this, "Age is too high !", Toast.LENGTH_LONG).show();
                        return;
                    }
                    if (user.getImageUrl() == null) {
                        Toast.makeText(Signup3Activity.this, "Choose an avatar for you.Click on the photo to add avatar", Toast.LENGTH_LONG).show();
                        return;

                    }

                    user.setAge(Integer.valueOf(ageEditText.getText().toString()));

                    JSONObject object = new JSONObject();
                    try {
                        object.put("email", user.getEmail());
                        object.put("password", user.getPassword());
                        object.put("username", user.getUsername());
                        object.put("full_name", user.getName());
                        object.put("gender", user.getGender());
                        object.put("age", user.getAge());

                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        imageToSend.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                        byte[] byteArrayImage = baos.toByteArray();


                        String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                        encodedImage = "data:image/jpeg;base64," + encodedImage;
                        object.put("avatar", encodedImage);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    RequestBody requestBody = new RequestBody();
                    requestBody.cancelRequestsWithSameTag =true;
                    requestBody.setJsonObjectRequestBody(object);

                    SignUpMapper mapper = new SignUpMapper(Signup3Activity.this, CommonConstants.SIGN_UP_ACTION);

                    mapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener<Entity>() {
                        @Override
                        public void onSuccess(Entity result) {
                            Toast.makeText(Signup3Activity.this, "account Created successfully", Toast.LENGTH_LONG).show();
                            //save user, auth data to shared prefs
                            if (result != null) {
                                user = (User) result;
                                SharedPreferences settings = getApplicationContext().getSharedPreferences(CommonConstants.PREFS_NAME, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = settings.edit();
                                Gson gson = new Gson();
                                String json = gson.toJson(user);
                                editor.putString(CommonConstants.PREFS_USER, json);


                                //set isLoggedin boolean in shared prefs with true
                                editor.putBoolean(CommonConstants.PREFS_LOGGED_IN, true);
                                editor.commit();


                                Intent i = new Intent(Signup3Activity.this, HomeActivity.class);


                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            } else {
                                Toast.makeText(Signup3Activity.this, "Server error", Toast.LENGTH_LONG).show();
                                return;
                            }



                        }

                        @Override
                        public void onError(Exception e, Entity result1) {
                            if (result1 != null) {
                                SignUpResult result = (SignUpResult) result1;
                                Toast.makeText(Signup3Activity.this, result.Errormessages, Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(Signup3Activity.this, "Server error", Toast.LENGTH_LONG).show();
                            }


                        }
                    }, requestBody);
                } else {
                    Toast.makeText(Signup3Activity.this, getString(R.string.fillData), Toast.LENGTH_LONG).show();
                }
            }
        });


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    void maleClicked() {
        maleButton.setBackgroundResource(R.drawable.button_white);
        maleButton.setTextColor(getResources().getColor(R.color.colorPrimary));


        femaleButton.setBackgroundResource(R.drawable.button_border);
        femaleButton.setTextColor(Color.WHITE);
    }

    void femaleClicked() {
        femaleButton.setBackgroundResource(R.drawable.button_white);
        femaleButton.setTextColor(getResources().getColor(R.color.colorPrimary));


        maleButton.setBackgroundResource(R.drawable.button_border);
        maleButton.setTextColor(Color.WHITE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case OPEN_GALLERY:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    user.setImageUrl(selectedImage.toString());
                    Picasso.with(this)
                            .load(selectedImage)
                            .error(R.mipmap.avatar)
                            .resize(80, 80)
                            .into(target);
                }
                break;
        }
    }




}
