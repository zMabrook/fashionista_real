package com.zmabrook.fashionista.Views.Activites.Registeration;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Views.Activites.Abstract.FashionistaActivity;

import io.fabric.sdk.android.Fabric;

public class SignupActivity extends FashionistaActivity implements  GoogleApiClient.OnConnectionFailedListener  {
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 1001;


    CallbackManager callbackManager;
    User user;

    @BindView(R.id.signupTitleTextView)
    TextView title;

    @BindView(R.id.signupFbButton)
    Button fbButton;

    @BindView(R.id.signupTwitterButton)
    Button twitterButton;

    @BindView(R.id.signupGplusButton)
    Button gplusButton;


    @BindView(R.id.signupEmailButton)
    Button emailButton;

    @BindView(R.id.twitter_login_button)
    TwitterLoginButton twitterSDKButton;

    @BindView(R.id.google_login_button)
    SignInButton signInButton;

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        super.onActivityResult(requestCode, responseCode, data);
        callbackManager.onActivityResult(requestCode, responseCode, data);
        twitterSDKButton.onActivityResult(requestCode, responseCode, data);


        Log.d("MyApp", "heeeeee:"+String.valueOf(responseCode));
        if(responseCode != RESULT_CANCELED) {
            if (requestCode == RC_SIGN_IN) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("myApp", "success");


                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("LoginChoicesActivity", response.toString());

                                        // Application code
                                        try {
                                            user = new User();
                                            if (object.has("email")) {
                                                user.setEmail(object.getString("email"));
                                            }
                                            if (object.has("name")) {
                                                user.setName(object.getString("name"));
                                            }
                                            if (object.has("gender")) {
                                                user.setGender(object.getString("gender"));
                                            }
                                            if (object.has("picture")
                                                    &&object.getJSONObject("picture").has("data")
                                                    &&object.getJSONObject("picture")
                                                    .getJSONObject("data").has("url")) {
                                                user.setImageUrl(object.getJSONObject("picture").getJSONObject("data").getString("url"));
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        startSignUpProcess();
                                    }
                                }

                        );

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,picture");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        Log.d("myApp", "cancel");

                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d("myApp", error.toString());

                        if (error.toString().equals("net::ERR_NAME_NOT_RESOLVED")) {
                            Toast.makeText(SignupActivity.this, "Network error check your internet connection", Toast.LENGTH_LONG).show();
                        }

                    }
                }

        );


        TwitterAuthConfig authConfig = new TwitterAuthConfig(CommonConstants.TWITTER_KEY, CommonConstants.TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));

        // TODO: Create a sign-in options object
        GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(CommonConstants.GOOGLE_CLIENT_ID)
                .requestEmail()
                .requestServerAuthCode(CommonConstants.GOOGLE_CLIENT_ID)
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .build();

        // Build the GoogleApiClient object
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        setContentView(R.layout.activity_signup);

        ButterKnife.bind(this);



        title.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        fbButton.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        twitterButton.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        gplusButton.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));
        emailButton.setTypeface(Typeface.createFromAsset(getAssets(), "helveticaLight.ttf"));



        fbButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> permissionNeeds = Arrays.asList("public_profile", "email");
                LoginManager.getInstance().logInWithReadPermissions(SignupActivity.this, permissionNeeds);
            }
        });

        twitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                twitterSDKButton.performClick();
            }
        });

        twitterSDKButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
        twitterSDKButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // The TwitterSession is also available through:
                // Twitter.getInstance().core.getSessionManager().getActiveSession()


                TwitterSession session = result.data;
                user = new User();
                user.setUsername(session.getUserName());
                startSignUpProcess();
            }

            @Override
            public void failure(TwitterException exception) {
                Log.d("TwitterKit", "Login with Twitter failure", exception);
                Toast.makeText(SignupActivity.this, "Failed to login with twitter", Toast.LENGTH_LONG).show();

            }
        });




        signInButton.setScopes(gso.getScopeArray());

        gplusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInButton.performClick();

            }
        });

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                googleSignIn();

            }
        });

        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSignUpProcess();
            }
        });
    }

    private void startSignUpProcess() {
        Intent i = new Intent(SignupActivity.this, Signup2Activity.class);
        i.putExtra("user", user);
        startActivity(i);
        finish();
    }

    private void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("MyApp", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            User user = new User();
            user.setName(acct.getDisplayName());
            user.setEmail(acct.getEmail());
            user.setImageUrl(acct.getPhotoUrl().toString());
            startSignUpProcess();
        } else {
            // Signed out, show unauthenticated UI.
            Log.d("MyApp",result.getStatus().getStatusMessage());
            Toast.makeText(SignupActivity.this, "Failed to login with Google", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(SignupActivity.this, "Could not connect to Google Play Services", Toast.LENGTH_LONG).show();

    }

}
