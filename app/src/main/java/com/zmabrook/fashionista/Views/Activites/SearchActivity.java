package com.zmabrook.fashionista.Views.Activites;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Adapters.SearchPagerViewAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends AppCompatActivity {

    @BindView(R.id.pager)
    ViewPager mViewPager;


    @BindView(R.id.tabs)
    TabLayout mTabLayout;

    @BindView(R.id.backImageview)
    ImageView backImageView;


    private String[] tabs;
    private SearchPagerViewAdapter mAdapter;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        SharedPreferences settings = getApplicationContext().getSharedPreferences(CommonConstants.PREFS_NAME, Context.MODE_PRIVATE);

        Gson gson = new Gson();
        String json = settings.getString(CommonConstants.PREFS_USER, "");
        user = gson.fromJson(json, User.class);

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tabs = getResources().getStringArray(R.array.tabs);
        mAdapter = new SearchPagerViewAdapter(tabs,user, getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(3);
        mTabLayout.setupWithViewPager(mViewPager);


    }
}
