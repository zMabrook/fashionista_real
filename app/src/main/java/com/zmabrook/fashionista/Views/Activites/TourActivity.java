package com.zmabrook.fashionista.Views.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.viewpagerindicator.CirclePageIndicator;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.Abstract.FashionistaActivity;
import com.zmabrook.fashionista.Views.Activites.Registeration.RegiserationActivity;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Views.Adapters.TourPagerViewAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TourActivity extends FashionistaActivity implements ViewPager.OnPageChangeListener {

    @BindView(R.id.pager)
    ViewPager pager;

    TourPagerViewAdapter mAdapter;

    @BindView(R.id.indicator)
    CirclePageIndicator mIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        SharedPreferences settings = getApplicationContext().getSharedPreferences(CommonConstants.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        boolean tourFinished = settings.getBoolean(CommonConstants.PREFS_TOUR_FINISHED, false);
        boolean isLoggedIn = settings.getBoolean(CommonConstants.PREFS_LOGGED_IN, false);
        if (isLoggedIn) {
            Intent i = new Intent(TourActivity.this, HomeActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(i);
        } else if (tourFinished) {

            Intent i = new Intent(TourActivity.this, RegiserationActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);

        } else {
            setContentView(R.layout.activity_tour);
            ButterKnife.bind(this);


            mAdapter = new TourPagerViewAdapter(getSupportFragmentManager());
            pager.setAdapter(mAdapter);
            pager.setOffscreenPageLimit(4);
            mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
            mIndicator.setViewPager(pager);
            mIndicator.setOnPageChangeListener(this);





        }



    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (position==0){
            mIndicator.setFillColor(getResources().getColor(R.color.colorPrimary));
            mIndicator.setPageColor(getResources().getColor(R.color.tw__composer_white));
            mIndicator.setStrokeColor(getResources().getColor(R.color.colorPrimary));
        }else {
            mIndicator.setFillColor(getResources().getColor(R.color.tw__composer_white));
            mIndicator.setPageColor(getResources().getColor(R.color.colorPrimary));
            mIndicator.setStrokeColor(getResources().getColor(R.color.tw__composer_white));
        }
    }

    @Override
    public void onPageSelected(int position) {
        if (position==0){
            mIndicator.setFillColor(getResources().getColor(R.color.colorPrimary));
            mIndicator.setPageColor(getResources().getColor(R.color.tw__composer_white));
            mIndicator.setStrokeColor(getResources().getColor(R.color.colorPrimary));
        }else {
            mIndicator.setFillColor(getResources().getColor(R.color.tw__composer_white));
            mIndicator.setPageColor(getResources().getColor(R.color.colorPrimary));
            mIndicator.setStrokeColor(getResources().getColor(R.color.tw__composer_white));
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
