package com.zmabrook.fashionista.Views.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.zmabrook.fashionista.Domain.Entities.Category;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * CategoriesRecyclerViewAdapter
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Adapters
 * @since 8/8/16
 **/
public class CategoriesRecyclerViewAdapter extends RecyclerView.Adapter<CategoriesRecyclerViewAdapter.ViewHolder> implements Filterable {
    ArrayList<Category> mCategories;
    ArrayList<Category> mCategoriesBackup;
    private LayoutInflater mInflater = null;
    ViewHolder viewHolder;
    Context mContext;
    private CategoriesFilters mCategoryFilter = null;

    public CategoriesRecyclerViewAdapter(Context mContext, ArrayList<Category> mCategories) {
        this.mCategories = mCategories;
        this.mCategoriesBackup = mCategories;
        this.mContext = mContext;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = mInflater.inflate(R.layout.categories_brands_item, parent, false);
        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ViewHolder viewHolder = holder;
        viewHolder.emptyViewHolder();
        viewHolder.nameTextView.setText(mCategories.get(position).getName());
    }

    @Override
    public int getItemCount() {
        if (mCategories != null)
            return mCategories.size();

        return 0;

    }

    @Override
    public Filter getFilter() {
        if (mCategoryFilter == null) {
            mCategoryFilter = new CategoriesFilters();
        }

        return mCategoryFilter;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.categoryName)
        TextView nameTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            nameTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaMedium.ttf"));

        }

        private void emptyViewHolder() {
            nameTextView.setText("");
        }


        public void add(ArrayList<? extends Entity> result) {
            for (int i = 0; i < result.size(); i++) {
                mCategories.add(((ArrayList<Category>) result).get(i));
            }
            notifyDataSetChanged();
        }

    }


    public class CategoriesFilters extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            // Initiate our results object
            FilterResults results = new FilterResults();
            // No prefix is sent to filter by so we're going to send back the original array
            if (prefix == null || prefix.length() == 0) {
                results.values = mCategoriesBackup;
                results.count = mCategoriesBackup.size();
            } else {
                // Compare lower case strings
                String prefixString = prefix.toString().toLowerCase();
                // Local to here so we're not changing actual array
                final ArrayList<Category> categoriesBackup = (ArrayList<Category>) mCategoriesBackup;
                final int count = categoriesBackup.size();
                final ArrayList<Category> categoriesResults = new ArrayList<Category>(count);
                for (int i = 0; i < count; i++) {
                    final Category category = categoriesBackup.get(i);
                    final String itemName = category.getName().toString().toLowerCase();
                    // First match against the whole, non-splitted value
                    if (itemName.matches(".*" + prefixString + ".*")) {
                        categoriesResults.add(category);
                    }
                }
                // Set and return
                results.values = categoriesResults;
                results.count = categoriesResults.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mCategories = (ArrayList<Category>) results.values;
            notifyDataSetChanged();
        }
    }
}
