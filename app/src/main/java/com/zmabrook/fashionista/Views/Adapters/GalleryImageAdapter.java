package com.zmabrook.fashionista.Views.Adapters;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Picasso;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Custom.CheckableImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

/**
 * GalleryImageAdapter
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Adapters
 * @since 8/22/16
 **/
public class GalleryImageAdapter extends BaseAdapter {
    public ArrayList<String> images;
    Context mContext;
    public int selectedImage = -1;

    public GalleryImageAdapter(Activity context) {
        mContext=context;
        this.images = getAllShownImagesPath(context);
        Collections.reverse(images);

    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.gridview_gallery_item, parent, false);
        }

        Glide.with(mContext)
                .load(images.get(position))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into((ImageView)convertView);

        if (position == selectedImage) {
            ((CheckableImageView) convertView).setBackgroundResource(R.drawable.galleryimage_style);
        }else {
            ((CheckableImageView) convertView).setBackgroundResource(R.color.tw__composer_white);

        }



        return convertView;
    }

    private ArrayList<String> getAllShownImagesPath(Activity activity) {
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name;
        ArrayList<String> listOfAllImages = new ArrayList<String>();
        String absolutePathOfImage = null;
        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = { MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME };

        cursor = activity.getContentResolver().query(uri, projection, null,
                null, null);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);

            listOfAllImages.add(absolutePathOfImage);
        }
        return listOfAllImages;
    }
}
