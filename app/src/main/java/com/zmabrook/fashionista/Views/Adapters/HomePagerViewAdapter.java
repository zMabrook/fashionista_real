package com.zmabrook.fashionista.Views.Adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.zmabrook.fashionista.Views.Fragments.HomeFragments.HomeMapFragment;
import com.zmabrook.fashionista.Views.Fragments.HomeFragments.MyFashionFragment;
import com.zmabrook.fashionista.Views.Fragments.HomeFragments.PeopleFragment;
import com.zmabrook.fashionista.Views.Fragments.HomeFragments.TrendingListFragment;

/**
 * HomePagerViewAdapter
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Adapters
 * @since 8/3/16
 **/
public class HomePagerViewAdapter extends FragmentPagerAdapter {
    public HomePagerViewAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                MyFashionFragment myFashionFragment = new MyFashionFragment();
                return myFashionFragment;
            case 1:
                PeopleFragment peopleFragment = new PeopleFragment();
                return peopleFragment;
            case 2:
                TrendingListFragment trendingListFragment = new TrendingListFragment();
                return trendingListFragment;
            case 3:
                HomeMapFragment homeMapFragment = new HomeMapFragment();
                return homeMapFragment;

        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }


}
