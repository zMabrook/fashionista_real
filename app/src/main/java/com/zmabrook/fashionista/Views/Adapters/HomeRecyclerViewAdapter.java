package com.zmabrook.fashionista.Views.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.Look;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.UserMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.LookViewActivity;
import com.zmabrook.fashionista.Views.Activites.ProfileActivity;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * HomeRecyclerViewAdapter
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Adapters
 * @since 8/3/16
 **/
public class HomeRecyclerViewAdapter extends RecyclerView.Adapter<HomeRecyclerViewAdapter.ViewHolder> {
    ArrayList<Look> mLooks;
    private LayoutInflater mInflater = null;
    ViewHolder viewHolder;
    Context mContext;

    public HomeRecyclerViewAdapter(Context mContext, ArrayList<Look> mLooks) {
        this.mLooks = mLooks;
        this.mContext = mContext;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = mInflater.inflate(R.layout.home_item, parent, false);
        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ViewHolder viewHolder = holder;
        viewHolder.emptyViewHolder();
        final Look look = mLooks.get(position);

        if (look.getViewsCount() != null) {
            viewHolder.seenTextView.setText(look.getViewsCount());
        }
        if (look.getLikesCount() != null) {
            viewHolder.likesTextView.setText(look.getLikesCount());
        }

        if (look.getUser() != null && look.getUser().getName() != null) {
            viewHolder.nameTextView.setText(look.getUser().getName());
        }

        if (!look.getUser().isfollowed()){
            holder.addToTrendsImageView.setImageResource(R.mipmap.iconaddintrend);
        }else {
            holder.addToTrendsImageView.setImageResource(R.mipmap.iconfollowedpurple);
        }

        holder.addToTrendsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (look.getUser().isfollowed()){

                    RequestBody requestBody = new RequestBody();
                    HashMap<String, String> pathParams =new HashMap<String, String>();
                    pathParams.put("id",look.getUser().getUserID());

                    requestBody.setPathParams(pathParams);
                    UserMapper userMapper = new UserMapper(mContext, CommonConstants.UNFOLLOW_USER_ACTION);
                    userMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            if (holder!=null){
                                holder.addToTrendsImageView.setImageResource(R.mipmap.iconaddintrend);
                                mLooks.get(position).getUser().setIsfollowed(false);
                            }
                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            if (holder!=null){
                                holder.addToTrendsImageView.setImageResource(R.mipmap.iconfollowedpurple);
                                mLooks.get(position).getUser().setIsfollowed(true);

                            }
                        }
                    },requestBody);

                    holder.addToTrendsImageView.setImageResource(R.mipmap.iconaddintrend);
                    mLooks.get(position).getUser().setIsfollowed(false);

                }else {
                    //make follow request;


                    RequestBody requestBody = new RequestBody();
                    HashMap<String, String> pathParams =new HashMap<String, String>();
                    pathParams.put("id",look.getUser().getUserID());

                    requestBody.setPathParams(pathParams);
                    UserMapper userMapper = new UserMapper(mContext, CommonConstants.FOLLOW_USER_ACTION);
                    userMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            if (holder!=null){
                                holder.addToTrendsImageView.setImageResource(R.mipmap.iconfollowedpurple);
                                mLooks.get(position).getUser().setIsfollowed(true);

                            }
                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            if (holder!=null){
                                holder.addToTrendsImageView.setImageResource(R.mipmap.iconaddintrend);
                                mLooks.get(position).getUser().setIsfollowed(false);
                            }
                        }
                    },requestBody);

                    holder.addToTrendsImageView.setImageResource(R.mipmap.iconfollowedpurple);
                    mLooks.get(position).getUser().setIsfollowed(true);
                }
            }
        });

        viewHolder.locationTextView.setText(look.getLocation().getCity());

        if (look.getImageUrl() != null) {
            Picasso.with(mContext)
                    .load(look.getImageUrl())
                    .into(viewHolder.lookImage, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(mContext)
                                    .load(look.getImageUrl())
                                    .into(viewHolder.lookImage);
                        }
                    });

        }

        if (look.getUser().getImageUrl() != null) {

            Picasso.with(mContext)
                    .load(look.getUser().getImageUrl())
                    .into(viewHolder.avatarImageview, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(mContext)
                                    .load(look.getUser().getImageUrl())
                                    .into(viewHolder.avatarImageview);
                        }
                    });



        }

        viewHolder.avatarImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProfileActivity.class);
                intent.putExtra(CommonConstants.PREFS_USER, look.getUser());

                //intent.putExtra(CommonConstants.PREFS_USER,peopleList.get(getLayoutPosition()));
                mContext.startActivity(intent);
            }
        });

        viewHolder.nameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.avatarImageview.performClick();
            }
        });


        viewHolder.homeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 Intent i = new Intent(mContext, LookViewActivity.class);

                LookViewActivity.look= mLooks.get(position);
                mContext.startActivity(i);


            }
        });

    }

    @Override
    public int getItemCount() {

        if (mLooks != null)
            return mLooks.size();

        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.likesTextView)
        TextView likesTextView;

        @BindView(R.id.homeItem)
        CardView homeItem;

        @BindView(R.id.seenTextView)
        TextView seenTextView;

        @BindView(R.id.HomeItemImage)
        ImageView lookImage;

        @BindView(R.id.addImageView)
        ImageView addToTrendsImageView;

        @BindView(R.id.avatarImageView)
        CircleImageView avatarImageview;

        @BindView(R.id.nameTextView)
        TextView nameTextView;

        @BindView(R.id.LocationTextView)
        TextView locationTextView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            nameTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            locationTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            seenTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            likesTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));


        }

        private void emptyViewHolder() {
            likesTextView.setText("");
            seenTextView.setText("");
            lookImage.setImageBitmap(null);
            addToTrendsImageView.setVisibility(View.VISIBLE);
            avatarImageview.setImageResource(R.mipmap.avatar);
            nameTextView.setText("");
            locationTextView.setText("");


        }


    }


    public void add(ArrayList<? extends Entity> result) {
        for (int i = 0; i < result.size(); i++) {
            mLooks.add(((ArrayList<Look>) result).get(i));
        }
        notifyDataSetChanged();
    }
}
