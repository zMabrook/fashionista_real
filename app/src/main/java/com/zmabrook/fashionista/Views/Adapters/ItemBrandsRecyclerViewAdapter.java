package com.zmabrook.fashionista.Views.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zmabrook.fashionista.Domain.Entities.Brand;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.NewLook.ChooseBrandActivity;
import com.zmabrook.fashionista.Views.Activites.NewLook.TagItemActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * ItemBrandsRecyclerViewAdapter
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Adapters
 * @since 8/9/16
 **/
public class ItemBrandsRecyclerViewAdapter extends RecyclerView.Adapter<ItemBrandsRecyclerViewAdapter.ViewHolder>  {
    ArrayList<Brand> mBrands;
    private LayoutInflater mInflater = null;
    ViewHolder viewHolder;
    Context mContext;

    public ItemBrandsRecyclerViewAdapter(Context mContext, ArrayList<Brand> mBrands) {
        this.mBrands = mBrands;
        this.mContext = mContext;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = mInflater.inflate(R.layout.item_brand_cell, parent, false);
        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ViewHolder viewHolder = holder;
        viewHolder.emptyViewHolder();
        viewHolder.nameTextView.setText(mBrands.get(position).getName());
        Picasso.with(mContext).load(mBrands.get(position).getIconUrl()).into(viewHolder.iconImageView, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {
                Picasso.with(mContext).load(mBrands.get(position).getIconUrl()).into(viewHolder.iconImageView);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mBrands != null)
            return mBrands.size();

        return 0;
    }




    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.nameTextView)
        TextView nameTextView;

        @BindView(R.id.iconImageView)
        ImageView iconImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            nameTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TagItemActivity.item.setWholeBrand(mBrands.get(getLayoutPosition()));
                    ((ChooseBrandActivity)mContext).showChoosenBrand();
                }
            });
        }

        private void emptyViewHolder() {
            nameTextView.setText("");
            iconImageView.setImageBitmap(null);
        }


    }




    public void add(ArrayList<? extends Entity> result) {
        for (int i = 0; i < result.size(); i++) {
            mBrands.add(((ArrayList<Brand>) result).get(i));
        }
        notifyDataSetChanged();
    }
}
