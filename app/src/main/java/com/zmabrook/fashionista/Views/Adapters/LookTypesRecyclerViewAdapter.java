package com.zmabrook.fashionista.Views.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.zmabrook.fashionista.Domain.Entities.LookType;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.NewLook.LookStatusActivity;
import com.zmabrook.fashionista.Views.Activites.NewLook.TagItemActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * LookTypesRecyclerViewAdapter
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Adapters
 * @since 8/31/16
 **/
public class LookTypesRecyclerViewAdapter extends RecyclerView.Adapter<LookTypesRecyclerViewAdapter.ViewHolder> {
    ArrayList<LookType> mLooks;
    private LayoutInflater mInflater = null;
    ViewHolder viewHolder;
    Context mContext;


    public LookTypesRecyclerViewAdapter(Context mContext, ArrayList<LookType> mLooks) {
        this.mLooks = mLooks;
        this.mContext = mContext;
        mInflater = LayoutInflater.from(mContext);

    }


    @Override
    public LookTypesRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = mInflater.inflate(R.layout.looktype_item, parent, false);
        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(LookTypesRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.emptyViewHolder();
        holder.typeNameTextView.setText(mLooks.get(position).getName());

    }

    @Override
    public int getItemCount() {
        if (mLooks != null)
            return mLooks.size();

        return 0;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.typeNameTextView)
        Button typeNameTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            typeNameTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TagItemActivity.newLook.setLookType(mLooks.get(getLayoutPosition()));
                    Intent i = new Intent(mContext, LookStatusActivity.class);
                    mContext.startActivity(i);
                }
            });


        }

        private void emptyViewHolder() {
            typeNameTextView.setText("");

        }

    }
}
