package com.zmabrook.fashionista.Views.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.Look;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.LooksMapper;
import com.zmabrook.fashionista.Domain.Mapper.UserMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.LookViewActivity;
import com.zmabrook.fashionista.Views.Activites.ProfileActivity;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by matrix on 12/08/2016.
 */
public class MyFashionRecyclerViewAdapter extends RecyclerView.Adapter<MyFashionRecyclerViewAdapter.ViewHolder> {

    ArrayList<Look> mLooks;
    private LayoutInflater mInflater = null;
    ViewHolder viewHolder;
    Context mContext;


    public MyFashionRecyclerViewAdapter(Context mContext,ArrayList<Look> mLooks) {
        this.mLooks = mLooks;
        this.mContext = mContext;
        mInflater = LayoutInflater.from(mContext);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = mInflater.inflate(R.layout.myfashion_item, parent, false);
        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ViewHolder viewHolder = holder;
        viewHolder.emptyViewHolder();
        final Look look = mLooks.get(position);

        if (look.getViewsCount() != null) {
            viewHolder.numOfViewsTextView.setText(look.getViewsCount());
        }
        if (look.getLikesCount() != null) {
            viewHolder.numOfLikesTextView.setText(look.getLikesCount());
        }

        if (!look.getUser().isfollowed()){
            viewHolder.followImageView.setImageResource(R.mipmap.iconequestpurple);
        }else {
            viewHolder.followImageView.setImageResource(R.mipmap.iconfollowedpurple);
        }

        if (look.isLiked()){
            viewHolder.likeImageView.setImageResource(R.mipmap.iconlikeposteron);
        }else {
            viewHolder.likeImageView.setImageResource(R.mipmap.iconlikeposteroff);

        }

        if (look.isPinned()){
            viewHolder.pinImageView.setImageResource(R.mipmap.iconpinposton);

        }else {
            viewHolder.pinImageView.setImageResource(R.mipmap.iconpinpostoff);

        }

        if (look.getCommentsCount() != null) {
            viewHolder.numOfCommentsTextView.setText(look.getCommentsCount());
        }

        if (look.getTitle() != null) {
            viewHolder.captionTextView.setText(look.getTitle());
        }

        if (look.getUser() != null && look.getUser().getName() != null) {
            viewHolder.nameTextView.setText(look.getUser().getName());

        }

        if (look.getUser() != null && look.getUser().getUsername() != null) {
            viewHolder.usernameTextView.setText("@"+look.getUser().getUsername());

        }

        if (look.getUser() != null && look.getUser().getFollowersCount() != null) {
            viewHolder.numOfFollowersTextView.setText(look.getUser().getFollowersCount());

        }

        if (look.getUser() != null && look.getCreatedAt() != null) {
            viewHolder.timeTextView.setText("Just Now");

        }

        viewHolder.nameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.avatar.performClick();
            }
        });

        viewHolder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProfileActivity.class);
                intent.putExtra(CommonConstants.PREFS_USER, look.getUser());

                //intent.putExtra(CommonConstants.PREFS_USER,peopleList.get(getLayoutPosition()));
                mContext.startActivity(intent);
            }
        });




        if (look.getImageUrl() != null) {
            Picasso.with(mContext)
                    .load(look.getImageUrl())
                    .into(viewHolder.lookImageView, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(mContext)
                                    .load(look.getImageUrl())
                                    .into(viewHolder.lookImageView);
                        }
                    });

        }

        if (look.getUser().getImageUrl() != null) {

            Picasso.with(mContext)
                    .load(look.getUser().getImageUrl())
                    .into(viewHolder.avatar, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(mContext)
                                    .load(look.getUser().getImageUrl())
                                    .into(viewHolder.avatar);
                        }
                    });

            Picasso.with(mContext)
                    .load(look.getUser().getImageUrl())
                    .into(viewHolder.smallAvatar, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(mContext)
                                    .load(look.getUser().getImageUrl())
                                    .into(viewHolder.smallAvatar);
                        }
                    });




        }

        holder.followImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (look.getUser().isfollowed()){

                    RequestBody requestBody = new RequestBody();
                    HashMap<String, String> pathParams =new HashMap<String, String>();
                    pathParams.put("id",look.getUser().getUserID());

                    requestBody.setPathParams(pathParams);
                    UserMapper userMapper = new UserMapper(mContext, CommonConstants.UNFOLLOW_USER_ACTION);
                    userMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            if (holder!=null){
                                holder.followImageView.setImageResource(R.mipmap.iconequestpurple);
                                mLooks.get(position).getUser().setIsfollowed(false);
                            }
                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            if (holder!=null){
                                holder.followImageView.setImageResource(R.mipmap.iconfollowedpurple);
                                mLooks.get(position).getUser().setIsfollowed(true);

                            }
                        }
                    },requestBody);

                    holder.followImageView.setImageResource(R.mipmap.iconequestpurple);
                    mLooks.get(position).getUser().setIsfollowed(false);

                }else {
                    //make follow request;


                    RequestBody requestBody = new RequestBody();
                    HashMap<String, String> pathParams =new HashMap<String, String>();
                    pathParams.put("id",look.getUser().getUserID());

                    requestBody.setPathParams(pathParams);
                    UserMapper userMapper = new UserMapper(mContext, CommonConstants.FOLLOW_USER_ACTION);
                    userMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            if (holder!=null){
                                holder.followImageView.setImageResource(R.mipmap.iconfollowedpurple);
                                mLooks.get(position).getUser().setIsfollowed(true);

                            }
                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            if (holder!=null){
                                holder.followImageView.setImageResource(R.mipmap.iconequestpurple);
                                mLooks.get(position).getUser().setIsfollowed(false);
                            }
                        }
                    },requestBody);

                    holder.followImageView.setImageResource(R.mipmap.iconfollowedpurple);
                    mLooks.get(position).getUser().setIsfollowed(true);
                }
            }
        });

        viewHolder.pinImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (look.isPinned()){
                    // unpin request

                    RequestBody requestBody = new RequestBody();
                    HashMap<String,String> pathParams = new HashMap<String, String>();
                    pathParams.put("id",look.getId());
                    requestBody.setPathParams(pathParams);

                    LooksMapper looksMapper = new LooksMapper(mContext,CommonConstants.UNPIN_LOOK_ACTION);
                    looksMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.DELETE, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            viewHolder.pinImageView.setImageResource(R.mipmap.iconpinpostoff);
                            mLooks.get(position).setIsPinned(false);
                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            viewHolder.pinImageView.setImageResource(R.mipmap.iconpinposton);
                            mLooks.get(position).setIsPinned(true);
                        }
                    }, requestBody);

                    viewHolder.pinImageView.setImageResource(R.mipmap.iconpinpostoff);
                }else {
                    // pin request

                    RequestBody requestBody = new RequestBody();
                    HashMap<String,String> pathParams = new HashMap<String, String>();
                    pathParams.put("id",look.getId());
                    requestBody.setPathParams(pathParams);

                    LooksMapper looksMapper = new LooksMapper(mContext,CommonConstants.PIN_LOOK_ACTION);
                    looksMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            viewHolder.pinImageView.setImageResource(R.mipmap.iconpinposton);
                            mLooks.get(position).setIsPinned(true);
                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            viewHolder.pinImageView.setImageResource(R.mipmap.iconpinpostoff);
                            mLooks.get(position).setIsPinned(false);
                        }
                    }, requestBody);

                    viewHolder.pinImageView.setImageResource(R.mipmap.iconpinposton);
                }
            }
        });
        viewHolder.likeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (look.isLiked()){
                    //unlike request
                    RequestBody requestBody = new RequestBody();
                    HashMap<String,String> pathParams = new HashMap<String, String>();
                    pathParams.put("id",look.getId());
                    requestBody.setPathParams(pathParams);

                    LooksMapper looksMapper = new LooksMapper(mContext,CommonConstants.UNLIKE_LOOK_ACTION);
                    looksMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.DELETE, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            mLooks.get(position).setIsLiked(false);
                            viewHolder.likeImageView.setImageResource(R.mipmap.iconlikeposteroff);

                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            mLooks.get(position).setIsLiked(true);
                            viewHolder.likeImageView.setImageResource(R.mipmap.iconlikeposteron);
                        }
                    },requestBody);
                    viewHolder.likeImageView.setImageResource(R.mipmap.iconlikeposteroff);

                }else {
                    //like request
                    RequestBody requestBody = new RequestBody();
                    HashMap<String,String> pathParams = new HashMap<String, String>();
                    pathParams.put("id",look.getId());
                    requestBody.setPathParams(pathParams);

                    LooksMapper looksMapper = new LooksMapper(mContext,CommonConstants.LIKE_LOOK_ACTION);
                    looksMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            mLooks.get(position).setIsLiked(true);
                            viewHolder.likeImageView.setImageResource(R.mipmap.iconlikeposteron);

                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            mLooks.get(position).setIsLiked(false);
                            viewHolder.likeImageView.setImageResource(R.mipmap.iconlikeposteroff);
                        }
                    },requestBody);

                    viewHolder.likeImageView.setImageResource(R.mipmap.iconlikeposteron);

                }

            }
        });

        viewHolder.myFashionItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, LookViewActivity.class);

                LookViewActivity.look= mLooks.get(position);
                mContext.startActivity(i);


            }
        });
    }



    @Override
    public int getItemCount() {

        if (mLooks != null)
            return mLooks.size();

        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.lookImageView)
        ImageView lookImageView;

        @BindView(R.id.myFashionItem)
        CardView myFashionItem;

        @BindView(R.id.followImageView)
        ImageView followImageView;

        @BindView(R.id.shareImageView)
        ImageView shareImageView;

        @BindView(R.id.likeImageView)
        ImageView likeImageView;

        @BindView(R.id.pinImageView)
        ImageView pinImageView;

        @BindView(R.id.avatarImageView)
        CircleImageView avatar;

        @BindView(R.id.smallAvatar)
        CircleImageView smallAvatar;

        @BindView(R.id.nameTextView)
        TextView nameTextView;

        @BindView(R.id.usernameTextView)
        TextView usernameTextView;

        @BindView(R.id.timeTextView)
        TextView timeTextView;

        @BindView(R.id.captionTextView)
        TextView captionTextView;

        @BindView(R.id.numOfFollowersTextView)
        TextView numOfFollowersTextView;

        @BindView(R.id.numOfLikesTextView)
        TextView numOfLikesTextView;

        @BindView(R.id.numOfCommentsTextView)
        TextView numOfCommentsTextView;

        @BindView(R.id.numOfViewsTextView)
        TextView numOfViewsTextView;



        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            nameTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaMedium.ttf"));
            usernameTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            timeTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            numOfCommentsTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            numOfFollowersTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            numOfLikesTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            numOfViewsTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            captionTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));



        }


        private void emptyViewHolder(){
            nameTextView.setText("");
            usernameTextView.setText("");
            timeTextView.setText("");
            numOfViewsTextView.setText("");
            numOfLikesTextView.setText("");
            numOfFollowersTextView.setText("");
            numOfCommentsTextView.setText("");
            captionTextView.setText("");

            avatar.setImageResource(R.mipmap.avatar);
            smallAvatar.setImageResource(R.mipmap.avatar);


        }
    }

    public void add(ArrayList<? extends Entity> result) {
        if (result==null)
            return;

        for (int i = 0; i < result.size() ; i++) {
            mLooks.add(((ArrayList<Look>) result).get(i));
        }
        notifyDataSetChanged();
    }
}
