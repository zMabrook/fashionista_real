package com.zmabrook.fashionista.Views.Adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Category;
import com.zmabrook.fashionista.Domain.Entities.CategorySearchResult;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.SearchMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.NewLook.ChooseBrandActivity;
import com.zmabrook.fashionista.Views.Activites.NewLook.TagItemActivity;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * NewLookCategoriesRecyclerViewAdapter
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Adapters
 * @since 8/28/16
 **/
public class NewLookCategoriesRecyclerViewAdapter extends RecyclerView.Adapter<NewLookCategoriesRecyclerViewAdapter.ViewHolder> {
    ArrayList<Category> mCategories;
    private LayoutInflater mInflater = null;
    ViewHolder viewHolder;
    Context mContext;



    public NewLookCategoriesRecyclerViewAdapter(Context mContext, ArrayList<Category> mCategories) {
        this.mCategories = mCategories;
        this.mContext = mContext;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = mInflater.inflate(R.layout.tag_item_cell, parent, false);
        viewHolder = new ViewHolder(view);

        return viewHolder;    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ViewHolder viewHolder = holder;
        viewHolder.emptyViewHolder();
        viewHolder.categoryNameTextView.setText(mCategories.get(position).getName());
        Picasso.with(mContext).load(mCategories.get(position).getIconUrl()).into(viewHolder.categoryIcon, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {
                Picasso.with(mContext).load(mCategories.get(position).getIconUrl()).into(viewHolder.categoryIcon);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mCategories != null)
            return mCategories.size();

        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        boolean follow = true;
        @BindView(R.id.categoryIcon)
        ImageView categoryIcon;

        @BindView(R.id.categoryNameTextView)
        TextView categoryNameTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            categoryNameTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaMedium.ttf"));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final TagItemActivity activity = (TagItemActivity)mContext;

                    if (mCategories.get(getLayoutPosition()).isSub()){
                        activity.item.setCategory(mCategories.get(getLayoutPosition()));
                        Toast.makeText(mContext,mCategories.get(getLayoutPosition()).getName(),Toast.LENGTH_LONG).show();
                        Intent i = new Intent(mContext, ChooseBrandActivity.class);
                        mContext.startActivity(i);
                        return;
                    }
                    RequestBody requestBody = new RequestBody();
                    HashMap<String, String> pathParams = new HashMap<String, String>();
                    pathParams.put("id",mCategories.get(getLayoutPosition()).getId());
                    requestBody.setPathParams(pathParams);

                    SearchMapper mapper = new SearchMapper(mContext, CommonConstants.SUB_CATEGORIES_SEARCH_ACTION);
                    mapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            ArrayList<Category> xcategories= ((CategorySearchResult)result).getCategories();
                            if (xcategories.size()==0){
                                activity.item.setCategory(mCategories.get(getLayoutPosition()));
                                Toast.makeText(mContext,mCategories.get(getLayoutPosition()).getName(),Toast.LENGTH_LONG).show();
                                Intent i = new Intent(mContext, ChooseBrandActivity.class);
                                mContext.startActivity(i);
                                return;
                            }

                            mCategories = xcategories;
                            NewLookCategoriesRecyclerViewAdapter.this.notifyDataSetChanged();


                        }

                        @Override
                        public void onError(Exception e, Object result) {

                        }
                    }, requestBody);









                }
            });


        }

        private void emptyViewHolder() {
            categoryNameTextView.setText("");
            categoryIcon.setImageBitmap(null);
        }


    }
}
