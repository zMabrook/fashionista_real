package com.zmabrook.fashionista.Views.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.zmabrook.fashionista.Views.Fragments.HomeFragments.HomeMapFragment;
import com.zmabrook.fashionista.Views.Fragments.HomeFragments.MyFashionFragment;
import com.zmabrook.fashionista.Views.Fragments.HomeFragments.PeopleFragment;
import com.zmabrook.fashionista.Views.Fragments.HomeFragments.TrendingListFragment;
import com.zmabrook.fashionista.Views.Fragments.NewLook.GalleryFragment;
import com.zmabrook.fashionista.Views.Fragments.NewLook.PhotoFragment;
import com.zmabrook.fashionista.Views.Fragments.NewLook.VideoFragment;

/**
 * NewLookPagerViewAdapter
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Adapters
 * @since 8/18/16
 **/
public class NewLookPagerViewAdapter extends FragmentPagerAdapter {


    private Fragment mCurrentFragment;

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getCurrentFragment() != object) {
            mCurrentFragment = ((Fragment) object);
        }
        super.setPrimaryItem(container, position, object);
    }

    public NewLookPagerViewAdapter(FragmentManager fm) {
        super(fm);
    }



    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                GalleryFragment myFashionFragment = new GalleryFragment();
                return myFashionFragment;
            case 1:
                PhotoFragment peopleFragment = new PhotoFragment();
                return peopleFragment;
            case 2:
                VideoFragment trendingListFragment = new VideoFragment();
                return trendingListFragment;

        }
        return null;    }

    @Override
    public int getCount() {
        return 3;
    }
}
