package com.zmabrook.fashionista.Views.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.UserMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.ProfileActivity;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * PeopleRecyclerViewAdapter
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Adapters
 * @since 8/6/16
 **/
public class PeopleRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private LayoutInflater mInflater = null;
    RecyclerView.ViewHolder viewHolder;
    Context mContext;
    ArrayList<User> peopleList;
    final int headerType = 1;
    final int normalType = 0;

    public PeopleRecyclerViewAdapter(Context mContext, ArrayList<User> mpeople) {
        this.peopleList = mpeople;
        this.mContext = mContext;
        mInflater = LayoutInflater.from(mContext);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case normalType:
                view = mInflater.inflate(R.layout.people_item, parent, false);
                viewHolder = new ViewHolder(view);

                break;
            case headerType:
                view = mInflater.inflate(R.layout.section_item, parent, false);
                viewHolder = new HeaderViewHolder(view);

                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {


        switch (getItemViewType(position)) {
            case normalType:
                final ViewHolder viewHolder = (ViewHolder) holder;
                viewHolder.emptyViewHolder();

                final User user = peopleList.get(position);


                if (user.getName() != null) {
                    viewHolder.name.setText(user.getName());

                }

                if (user.getBio() != null) {
                    viewHolder.bio.setText(user.getBio());

                }else {
                    viewHolder.bio.setVisibility(View.GONE);
                }

                if (user.getImageUrl() != null) {

                    Picasso.with(mContext)
                            .load(user.getImageUrl())
                            .into(viewHolder.avatar, new Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {
                                    Picasso.with(mContext)
                                            .load(user.getImageUrl())
                                            .into(viewHolder.avatar);
                                }
                            });
                }

                if (!user.isfollowed()){
                    ((ViewHolder) holder).followButton.setImageResource(R.mipmap.iconequestpurple);
                }else {
                    ((ViewHolder) holder).followButton.setImageResource(R.mipmap.iconfollowedpurple);
                }

                viewHolder.followButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (user.isfollowed()){
                            //make unfollow request;

                            RequestBody requestBody = new RequestBody();
                            HashMap<String, String> pathParams =new HashMap<String, String>();
                            pathParams.put("id",user.getUserID());

                            requestBody.setPathParams(pathParams);
                            UserMapper userMapper = new UserMapper(mContext, CommonConstants.UNFOLLOW_USER_ACTION);
                            userMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                                @Override
                                public void onSuccess(Object result) {
                                    if (holder!=null){
                                        ((ViewHolder) holder).followButton.setImageResource(R.mipmap.iconequestpurple);
                                        peopleList.get(position).setIsfollowed(false);
                                    }
                                }

                                @Override
                                public void onError(Exception e, Object result) {
                                    if (holder!=null){
                                        ((ViewHolder) holder).followButton.setImageResource(R.mipmap.iconfollowedpurple);
                                        peopleList.get(position).setIsfollowed(true);

                                    }
                                }
                            },requestBody);

                            ((ViewHolder) holder).followButton.setImageResource(R.mipmap.iconequestpurple);
                            peopleList.get(position).setIsfollowed(false);

                        }else {
                            //make follow request;


                            RequestBody requestBody = new RequestBody();
                            HashMap<String, String> pathParams =new HashMap<String, String>();
                            pathParams.put("id", user.getUserID());

                            requestBody.setPathParams(pathParams);
                            UserMapper userMapper = new UserMapper(mContext, CommonConstants.FOLLOW_USER_ACTION);
                            userMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                                @Override
                                public void onSuccess(Object result) {
                                    if (holder!=null){
                                        ((ViewHolder) holder).followButton.setImageResource(R.mipmap.iconfollowedpurple);
                                        peopleList.get(position).setIsfollowed(true);

                                    }
                                }

                                @Override
                                public void onError(Exception e, Object result) {
                                    if (holder!=null){
                                        ((ViewHolder) holder).followButton.setImageResource(R.mipmap.iconequestpurple);
                                        peopleList.get(position).setIsfollowed(false);
                                    }
                                }
                            },requestBody);

                            ((ViewHolder) holder).followButton.setImageResource(R.mipmap.iconfollowedpurple);
                            peopleList.get(position).setIsfollowed(false);
                        }
                    }
                });

                return;

            case headerType:
                final HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                headerViewHolder.emptyViewHolder();
                headerViewHolder.headerTextView.setText(peopleList.get(position).getName());
                return;
        }


    }

    @Override
    public int getItemViewType(int position) {
        if (peopleList.get(position).isHeader()) {

            return headerType;
        } else {
            return normalType;
        }
    }

    @Override
    public int getItemCount() {
        if (peopleList != null)
            return peopleList.size();

        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.avatarImageView)
        CircleImageView avatar;

        @BindView(R.id.nameTextView)
        TextView name;

        @BindView(R.id.bioTextView)
        TextView bio;

        @BindView(R.id.followImageView)
        ImageView followButton;
        boolean follow = true;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ProfileActivity.class);
                    intent.putExtra(CommonConstants.PREFS_USER, peopleList.get(getLayoutPosition()));

                    //intent.putExtra(CommonConstants.PREFS_USER,peopleList.get(getLayoutPosition()));
                    mContext.startActivity(intent);
                }
            });


        }

        private void emptyViewHolder() {
            name.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaMedium.ttf"));
            bio.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));

            avatar.setImageResource(R.mipmap.avatar);
            name.setText("");
            bio.setText("");
            followButton.setImageResource(R.mipmap.iconequestpurple);

            bio.setVisibility(View.VISIBLE);

        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.headerTextView)
        TextView headerTextView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }

        private void emptyViewHolder() {
            headerTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            headerTextView.setText("");

        }

    }

    public void add(ArrayList<? extends Entity> result) {
        for (int i = 0; i < result.size(); i++) {
            peopleList.add(((ArrayList<User>) result).get(i));
        }
        notifyDataSetChanged();
    }
}
