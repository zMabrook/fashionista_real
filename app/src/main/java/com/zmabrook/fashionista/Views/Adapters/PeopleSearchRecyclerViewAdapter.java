package com.zmabrook.fashionista.Views.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.UserMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.ProfileActivity;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * PeopleSearchRecyclerViewAdapter
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Adapters
 * @since 8/10/16
 **/
public class PeopleSearchRecyclerViewAdapter extends RecyclerView.Adapter<PeopleSearchRecyclerViewAdapter.ViewHolder> implements Filterable {
    ArrayList<User> peopleList;
    ArrayList<User> peopleListBackup;
    private LayoutInflater mInflater = null;
    ViewHolder viewHolder;
    Context mContext;
    private CategoriesFilters mCategoryFilter = null;

    public PeopleSearchRecyclerViewAdapter(Context mContext, ArrayList<User> mCategories) {
        this.peopleList = mCategories;
        this.peopleListBackup = mCategories;
        this.mContext = mContext;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public PeopleSearchRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = mInflater.inflate(R.layout.search_people_item, parent, false);
        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PeopleSearchRecyclerViewAdapter.ViewHolder holder, int position) {
        final ViewHolder viewHolder = holder;
        viewHolder.emptyViewHolder();

        final User user = peopleList.get(position);


        if (user.getName() != null) {
            viewHolder.name.setText(user.getName());

        }


        if (!user.isfollowed()){
            viewHolder.followButton.setImageResource(R.mipmap.iconequestpurple);
        }else {
            viewHolder.followButton.setImageResource(R.mipmap.iconfollowedpurple);
        }

        if (user.getImageUrl() != null) {

            Picasso.with(mContext)
                    .load(user.getImageUrl())
                    .into(viewHolder.avatar, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(mContext)
                                    .load(user.getImageUrl())
                                    .into(viewHolder.avatar);
                        }
                    });
        }

        viewHolder.followButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user.isfollowed()){
                    //make unfollow request;

                    RequestBody requestBody = new RequestBody();
                    HashMap<String, String> pathParams =new HashMap<String, String>();
                    pathParams.put("id",user.getUserID());

                    requestBody.setPathParams(pathParams);
                    UserMapper userMapper = new UserMapper(mContext, CommonConstants.UNFOLLOW_USER_ACTION);
                    userMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            if (viewHolder.followButton!=null){
                                viewHolder.followButton.setImageResource(R.mipmap.iconequestpurple);
                                user.setIsfollowed(false);
                            }
                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            if (viewHolder.followButton!=null){
                                viewHolder.followButton.setImageResource(R.mipmap.iconfollowedpurple);
                                user.setIsfollowed(true);

                            }
                        }
                    },requestBody);

                    viewHolder.followButton.setImageResource(R.mipmap.iconequestpurple);
                    user.setIsfollowed(false);

                }else {
                    //make follow request;


                    RequestBody requestBody = new RequestBody();
                    HashMap<String, String> pathParams =new HashMap<String, String>();
                    pathParams.put("id", user.getUserID());

                    requestBody.setPathParams(pathParams);
                    UserMapper userMapper = new UserMapper(mContext, CommonConstants.FOLLOW_USER_ACTION);
                    userMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.POST, new FashionistaListener() {
                        @Override
                        public void onSuccess(Object result) {
                            if (viewHolder.followButton!=null){
                                viewHolder.followButton.setImageResource(R.mipmap.iconfollowedpurple);
                                user.setIsfollowed(true);

                            }
                        }

                        @Override
                        public void onError(Exception e, Object result) {
                            if (viewHolder.followButton!=null){
                                viewHolder.followButton.setImageResource(R.mipmap.iconequestpurple);
                                user.setIsfollowed(false);
                            }
                        }
                    },requestBody);

                    viewHolder.followButton.setImageResource(R.mipmap.iconfollowedpurple);
                    user.setIsfollowed(true);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        if (peopleList != null)
            return peopleList.size();

        return 0;
    }

    @Override
    public Filter getFilter() {
        if (mCategoryFilter == null) {
            mCategoryFilter = new CategoriesFilters();
        }

        return mCategoryFilter;
    }

    public class CategoriesFilters extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            // Initiate our results object
            FilterResults results = new FilterResults();
            // No prefix is sent to filter by so we're going to send back the original array
            if (prefix == null || prefix.length() == 0) {
                results.values = peopleListBackup;
                results.count = peopleListBackup.size();
            } else {
                // Compare lower case strings
                String prefixString = prefix.toString().toLowerCase();
                // Local to here so we're not changing actual array
                final ArrayList<User> categoriesBackup = (ArrayList<User>) peopleListBackup;
                final int count = categoriesBackup.size();
                final ArrayList<User> categoriesResults = new ArrayList<User>(count);
                for (int i = 0; i < count; i++) {
                    final User category = categoriesBackup.get(i);
                    final String itemName = category.getName().toString().toLowerCase();
                    // First match against the whole, non-splitted value
                    if (itemName.matches(".*" + prefixString + ".*")) {
                        categoriesResults.add(category);
                    }
                }
                // Set and return
                results.values = categoriesResults;
                results.count = categoriesResults.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            peopleList = (ArrayList<User>) results.values;
            notifyDataSetChanged();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.avatarImageView)
        CircleImageView avatar;

        @BindView(R.id.nameTextView)
        TextView name;

        @BindView(R.id.followImageView)
        ImageView followButton;
        boolean follow = true;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ProfileActivity.class);
                    intent.putExtra(CommonConstants.PREFS_USER, peopleList.get(getLayoutPosition()));

                    //intent.putExtra(CommonConstants.PREFS_USER,peopleList.get(getLayoutPosition()));
                    mContext.startActivity(intent);
                }
            });


        }

        private void emptyViewHolder() {
            name.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            avatar.setImageResource(R.mipmap.avatar);
            name.setText("");
            followButton.setImageResource(R.mipmap.iconequestpurple);

        }


    }


    public void add(ArrayList<? extends Entity> result) {
        for (int i = 0; i < result.size(); i++) {
            peopleList.add(((ArrayList<User>) result).get(i));
        }
        notifyDataSetChanged();
    }
}
