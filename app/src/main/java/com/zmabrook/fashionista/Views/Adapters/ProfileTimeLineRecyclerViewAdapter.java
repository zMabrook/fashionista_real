package com.zmabrook.fashionista.Views.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.Look;
import com.zmabrook.fashionista.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * ProfileTimeLineRecyclerViewAdapter
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Adapters
 * @since 8/15/16
 **/

public class ProfileTimeLineRecyclerViewAdapter extends RecyclerView.Adapter<ProfileTimeLineRecyclerViewAdapter.ViewHolder> {

    ArrayList<Look> mLooks;
    private LayoutInflater mInflater = null;
    ViewHolder viewHolder;
    Context mContext;


    public ProfileTimeLineRecyclerViewAdapter(Context mContext,ArrayList<Look> mLooks) {
        this.mLooks = mLooks;
        this.mContext = mContext;
        mInflater = LayoutInflater.from(mContext);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = mInflater.inflate(R.layout.myfashion_item, parent, false);
        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ViewHolder viewHolder = holder;
        viewHolder.emptyViewHolder();
        final Look look = mLooks.get(position);

        if (look.getViewsCount() != null) {
            viewHolder.numOfViewsTextView.setText(look.getViewsCount());
        }
        if (look.getLikesCount() != null) {
            viewHolder.numOfLikesTextView.setText(look.getLikesCount());
        }

        if (look.getCommentsCount() != null) {
            viewHolder.numOfCommentsTextView.setText(look.getCommentsCount());
        }

        if (look.getTitle() != null) {
            viewHolder.captionTextView.setText(look.getTitle());
        }

        if (look.getUser() != null && look.getUser().getName() != null) {
            viewHolder.nameTextView.setText(look.getUser().getName());

        }

        if (look.getUser() != null && look.getUser().getUsername() != null) {
            viewHolder.usernameTextView.setText("@"+look.getUser().getUsername());

        }

        if (look.getUser() != null && look.getUser().getFollowersCount() != null) {
            viewHolder.numOfFollowersTextView.setText(look.getUser().getFollowersCount());

        }

        if (look.getUser() != null && look.getCreatedAt() != null) {
            viewHolder.timeTextView.setText("Just Now");

        }




        if (look.getImageUrl() != null) {
            Picasso.with(mContext)
                    .load(look.getImageUrl())
                    .into(viewHolder.lookImageView, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(mContext)
                                    .load(look.getImageUrl())
                                    .into(viewHolder.lookImageView);
                        }
                    });

        }

        if (look.getUser().getImageUrl() != null) {

            Picasso.with(mContext)
                    .load(look.getUser().getImageUrl())
                    .into(viewHolder.avatar, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(mContext)
                                    .load(look.getUser().getImageUrl())
                                    .into(viewHolder.avatar);
                        }
                    });

            Picasso.with(mContext)
                    .load(look.getUser().getImageUrl())
                    .into(viewHolder.smallAvatar, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(mContext)
                                    .load(look.getUser().getImageUrl())
                                    .into(viewHolder.smallAvatar);
                        }
                    });




        }
    }



    @Override
    public int getItemCount() {
        if (mLooks!=null){
            return mLooks.size();

        }
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.lookImageView)
        ImageView lookImageView;

        @BindView(R.id.followImageView)
        ImageView followImageView;

        @BindView(R.id.shareImageView)
        ImageView shareImageView;

        @BindView(R.id.likeImageView)
        ImageView likeImageView;

        @BindView(R.id.pinImageView)
        ImageView pinImageView;

        @BindView(R.id.avatarImageView)
        CircleImageView avatar;

        @BindView(R.id.smallAvatar)
        CircleImageView smallAvatar;

        @BindView(R.id.nameTextView)
        TextView nameTextView;

        @BindView(R.id.usernameTextView)
        TextView usernameTextView;

        @BindView(R.id.timeTextView)
        TextView timeTextView;

        @BindView(R.id.captionTextView)
        TextView captionTextView;

        @BindView(R.id.numOfFollowersTextView)
        TextView numOfFollowersTextView;

        @BindView(R.id.numOfLikesTextView)
        TextView numOfLikesTextView;

        @BindView(R.id.numOfCommentsTextView)
        TextView numOfCommentsTextView;

        @BindView(R.id.numOfViewsTextView)
        TextView numOfViewsTextView;



        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            nameTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaMedium.ttf"));
            usernameTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            timeTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            numOfCommentsTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            numOfFollowersTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            numOfLikesTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            numOfViewsTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));
            captionTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "helveticaLight.ttf"));

            shareImageView.setVisibility(View.GONE);
            pinImageView.setVisibility(View.GONE);
            likeImageView.setVisibility(View.GONE);
            followImageView.setVisibility(View.GONE);



        }


        private void emptyViewHolder(){
            nameTextView.setText("");
            usernameTextView.setText("");
            timeTextView.setText("");
            numOfViewsTextView.setText("");
            numOfLikesTextView.setText("");
            numOfFollowersTextView.setText("");
            numOfCommentsTextView.setText("");
            captionTextView.setText("");

            avatar.setImageResource(R.mipmap.avatar);
            smallAvatar.setImageResource(R.mipmap.avatar);


        }
    }

    public void add(ArrayList<? extends Entity> result) {
        for (int i = 0; i < result.size() ; i++) {
            mLooks.add(((ArrayList<Look>) result).get(i));
        }
        notifyDataSetChanged();
    }
}
