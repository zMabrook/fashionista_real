package com.zmabrook.fashionista.Views.Adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Views.Fragments.SearchFragments.SearchBrandsFragment;
import com.zmabrook.fashionista.Views.Fragments.SearchFragments.SearchCategoriesFragment;
import com.zmabrook.fashionista.Views.Fragments.SearchFragments.SearchPeopleFragment;

/**
 * SearchPagerViewAdapter
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Adapters
 * @since 8/8/16
 **/
public class SearchPagerViewAdapter extends FragmentPagerAdapter  {
    String[] tabsNames;
    User user=null;

    public SearchPagerViewAdapter(String[] tabsNames,User user ,FragmentManager fm) {
        super(fm);
        this.tabsNames = tabsNames;
        this.user = user;

    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle;

        switch (position) {
            case 0:
                SearchCategoriesFragment searchCategoriesFragment = new SearchCategoriesFragment();
                bundle = new Bundle();
                bundle.putSerializable("user",user);
                searchCategoriesFragment.setArguments(bundle);
                return searchCategoriesFragment;


            case 1:
                SearchBrandsFragment searchBrandsFragment = new SearchBrandsFragment();
                bundle = new Bundle();
                bundle.putSerializable("user",user);
                searchBrandsFragment.setArguments(bundle);
                return searchBrandsFragment;


            case 2:
                SearchPeopleFragment searchPeopleFragment = new SearchPeopleFragment();
                bundle = new Bundle();
                bundle.putSerializable("user",user);
                searchPeopleFragment.setArguments(bundle);
                return searchPeopleFragment;

            default:
                return null;
        }    }

    @Override
    public int getCount() {
        return 3;
    }

    /**
     * @param position the position of tab
     * @return the title of tab
     */
    @Override
    public CharSequence getPageTitle(int position) {
        return tabsNames[position];
    }
}
