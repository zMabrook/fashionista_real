package com.zmabrook.fashionista.Views.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.zmabrook.fashionista.Views.Fragments.Tour.Tour1Fragment;
import com.zmabrook.fashionista.Views.Fragments.Tour.Tour2Fragment;
import com.zmabrook.fashionista.Views.Fragments.Tour.Tour3Fragment;
import com.zmabrook.fashionista.Views.Fragments.Tour.Tour4Fragment;

/**
 * TourPagerViewAdapter
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Adapters
 * @since 8/10/16
 **/
public class TourPagerViewAdapter extends FragmentPagerAdapter {
    public TourPagerViewAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                Tour1Fragment tour1Fragment = new Tour1Fragment();
                return tour1Fragment;

            case 1:
                Tour2Fragment tour2Fragment = new Tour2Fragment();
                return tour2Fragment;

            case 2:
                Tour3Fragment tour3Fragment = new Tour3Fragment();
                return tour3Fragment;

            case 3:
                Tour4Fragment tour4Fragment = new Tour4Fragment();
                return tour4Fragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
