package com.zmabrook.fashionista.Views.Custom;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zmabrook.fashionista.R;

/**
 * PeopleTabLayout
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Custom
 * @since 8/8/16
 **/
public class PeopleTabLayout extends TabLayout {
    private Typeface mTypeface;

    public PeopleTabLayout(Context context) {
        super(context);
        init();

    }

    public PeopleTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public PeopleTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();

    }


    private void init() {
        mTypeface = Typeface.createFromAsset(getContext().getAssets(),"helveticaLight.ttf");
    }


    @Override
    public void addTab(Tab tab) {
        super.addTab(tab);

        ViewGroup mainView = (ViewGroup) getChildAt(0);
        ViewGroup tabView = (ViewGroup) mainView.getChildAt(tab.getPosition());

        int tabChildCount = tabView.getChildCount();
        for (int i = 0; i < tabChildCount; i++) {
            View tabViewChild = tabView.getChildAt(i);
            if (tabViewChild instanceof TextView) {
                ((TextView) tabViewChild).setTypeface(mTypeface, Typeface.NORMAL);
                ((TextView) tabViewChild).setAllCaps(false);
                ((TextView) tabViewChild).setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

            }
        }
    }
}
