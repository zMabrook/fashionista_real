package com.zmabrook.fashionista.Views.Fragments.HomeFragments;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.R;


import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * HomeMapFragment
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Fragments
 * @since 8/3/16
 **/
public class HomeMapFragment extends Fragment {
    View view;
    SupportMapFragment mapView;


    GoogleMap map;
    LatLng CENTER = null;

    public LocationManager locationManager;

    double longitudeDouble;
    double latitudeDouble;

    String snippet;
    String title;
    Location location;
    String myAddress;

    String LocationId;
    String CityName;
    String imageURL;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_map, container, false);
        init();
        return view;
    }

    private void init() {


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            setMapView();

        }
    }

    private void setMapView() {
        mapView = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.mapview));

        try {
            MapsInitializer.initialize(getActivity());

            switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity())) {
                case ConnectionResult.SUCCESS:
                    // Toast.makeText(getActivity(), "SUCCESS", Toast.LENGTH_SHORT)
                    // .show();

                    // Gets to GoogleMap from the MapView and does initialization
                    // stuff
                    if (mapView != null) {

                        locationManager = ((LocationManager) getActivity()
                                .getSystemService(Context.LOCATION_SERVICE));

                        Boolean localBoolean = Boolean.valueOf(locationManager
                                .isProviderEnabled("network"));

                        if (localBoolean.booleanValue()) {

                            CENTER = new LatLng(30.0553861, 31.3834799);

                        } else {

                        }
                        map =mapView.getMap();
                        if (map == null) {

                            Log.d(CommonConstants.APP_TAG, "Map SearchBrandsFragment Not Found or no Map in it!!");

                        }

                        map.clear();
                        try {
                            map.addMarker(new MarkerOptions().position(CENTER)
                                    .title(CityName).snippet(""));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        map.setIndoorEnabled(true);
                        map.setMyLocationEnabled(true);
                        map.moveCamera(CameraUpdateFactory.zoomTo(5));
                        if (CENTER != null) {
                            map.animateCamera(
                                    CameraUpdateFactory.newLatLng(CENTER), 1750,
                                    null);
                        }
                        // add circle
                        CircleOptions circle = new CircleOptions();
                        circle.center(CENTER).fillColor(Color.BLUE).radius(10);
                        map.addCircle(circle);
                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                    }
                    break;
                case ConnectionResult.SERVICE_MISSING:
                    Log.d(CommonConstants.APP_TAG, "Google Play service missing");

                    break;
                case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                    Log.d(CommonConstants.APP_TAG, "Google Play service require update");

                    break;
                default:

            }
        } catch (Exception e) {

        }

    }
}
