package com.zmabrook.fashionista.Views.Fragments.HomeFragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.LooksResult;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.LooksMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Adapters.MyFashionRecyclerViewAdapter;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by matrix on 12/08/2016.
 */
public class MyFashionFragment extends Fragment {
    View view;
    LinearLayoutManager linearLayoutManager;
    int mFirstVisibleItem, mVisibleItemCount, mTotalItemCount, mLastFirstVisibleItem;
    boolean isLoading = false;
    boolean gotToEnd = false;
    LooksResult result;
    public static int page = 1;

    @BindView(R.id.homeTrendingListRecyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.trloadingBar)
    TextView loadingBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_trending_list, container, false);


        init();


        return view;
    }


    private void init() {
        ButterKnife.bind(this, view);
        loadingBar.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "helveticaLight.ttf"));
        showLoadingBar();

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

    }



    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (result == null) {

                RequestBody requestBody = new RequestBody();

                HashMap<String, String> queryparams = new HashMap<String, String>();
                queryparams.put("page", String.valueOf(page));

                requestBody.setQueryParams(queryparams);

                LooksMapper looksMapper = new LooksMapper(getActivity(), CommonConstants.MYFASHION_ACTION);
                looksMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
                    @Override
                    public void onSuccess(Entity result) {
                        final LooksResult looksResult = (LooksResult) result;
                        if (result != null) {

                            final MyFashionRecyclerViewAdapter adapter = new MyFashionRecyclerViewAdapter(getActivity(), looksResult.looks);
                            recyclerView.setAdapter(adapter);
                            recyclerView.setHasFixedSize(true);

                            if (view != null)
                                hideLoadingBar();

                            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

                                @Override
                                public void onScrolled(final RecyclerView recyclerView, int dx, int dy) {
                                    super.onScrolled(recyclerView, dx, dy);
                                    mVisibleItemCount = recyclerView.getChildCount();
                                    mTotalItemCount = linearLayoutManager.getItemCount();
                                    mFirstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                                    //checking if no more results to get
                                    if (mTotalItemCount >= looksResult.numberOfLooks) {
                                        gotToEnd = true;

                                        return;
                                    }

                                    //checking if scrolling down
                                    if (mLastFirstVisibleItem < mFirstVisibleItem) {
                                        int lastInScreen = mFirstVisibleItem + mVisibleItemCount;

                                        //check if got to last item and not loading and the item count is bigger than default:20
                                        if ((lastInScreen == mTotalItemCount) && !(isLoading) && !gotToEnd) {
                                            showLoadingBar();
                                            isLoading = true;


                                            RequestBody requestBody = new RequestBody();


                                            HashMap<String, String> queryparams = new HashMap<String, String>();
                                            queryparams.put("page", String.valueOf(++page));

                                            requestBody.setQueryParams(queryparams);

                                            LooksMapper looksMapper = new LooksMapper(getActivity(), CommonConstants.TRENDING_ACTION);
                                            looksMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener() {
                                                @Override
                                                public void onSuccess(Object result) {
                                                    LooksResult looksResult = (LooksResult) result;
                                                    result = looksResult;
                                                    if (looksResult.numberOfLooks!=0){
                                                        adapter.add(looksResult.looks);

                                                    }
                                                    hideLoadingBar();
                                                    isLoading = false;


                                                }

                                                @Override
                                                public void onError(Exception e, Object result) {
                                                    isLoading = false;
                                                    hideLoadingBar();
                                                    showErrorBar();
                                                }
                                            }, requestBody);

                                        }
                                        mLastFirstVisibleItem = mFirstVisibleItem;

                                    }

                                }
                            });

                        }
                    }

                    @Override
                    public void onError(Exception e, Entity result) {
                        Toast.makeText(getActivity(), "Server error", Toast.LENGTH_LONG);
                        if (view != null)
                            hideLoadingBar();

                    }
                }, requestBody);


            }
        }
    }


    private void showLoadingBar() {
        loadingBar.setText("Loading...");
        loadingBar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        loadingBar.setVisibility(View.VISIBLE);
        loadingBar
                .animate()
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator(1));
    }

    private void hideLoadingBar() {
        loadingBar.setText("Loading...");
        loadingBar
                .animate()
                .translationY(loadingBar.getHeight())
                .setInterpolator(new AccelerateInterpolator(1));
        loadingBar.setVisibility(View.GONE);

    }

    private void showErrorBar() {
        loadingBar.setText("Something went wrong");
        loadingBar.setBackgroundColor(getResources().getColor(R.color.tw__composer_red));

        loadingBar.setVisibility(View.VISIBLE);
        loadingBar
                .animate()
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator(2));
    }

    private void hideErrorBar() {
        loadingBar.setText("Something went wrong");
        loadingBar.setBackgroundColor(getResources().getColor(R.color.tw__composer_red));
        loadingBar
                .animate()
                .translationY(loadingBar.getHeight())
                .setInterpolator(new AccelerateInterpolator(2));
        loadingBar.setVisibility(View.GONE);

    }
}
