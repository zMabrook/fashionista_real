package com.zmabrook.fashionista.Views.Fragments.HomeFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.PeopleSearchResult;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Entities.User;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.PeopleMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Adapters.PeopleRecyclerViewAdapter;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * PeopleFragment
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Fragments.HomeFragments
 * @since 8/11/16
 **/
public class PeopleFragment extends Fragment {

    View view;
    LinearLayoutManager linearLayoutManager;
    int mFirstVisibleItem, mVisibleItemCount, mTotalItemCount, mLastFirstVisibleItem;
    boolean isLoading = false;
    boolean gotToEnd = false;
    PeopleSearchResult result;
    public static int pageNumber = 1;
    PeopleSearchResult peopleSearchResult;
    PeopleRecyclerViewAdapter adapter;


    @BindView(R.id.PeopeleRecyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.trloadingBar)
    TextView loadingBar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_people, container, false);


        init();
    return view;
    }

    private void init() {
        ButterKnife.bind(this, view);
        showLoadingBar();

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        peopleSearchResult = new PeopleSearchResult();




    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser){
            if (result == null) {

                final RequestBody requestBody = new RequestBody();

                PeopleMapper peopleMapper = new PeopleMapper(getActivity(), CommonConstants.FEATURED_PEOPLE_ACTION);
                peopleMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
                    @Override
                    public void onSuccess(Entity result) {
                        final PeopleSearchResult featuredPeopleSearchResult = (PeopleSearchResult) result;
                        if (result != null) {
                            User user = new User();
                            user.setName("FEATURED PEOPLE");
                            user.setIsHeader(true);
                            featuredPeopleSearchResult.getPeopleList().add(0, user);
                            peopleSearchResult.setNumOfPeople(featuredPeopleSearchResult.getNumOfPeople()+1);
                            peopleSearchResult.setNumOfPages(featuredPeopleSearchResult.getNumOfPages());
                            peopleSearchResult.setPeopleList(featuredPeopleSearchResult.getPeopleList());


                            HashMap<String, String> queryparams = new HashMap<String, String>();
                            queryparams.put("page", String.valueOf(pageNumber));
                            final RequestBody requestBody = new RequestBody();
                            requestBody.setQueryParams(queryparams);


                            PeopleMapper peopleMapper = new PeopleMapper(getActivity(), CommonConstants.TRENDING_PEOPLE_ACTION);
                            peopleMapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
                                @Override
                                public void onSuccess(Entity result) {
                                    if (result != null) {
                                         PeopleSearchResult trendingPeopleSearchResult = (PeopleSearchResult) result;
                                        User user = new User();
                                        user.setName("TRENDING PEOPLE");
                                        user.setIsHeader(true);
                                        trendingPeopleSearchResult.getPeopleList().add(0, user);

                                        peopleSearchResult.getPeopleList().addAll(trendingPeopleSearchResult.getPeopleList());
                                        peopleSearchResult.setNumOfPages(trendingPeopleSearchResult.getNumOfPages());
                                        peopleSearchResult.setNumOfPeople(peopleSearchResult.getNumOfPeople() + trendingPeopleSearchResult.getNumOfPeople() + 1);

                                        adapter = new PeopleRecyclerViewAdapter(getActivity(), peopleSearchResult.getPeopleList());
                                        recyclerView.setAdapter(adapter);
                                        recyclerView.setHasFixedSize(true);
                                        hideLoadingBar();





                                        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

                                            @Override
                                            public void onScrolled(final RecyclerView recyclerView, int dx, int dy) {
                                                super.onScrolled(recyclerView, dx, dy);
                                                mVisibleItemCount = recyclerView.getChildCount();
                                                mTotalItemCount = linearLayoutManager.getItemCount();
                                                mFirstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                                                //checking if no more results to get
                                                if (mTotalItemCount >= peopleSearchResult.getNumOfPeople()) {
                                                    gotToEnd = true;

                                                    return;
                                                }

                                                //checking if scrolling down
                                                if (mLastFirstVisibleItem < mFirstVisibleItem) {
                                                    int lastInScreen = mFirstVisibleItem + mVisibleItemCount;

                                                    //check if got to last item and not loading and the item count is bigger than default:20
                                                    if ((lastInScreen == mTotalItemCount) && !(isLoading) && !gotToEnd) {
                                                        showLoadingBar();
                                                        isLoading = true;


                                                        RequestBody requestBody = new RequestBody();
                                                        HashMap<String, String> queryparams = new HashMap<String, String>();
                                                        queryparams.put("page", String.valueOf(++pageNumber));


                                                        PeopleMapper mapper = new PeopleMapper(getActivity(), CommonConstants.TRENDING_PEOPLE_ACTION);
                                                        mapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
                                                            @Override
                                                            public void onSuccess(Entity result) {
                                                                PeopleSearchResult trendingListResult = (PeopleSearchResult) result;
                                                                result = trendingListResult;
                                                                adapter.add(trendingListResult.getPeopleList());
                                                                hideLoadingBar();
                                                                isLoading = false;
                                                            }

                                                            @Override
                                                            public void onError(Exception e, Entity result) {

                                                            }
                                                        }, requestBody);


                                                    }
                                                    mLastFirstVisibleItem = mFirstVisibleItem;

                                                }

                                            }
                                        });



                                    }
                                }

                                @Override
                                public void onError(Exception e, Entity result) {

                                }
                            },requestBody);


                        }
                    }@Override
                     public void onError(Exception e, Entity result) {
                        Toast.makeText(getActivity(), "Server error", Toast.LENGTH_LONG);
                        if (view != null){

                        }

                    }
                }, requestBody);



            }
        }
    }

    private void  showLoadingBar(){
        loadingBar.setText("Loading...");
        loadingBar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        loadingBar.setVisibility(View.VISIBLE);
        loadingBar
                .animate()
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator(1));
    }

    private void  hideLoadingBar(){
        loadingBar.setText("Loading...");
        loadingBar
                .animate()
                .translationY(loadingBar.getHeight())
                .setInterpolator(new AccelerateInterpolator(1));
        loadingBar.setVisibility(View.GONE);

    }
}
