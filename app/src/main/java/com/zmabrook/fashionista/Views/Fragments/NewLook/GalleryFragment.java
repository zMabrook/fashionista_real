package com.zmabrook.fashionista.Views.Fragments.NewLook;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.NewLook.NewLookActivity;
import com.zmabrook.fashionista.Views.Adapters.GalleryImageAdapter;
import com.zmabrook.fashionista.Views.Custom.CheckableImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * GalleryFragment
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Fragments.NewLook
 * @since 8/18/16
 **/
public class GalleryFragment extends Fragment {
    View view;
    GalleryImageAdapter adapter;
    @BindView(R.id.gridview)
    GridView gridView;

    @BindView(R.id.choosenImage)
    public ImageView choosenImageView;

    @BindView(R.id.closeImage)
    public ImageView closeImageView;

    int oldPosition = -1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_gallery, container, false);


        init();


        return view;
    }


    private void init() {
        ButterKnife.bind(this, view);
        adapter = new GalleryImageAdapter(getActivity());
        gridView.setAdapter(adapter);
        gridView.setChoiceMode(GridView.CHOICE_MODE_SINGLE);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NewLookActivity newLookActivity =(NewLookActivity)getActivity();
                newLookActivity.selectedMediaUrl = adapter.images.get(position);

                adapter.selectedImage = position;
                adapter.notifyDataSetChanged();

            }
        });

        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                closeImageView.setVisibility(View.VISIBLE);
                choosenImageView.setVisibility(View.VISIBLE);


                Glide.with(getActivity())
                        .load(adapter.images.get(position))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(choosenImageView);

                closeImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        choosenImageView.setVisibility(View.GONE);
                        closeImageView.setVisibility(View.GONE);
                    }
                });
                return true;
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && view !=null){
            if (choosenImageView.getVisibility()==View.VISIBLE){
                choosenImageView.setVisibility(View.GONE);
                closeImageView.setVisibility(View.GONE);

            }
        }

    }
}
