package com.zmabrook.fashionista.Views.Fragments.NewLook;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zmabrook.fashionista.R;

import butterknife.ButterKnife;

/**
 * PhotoFragment
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Fragments.NewLook
 * @since 8/18/16
 **/
public class PhotoFragment extends Fragment {

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_photo, container, false);


        init();


        return view;
    }


    private void init() {
        ButterKnife.bind(this, view);


    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser){
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {


        }

    }
}
