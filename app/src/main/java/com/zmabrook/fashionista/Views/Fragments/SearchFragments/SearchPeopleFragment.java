package com.zmabrook.fashionista.Views.Fragments.SearchFragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.Domain.Entities.Entity;
import com.zmabrook.fashionista.Domain.Entities.PeopleSearchResult;
import com.zmabrook.fashionista.Domain.Entities.RequestBody;
import com.zmabrook.fashionista.Domain.Mapper.Abstracts.AbstractWebserviceMapper;
import com.zmabrook.fashionista.Domain.Mapper.SearchMapper;
import com.zmabrook.fashionista.Listeners.FashionistaListener;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Adapters.PeopleSearchRecyclerViewAdapter;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * SearchPeopleFragment
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Fragments
 * @since 8/10/16
 **/
public class SearchPeopleFragment extends Fragment {
    PeopleSearchResult brandSearchResult;

    View view;
    LinearLayoutManager linearLayoutManager;
    public static int pageNumber = 1;
    int mFirstVisibleItem, mVisibleItemCount, mTotalItemCount, mLastFirstVisibleItem;
    boolean isLoading=false;
    boolean gotToEnd=false;
    PeopleSearchRecyclerViewAdapter adapter;


    @BindView(R.id.searchEditText)
    EditText searchEditText;

    @BindView(R.id.trendingCategoriesTextView)
    TextView trendingCategoriesTitle;

    @BindView(R.id.categoriesListRecyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.noresultsTextView)
    TextView noResultTextView;

    @BindView(R.id.trloadingBar)
    TextView loadingBar;

    @BindView(R.id.searchImageButton)
    ImageButton searchButton;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_categories, container, false);


        init();


        return view;
    }


    private void init() {
        ButterKnife.bind(this, view);
        loadingBar.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "helveticaLight.ttf"));
        showLoadingBar();

        trendingCategoriesTitle.setVisibility(View.GONE);

        searchEditText.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "helveticaLight.ttf"));
        noResultTextView.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "helveticaLight.ttf"));

        trendingCategoriesTitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "helveticaMedium.ttf"));

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
                adapter.notifyDataSetChanged();
                if (adapter.getItemCount() == 0) {
                    noResultTextView.setVisibility(View.VISIBLE);

                } else {
                    noResultTextView.setVisibility(View.GONE);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.notifyDataSetChanged();

            }
        });


        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                RequestBody requestBody = new RequestBody();
                HashMap<String, String> queryparams = new HashMap<String, String>();
                pageNumber=1;
                queryparams.put("page", String.valueOf(pageNumber));
                if (!searchEditText.getText().toString().equals("")){
                    queryparams.put("query",searchEditText.getText().toString());

                }
                SearchMapper mapper = new SearchMapper(getActivity(), CommonConstants.PEOPLE_SEARCH_ACTION);
                mapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
                    @Override
                    public void onSuccess(Entity result) {
                        hideLoadingBar();
                        brandSearchResult = (PeopleSearchResult) result;
                        adapter = new PeopleSearchRecyclerViewAdapter(getActivity(), brandSearchResult.getPeopleList());
                        recyclerView.setAdapter(adapter);
                        recyclerView.setHasFixedSize(true);
                        if (brandSearchResult.getPeopleList().size() == 0) {
                            noResultTextView.setVisibility(View.VISIBLE);

                        } else {
                            noResultTextView.setVisibility(View.GONE);

                        }

                        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

                            @Override
                            public void onScrolled(final RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                mVisibleItemCount = recyclerView.getChildCount();
                                mTotalItemCount = linearLayoutManager.getItemCount();
                                mFirstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                                //checking if no more results to get
                                if (mTotalItemCount >= brandSearchResult.getNumOfPeople()) {
                                    gotToEnd = true;

                                    return;
                                }

                                //checking if scrolling down
                                if (mLastFirstVisibleItem < mFirstVisibleItem) {
                                    int lastInScreen = mFirstVisibleItem + mVisibleItemCount;

                                    //check if got to last item and not loading and the item count is bigger than default:20
                                    if ((lastInScreen == mTotalItemCount) && !(isLoading) && !gotToEnd) {
                                        showLoadingBar();
                                        isLoading = true;


                                        RequestBody requestBody = new RequestBody();
                                        HashMap<String, String> queryparams = new HashMap<String, String>();
                                        queryparams.put("page", String.valueOf(++pageNumber));


                                        SearchMapper mapper = new SearchMapper(getActivity(), CommonConstants.BRANDS_SEARCH_ACTION);
                                        mapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
                                            @Override
                                            public void onSuccess(Entity result) {
                                                PeopleSearchResult trendingListResult = (PeopleSearchResult) result;
                                                result = trendingListResult;
                                                adapter.add(trendingListResult.getPeopleList());
                                                hideLoadingBar();
                                                isLoading = false;
                                            }

                                            @Override
                                            public void onError(Exception e, Entity result) {

                                            }
                                        }, requestBody);


                                    }
                                    mLastFirstVisibleItem = mFirstVisibleItem;

                                }

                            }
                        });


                    }

                    @Override
                    public void onError(Exception e, Entity result) {

                    }
                }, requestBody);


            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (brandSearchResult == null) {

                RequestBody requestBody = new RequestBody();
                HashMap<String, String> queryparams = new HashMap<String, String>();
                queryparams.put("page",String.valueOf(pageNumber));
                if (!searchEditText.getText().toString().equals("")){
                    queryparams.put("query",searchEditText.getText().toString());

                }

                requestBody.setQueryParams(queryparams);
                SearchMapper mapper = new SearchMapper(getActivity(), CommonConstants.PEOPLE_SEARCH_ACTION);
                mapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
                    @Override
                    public void onSuccess(Entity result) {
                        hideLoadingBar();
                        brandSearchResult = (PeopleSearchResult) result;
                        adapter = new PeopleSearchRecyclerViewAdapter(getActivity(), brandSearchResult.getPeopleList());
                        recyclerView.setAdapter(adapter);
                        recyclerView.setHasFixedSize(true);
                        if (brandSearchResult.getPeopleList().size() == 0) {
                            noResultTextView.setVisibility(View.VISIBLE);

                        } else {
                            noResultTextView.setVisibility(View.GONE);

                        }

                        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

                            @Override
                            public void onScrolled(final RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                mVisibleItemCount = recyclerView.getChildCount();
                                mTotalItemCount = linearLayoutManager.getItemCount();
                                mFirstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                                //checking if no more results to get
                                if (mTotalItemCount >= brandSearchResult.getNumOfPeople()) {
                                    gotToEnd = true;

                                    return;
                                }

                                //checking if scrolling down
                                if (mLastFirstVisibleItem < mFirstVisibleItem) {
                                    int lastInScreen = mFirstVisibleItem + mVisibleItemCount;

                                    //check if got to last item and not loading and the item count is bigger than default:20
                                    if ((lastInScreen == mTotalItemCount) && !(isLoading) && !gotToEnd) {
                                        showLoadingBar();
                                        isLoading = true;


                                        RequestBody requestBody = new RequestBody();
                                        HashMap<String, String> queryparams = new HashMap<String, String>();
                                        queryparams.put("page",String.valueOf(++pageNumber));
                                        if (!searchEditText.getText().toString().equals("")){
                                            queryparams.put("query",searchEditText.getText().toString());

                                        }

                                        SearchMapper mapper = new SearchMapper(getActivity(), CommonConstants.BRANDS_SEARCH_ACTION);
                                        mapper.executeRequest(AbstractWebserviceMapper.JsonRequestType.JSON_OBJECT, Request.Method.GET, new FashionistaListener<Entity>() {
                                            @Override
                                            public void onSuccess(Entity result) {
                                                PeopleSearchResult trendingListResult = (PeopleSearchResult) result;
                                                result = trendingListResult;
                                                adapter.add(trendingListResult.getPeopleList());
                                                hideLoadingBar();
                                                isLoading = false;
                                            }

                                            @Override
                                            public void onError(Exception e, Entity result) {

                                            }
                                        }, requestBody);


                                    }
                                    mLastFirstVisibleItem = mFirstVisibleItem;

                                }

                            }
                        });


                    }

                    @Override
                    public void onError(Exception e, Entity result) {

                    }
                }, requestBody);

            }
        }



    }

    private void  showLoadingBar(){
        loadingBar.setText("Loading...");
        loadingBar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        loadingBar.setVisibility(View.VISIBLE);
        loadingBar
                .animate()
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator(1));
    }

    private void  hideLoadingBar(){
        loadingBar.setText("Loading...");
        loadingBar
                .animate()
                .translationY(loadingBar.getHeight())
                .setInterpolator(new AccelerateInterpolator(1));
        loadingBar.setVisibility(View.GONE);

    }
}
