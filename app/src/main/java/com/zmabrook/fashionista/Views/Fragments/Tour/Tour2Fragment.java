package com.zmabrook.fashionista.Views.Fragments.Tour;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zmabrook.fashionista.Config.CommonConstants;
import com.zmabrook.fashionista.R;
import com.zmabrook.fashionista.Views.Activites.Registeration.RegiserationActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Tour2Fragment
 * <br/> class description and Used for What
 *
 * @author Ahmed Mabrook  <a.mabrok@yellow.com.eg>
 * @package com.zmabrook.fashionista.Views.Fragments.Tour
 * @since 8/10/16
 **/
public class Tour2Fragment extends Fragment {
    View view;

    @BindView(R.id.tour2SkipButton)
    TextView skipButton;

    @BindView(R.id.tour2TextView)
    TextView description;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tour2, container, false);


        init();




        return view;
    }


    private void init() {
        ButterKnife.bind(this, view);


        description.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "helveticaLight.ttf"));
        skipButton.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "helveticaLight.ttf"));

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences settings = getActivity().getApplicationContext().getSharedPreferences(CommonConstants.PREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();

                editor.putBoolean(CommonConstants.PREFS_TOUR_FINISHED, true);

                editor.commit();


                Intent i = new Intent(getActivity(),RegiserationActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);


            }
        });





    }
}
